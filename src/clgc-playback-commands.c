/*
 * clgc-playback-commands: Calgici PlaybackCommands class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-playback-commands.h"

#include <libintl.h>

enum _ClgcPlaybackCommandsId
{
	CLGC_PLAYBACK_COMMANDS_PREVIOUS,
	CLGC_PLAYBACK_COMMANDS_PLAY,
	CLGC_PLAYBACK_COMMANDS_PAUSE,
	CLGC_PLAYBACK_COMMANDS_STOP,
	CLGC_PLAYBACK_COMMANDS_NEXT,
	CLGC_PLAYBACK_COMMANDS_MAX
};

enum _ClgcPlaybackCommandsProperty
{
	PROP_POLICY = 1,
	N_PROPERTIES
};

struct _ClgcPlaybackCommands
{
	GtkWidget m_parent;

	ClgcPlaybackCommandsPolicy m_policy;

	GtkWidget* m_previous;
	GtkWidget* m_play;
	GtkWidget* m_pause;
	GtkWidget* m_stop;
	GtkWidget* m_next;
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcPlaybackCommands, clgc_playback_commands, GTK_TYPE_WIDGET)

static void clgc_playback_commands_update_visual(ClgcPlaybackCommands* self, ClgcPlaybackCommandsPolicy policy)
{
	switch (policy)
	{
		case CLGC_PLAYBACK_COMMANDS_POLICY_NARROW:
			gtk_widget_set_visible(self->m_previous, FALSE);
			gtk_widget_set_visible(self->m_stop, FALSE);
			gtk_widget_set_visible(self->m_next, FALSE);
			break;
		case CLGC_PLAYBACK_COMMANDS_POLICY_NORMAL:
			gtk_widget_set_visible(self->m_previous, FALSE);
			gtk_widget_set_visible(self->m_stop, FALSE);
			gtk_widget_set_visible(self->m_next, TRUE);
			break;
		case CLGC_PLAYBACK_COMMANDS_POLICY_WIDE:
			gtk_widget_set_visible(self->m_previous, TRUE);
			gtk_widget_set_visible(self->m_stop, TRUE);
			gtk_widget_set_visible(self->m_next, TRUE);
			break;
		default:
			break;
	}
}

static void clgc_playback_commands_dispose(GObject* obj)
{
	int id;
	ClgcPlaybackCommands* self = CLGC_PLAYBACK_COMMANDS(obj);

	GtkWidget** buttons = &(self->m_previous);

	for (id = 0; id < CLGC_PLAYBACK_COMMANDS_MAX; id ++)
		g_clear_pointer(&(buttons[id]), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_playback_commands_parent_class)->dispose(obj);
}

static void clgc_playback_commands_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcPlaybackCommands* self = CLGC_PLAYBACK_COMMANDS(obj);

	switch (property_id)
	{
		case PROP_POLICY:
			g_value_set_int(value, self->m_policy);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_playback_commands_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcPlaybackCommands* self = CLGC_PLAYBACK_COMMANDS(obj);

	switch (property_id)
	{
		case PROP_POLICY:
			self->m_policy = g_value_get_int(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_playback_commands_measure(GtkWidget* widget, GtkOrientation orientation, int for_size, int* minimum, int* natural, int* minimum_baseline, int* natural_baseline)
{
	int id;
	int tmp_min, tmp_nat;
	int size_min = 0, size_nat = 0;
	ClgcPlaybackCommands* self = CLGC_PLAYBACK_COMMANDS(widget);
	GtkWidget** buttons = &(self->m_previous);

	for (id = 0; id < CLGC_PLAYBACK_COMMANDS_MAX; id ++)
	{
		tmp_min = 0;
		tmp_nat = 0;

		if (gtk_widget_get_visible(buttons[id]))
			gtk_widget_measure(buttons[id], orientation, for_size, &tmp_min, &tmp_nat, NULL, NULL);

		if (orientation == GTK_ORIENTATION_HORIZONTAL)
		{
			size_min += (id == CLGC_PLAYBACK_COMMANDS_PLAY || id == CLGC_PLAYBACK_COMMANDS_PAUSE) ? tmp_min : 0;
			size_nat += tmp_nat;
		}
		else
		{
			size_min = MAX(tmp_min, size_min);
			size_nat = MAX(tmp_nat, size_nat);
		}
	}

	if (minimum)
		*minimum = size_min;
	if (natural)
		*natural = size_nat;
	if (minimum_baseline)
		*minimum_baseline = -1;
	if (natural_baseline)
		*natural_baseline = -1;
}

static void clgc_playback_commands_size_allocate(GtkWidget* widget, int width, int height, int baseline)
{
	int id;
	int natural_width;
	int natural_height;
	GtkAllocation child_allocation = {0, 0, 0, 0};
	ClgcPlaybackCommands* self = CLGC_PLAYBACK_COMMANDS(widget);
	GtkWidget** buttons = &(self->m_previous);

	if (self->m_policy != CLGC_PLAYBACK_COMMANDS_POLICY_AUTO)
		return;

	if (width < 80)
		clgc_playback_commands_update_visual(self, CLGC_PLAYBACK_COMMANDS_POLICY_NARROW);
	else if (width < 140)
		clgc_playback_commands_update_visual(self, CLGC_PLAYBACK_COMMANDS_POLICY_NORMAL);
	else
		clgc_playback_commands_update_visual(self, CLGC_PLAYBACK_COMMANDS_POLICY_WIDE);

	for (id = 0; id < CLGC_PLAYBACK_COMMANDS_MAX; id ++)
	{
		gtk_widget_measure(buttons[id], GTK_ORIENTATION_HORIZONTAL, -1, NULL, &natural_width, NULL, NULL);
		gtk_widget_measure(buttons[id], GTK_ORIENTATION_VERTICAL, -1, NULL, &natural_height, NULL, NULL);
		child_allocation.x += child_allocation.width;
		child_allocation.y = 0;
		child_allocation.width = natural_width;
		child_allocation.height = natural_height;

		gtk_widget_size_allocate(buttons[id], &child_allocation, baseline);
	}
}

static void clgc_playback_commands_class_init(ClgcPlaybackCommandsClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_playback_commands_dispose;
	obj_class->get_property = clgc_playback_commands_get_property;
	obj_class->set_property = clgc_playback_commands_set_property;

	widget_class->measure = clgc_playback_commands_measure;
	widget_class->size_allocate = clgc_playback_commands_size_allocate;

	obj_properties[PROP_POLICY] = g_param_spec_int(
		"policy",
		"Policy",
		"The policy to determine the mode to use",
		CLGC_PLAYBACK_COMMANDS_POLICY_AUTO,
		CLGC_PLAYBACK_COMMANDS_POLICY_WIDE,
		CLGC_PLAYBACK_COMMANDS_POLICY_AUTO,
		G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_css_name(widget_class, "box");
	gtk_widget_class_set_accessible_role(widget_class, GTK_ACCESSIBLE_ROLE_GROUP);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-playback-commands.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackCommands, m_previous);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackCommands, m_play);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackCommands, m_pause);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackCommands, m_stop);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackCommands, m_next);
}

static void clgc_playback_commands_init(ClgcPlaybackCommands* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_playback_commands_new()
{
	return g_object_new(CLGC_TYPE_PLAYBACK_COMMANDS, NULL);
}

ClgcPlaybackCommandsPolicy clgc_playback_commands_get_policy(ClgcPlaybackCommands* self)
{
	g_return_val_if_fail(CLGC_IS_PLAYBACK_COMMANDS(self), CLGC_PLAYBACK_COMMANDS_POLICY_WIDE);

	return self->m_policy;
}

void clgc_playback_commands_set_state(ClgcPlaybackCommands* self, ClgcPlaybackState state)
{
	switch (state)
	{
		case CLGC_PLAYBACK_STATE_STOP:
			gtk_widget_set_sensitive(self->m_stop, FALSE);
			__attribute__((fallthrough));
		case CLGC_PLAYBACK_STATE_PAUSE:
			gtk_widget_set_sensitive(self->m_next, FALSE);
			gtk_widget_set_sensitive(self->m_previous, FALSE);
			gtk_widget_set_visible(self->m_play, TRUE);
			gtk_widget_set_visible(self->m_pause, FALSE);
			break;
		case CLGC_PLAYBACK_STATE_PLAY:
			gtk_widget_set_sensitive(self->m_next, TRUE);
			gtk_widget_set_sensitive(self->m_previous, TRUE);
			gtk_widget_set_sensitive(self->m_stop, TRUE);
			gtk_widget_set_visible(self->m_play, FALSE);
			gtk_widget_set_visible(self->m_pause, TRUE);
			break;
		default:
			break;
	}
}

void clgc_playback_commands_set_policy(ClgcPlaybackCommands* self, ClgcPlaybackCommandsPolicy policy)
{
	fprintf(stdout, "[INFO] clgc_playback_commands_set_policy: policy=%d\n", policy);
	g_return_if_fail(CLGC_IS_PLAYBACK_COMMANDS(self));

	if (self->m_policy == policy)
		return;

	self->m_policy = policy;
	g_object_notify_by_pspec(G_OBJECT(self), obj_properties[PROP_POLICY]);
	clgc_playback_commands_update_visual(self, policy);
	gtk_widget_queue_resize(GTK_WIDGET(self));
}
