/*
 * clgc-tag: Calgici Tag class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-tag.h"

#define DEFAULT_ICON_NAME "audio-x-generic-symbolic"

struct _ClgcTag
{
	ClgcTagType m_type;
	char* m_value;
};

static const char* tag_id[CLGC_TAG_MAX] = {
	"title",
	"album",
	"artist",
	"track",
	"time",
	"playing",
	"album-artist",
	"genre",
	"date"
};

static const char* tag_name[CLGC_TAG_MAX] = {
	"Title",
	"Album",
	"Artist",
	"Track",
	"Time",
	"Playing",
	"Album/Artist",
	"Genre",
	"Date"
};

static const char* tag_icon_name[CLGC_TAG_MAX] = {
	DEFAULT_ICON_NAME,
	"media-optical-cd-audio-symbolic",
	"avatar-default-symbolic",
	DEFAULT_ICON_NAME,
	DEFAULT_ICON_NAME,
	DEFAULT_ICON_NAME,
	DEFAULT_ICON_NAME,
	"user-bookmarks-symbolic",
	DEFAULT_ICON_NAME
};

G_DEFINE_BOXED_TYPE(ClgcTag, clgc_tag, clgc_tag_copy, clgc_tag_free);

ClgcTag* clgc_tag_new(ClgcTagType type, const char* value)
{
	ClgcTag* tag = NULL;

	if (type < CLGC_TAG_TITLE || type >= CLGC_TAG_MAX)
		return NULL;

	tag = g_slice_new(ClgcTag);

	tag->m_type = type;

	if (value)
		tag->m_value = strdup(value);
	else
		tag->m_value = NULL;

	return tag;
}

ClgcTag* clgc_tag_build_from_idname(const char* name)
{
	int id;

	for (id = 0; id < CLGC_TAG_MAX; id ++)
		if (g_strcmp0(name, tag_id[id]) == 0)
			break;

	if (id >= CLGC_TAG_MAX)
		return NULL;

	return clgc_tag_new(id, NULL);
}

ClgcTag* clgc_tag_copy(ClgcTag* self)
{
	if (self == NULL)
		return NULL;

	return clgc_tag_new(self->m_type, self->m_value);
}

void clgc_tag_free(ClgcTag* self)
{
	if (self == NULL)
		return;

	free(self->m_value);
	g_slice_free(ClgcTag, self);
}

const char* clgc_tag_get_icon_name(ClgcTag* self)
{
	return clgc_tag_type_get_icon_name(self->m_type);
}

const char* clgc_tag_get_id(ClgcTag* self)
{
	return clgc_tag_type_get_id(self->m_type);
}

ClgcTagType clgc_tag_get_key(ClgcTag* self)
{
	return self->m_type;
}

const char* clgc_tag_get_name(ClgcTag* self)
{
	return clgc_tag_type_get_name(self->m_type);
}

const char* clgc_tag_get_value(ClgcTag* self)
{
	return self->m_value;
}

const char* clgc_tag_type_get_icon_name(ClgcTagType tag)
{
	if (!(tag >= 0 && tag < CLGC_TAG_MAX))
		return DEFAULT_ICON_NAME;

	return tag_icon_name[tag];
}

const char* clgc_tag_type_get_id(ClgcTagType tag)
{
	if (!(tag >= 0 && tag < CLGC_TAG_MAX))
		return "";

	return tag_id[tag];
}

const char* clgc_tag_type_get_name(ClgcTagType tag)
{
	if (!(tag >= 0 && tag < CLGC_TAG_MAX))
		return "";

	return tag_name[tag];
}

ClgcTagType clgc_string_get_type(const char* name)
{

	return -1;
}
