/*
 * clgc-song-row: Calgici SongRow class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-song-row.h"

enum _ClgcSongRowProperty
{
	PROP_0,
	PROP_SONG,
	N_PROPERTIES
};

struct _ClgcSongRow
{
	GtkListBoxRow m_parent;

	ClgcInfoSong* m_song;

	GtkWidget* m_grid;
	GtkWidget* m_stack;
	GtkWidget* m_state;
	GtkWidget* m_button;
	GtkWidget* m_track;
	GtkWidget* m_title;
	GtkWidget* m_artist;
	GtkWidget* m_album;
	GtkWidget* m_duration;
	GtkWidget* m_rate;

};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcSongRow, clgc_song_row, GTK_TYPE_LIST_BOX_ROW)

static void clgc_song_row_constructed(GObject* obj)
{
	ClgcSongRow* self = CLGC_SONG_ROW(obj);

	G_OBJECT_CLASS(clgc_song_row_parent_class)->constructed(obj);

	gtk_label_set_text(GTK_LABEL(self->m_track), clgc_info_song_get_track(self->m_song));
	gtk_label_set_text(GTK_LABEL(self->m_title), clgc_info_song_get_title(self->m_song));
	gtk_label_set_text(GTK_LABEL(self->m_artist), clgc_info_song_get_artist(self->m_song));
	gtk_label_set_text(GTK_LABEL(self->m_album), clgc_info_song_get_album(self->m_song));

}

static void clgc_song_row_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcSongRow* self = CLGC_SONG_ROW(obj);

	switch (property_id)
	{
		case PROP_SONG:
			g_value_set_object(value, self->m_song);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_song_row_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcSongRow* self = CLGC_SONG_ROW(obj);

	switch (property_id)
	{
		case PROP_SONG:
			self->m_song = g_value_get_object(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_song_row_activate(GtkListBoxRow* row)
{
	ClgcSongRow* self = CLGC_SONG_ROW(row);

	fprintf(stdout, "[INFO] clgc_song_row_activate: coucou\n");
}

static void clgc_song_row_class_init(ClgcSongRowClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);
	GtkListBoxRowClass* row_class = GTK_LIST_BOX_ROW_CLASS(klass); 

	obj_class->constructed = clgc_song_row_constructed;
	obj_class->get_property = clgc_song_row_get_property;
	obj_class->set_property = clgc_song_row_set_property;

	row_class->activate = clgc_song_row_activate;

	obj_properties[PROP_SONG] = g_param_spec_object(
		"song",
		"Song",
		"The information of the song",
		CLGC_TYPE_INFO_SONG,
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_EXPLICIT_NOTIFY
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-song-row.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_grid);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_stack);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_state);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_button);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_track);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_title);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_artist);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_album);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_duration);
	gtk_widget_class_bind_template_child(widget_class, ClgcSongRow, m_rate);
}

static void clgc_song_row_init(ClgcSongRow* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
	clgc_song_row_set_mode(CLGC_SONG_ROW(self), CLGC_MODE_NORMAL);
}

GtkWidget* clgc_song_row_new(ClgcInfoSong* song)
{
	return g_object_new(CLGC_TYPE_SONG_ROW, "song", song, NULL);
}

ClgcInfoSong* clgc_song_row_get_song(ClgcSongRow* self)
{
	return self->m_song;
}

int clgc_song_row_is_selected(ClgcSongRow* self)
{
	return gtk_check_button_get_active(GTK_CHECK_BUTTON(self->m_button));
}

void clgc_song_row_set_selected(ClgcSongRow* self, int selected)
{
	gtk_check_button_set_active(GTK_CHECK_BUTTON(self->m_button), selected);
}

void clgc_song_row_set_state(ClgcSongRow* self, ClgcPlaybackState state)
{
	switch (state)
	{
		case CLGC_PLAYBACK_STATE_PLAY:
			gtk_image_set_from_icon_name(GTK_IMAGE(self->m_state), "media-playback-start-symbolic");
			break;
		case CLGC_PLAYBACK_STATE_PAUSE:
			gtk_image_set_from_icon_name(GTK_IMAGE(self->m_state), "media-playback-pause-symbolic");
			break;
		case CLGC_PLAYBACK_STATE_STOP:
			gtk_image_set_from_icon_name(GTK_IMAGE(self->m_state), "media-playback-stop-symbolic");
			break;
		default:
			gtk_image_set_from_icon_name(GTK_IMAGE(self->m_state), "");
			break;
	}
}

void clgc_song_row_set_mode(ClgcSongRow* self, ClgcMode mode)
{
	if (mode == CLGC_MODE_NORMAL)
		gtk_stack_set_visible_child(GTK_STACK(self->m_stack), self->m_state);
	else if (mode == CLGC_MODE_SELECTION)
		gtk_stack_set_visible_child(GTK_STACK(self->m_stack), self->m_button);
}
