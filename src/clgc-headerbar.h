/*
 * clgc-headerbar: Calgici HeaderBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_HEADERBAR__
#define __CALGICI_HEADERBAR__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_HEADERBAR (clgc_headerbar_get_type())

G_DECLARE_FINAL_TYPE(ClgcHeaderBar, clgc_headerbar, CLGC, HEADERBAR, GtkWidget)

/**
 * @brief Creates an instance of headerbar
 * @return An instance of headerbar
 */
GtkWidget* clgc_headerbar_new();

/**
 * @brief Sets the center of the headerbar
 * @param self The headerbar
 * @param center The center to be set
 */
void clgc_headerbar_set_center(ClgcHeaderBar* self, GtkWidget* center);

void clgc_headerbar_set_audio_outputs(ClgcHeaderBar* self, GList* list);

G_END_DECLS

#endif // __CALGICI_HEADERBAR__
