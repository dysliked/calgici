/*
 * clgc-headerbar: Calgici HeaderBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-headerbar.h"

#include "clgc-option-button.h"

#include <adwaita.h>
#include <libintl.h>
#include <stdio.h>

enum _ClgcHeaderBarProperty
{
	PROP_0,
	PROP_SHOW_MAIN_BUTTONS,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcHeaderBar
{
	GtkWidget m_parent;

	GtkWidget* m_bar;
	GtkWidget* m_select_button;
	GtkWidget* m_option_button;
};

static GtkBuildableIface* parent_buildable_iface;

static void clgc_headerbar_buildable_init(GtkBuildableIface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcHeaderBar, clgc_headerbar, GTK_TYPE_WIDGET, G_IMPLEMENT_INTERFACE(GTK_TYPE_BUILDABLE, clgc_headerbar_buildable_init))

static void clgc_headerbar_set_show_main_buttons(ClgcHeaderBar* self, gboolean setting)
{
	if (!setting)
		return;

	self->m_option_button = clgc_option_button_new();
	self->m_select_button = gtk_button_new_from_icon_name("selection-mode-symbolic");

	adw_header_bar_pack_end(ADW_HEADER_BAR(self->m_bar), self->m_option_button);
	adw_header_bar_pack_end(ADW_HEADER_BAR(self->m_bar), self->m_select_button);

	gtk_widget_set_tooltip_text(self->m_select_button, gettext("Select items"));
	gtk_actionable_set_action_name(GTK_ACTIONABLE(self->m_select_button), "win.selection-mode");
}

static void clgc_headerbar_dispose(GObject* obj)
{
	ClgcHeaderBar* self = CLGC_HEADERBAR(obj);

	g_clear_pointer(&(self->m_bar), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_headerbar_parent_class)->dispose(obj);
}

static void clgc_headerbar_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcHeaderBar* self = CLGC_HEADERBAR(obj);

	switch (property_id)
	{
		case PROP_SHOW_MAIN_BUTTONS:
			g_value_set_boolean(value, self->m_option_button != NULL && self->m_select_button != NULL);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_headerbar_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcHeaderBar* self = CLGC_HEADERBAR(obj);

	switch (property_id)
	{
		case PROP_SHOW_MAIN_BUTTONS:
			clgc_headerbar_set_show_main_buttons(self, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_headerbar_class_init(ClgcHeaderBarClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_headerbar_dispose;
	obj_class->get_property = clgc_headerbar_get_property;
	obj_class->set_property = clgc_headerbar_set_property;

	obj_properties[PROP_SHOW_MAIN_BUTTONS] = g_param_spec_boolean("show-main-buttons", "Show main buttons", "Whether to show main buttons", FALSE, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void clgc_headerbar_init(ClgcHeaderBar* self)
{
	self->m_bar = adw_header_bar_new();
	self->m_option_button = NULL;
	self->m_select_button = NULL;

	gtk_widget_set_parent(self->m_bar, GTK_WIDGET(self));

	adw_header_bar_set_show_end_title_buttons(ADW_HEADER_BAR(self->m_bar), TRUE);
	adw_header_bar_set_centering_policy(ADW_HEADER_BAR(self->m_bar), ADW_CENTERING_POLICY_STRICT);
}

static void clgc_headerbar_buildable_add_child(GtkBuildable* buildable, GtkBuilder* builder, GObject* child, const char* type)
{
	if (g_strcmp0(type, "title") == 0)
		clgc_headerbar_set_center(CLGC_HEADERBAR(buildable), GTK_WIDGET(child));
	else
		parent_buildable_iface->add_child(buildable, builder, child, type);
}

static void clgc_headerbar_buildable_init(GtkBuildableIface* iface)
{
	parent_buildable_iface = g_type_interface_peek_parent(iface);

	iface->add_child = clgc_headerbar_buildable_add_child;
}

GtkWidget* clgc_headerbar_new()
{
	return g_object_new(CLGC_TYPE_HEADERBAR, NULL);
}

void clgc_headerbar_set_center(ClgcHeaderBar* self, GtkWidget* title)
{
	adw_header_bar_set_title_widget(ADW_HEADER_BAR(self->m_bar), title);
}

void clgc_headerbar_set_audio_outputs(ClgcHeaderBar* self, GList* list)
{
	clgc_option_button_set_audio_outputs(CLGC_OPTION_BUTTON(self->m_option_button), list);
}
