/*
 * clgc-medium-widget: Calgici MediumWidget class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-medium-widget.h"

#include "clgc-tag-art.h"

enum _ClgcMediumWidgetProperty
{
	PROP_TAG = 1,
	PROP_NAME,
/*	PROP_FILENAME,*/
	N_PROPERTIES
};

struct _ClgcMediumWidget
{
	GtkWidget m_parent;

	int m_tag;

	GtkWidget* m_box;
	GtkWidget* m_art;
	GtkWidget* m_label;
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcMediumWidget, clgc_medium_widget, GTK_TYPE_WIDGET)

static void clgc_medium_widget_constructed(GObject* obj)
{
	ClgcMediumWidget* self = CLGC_MEDIUM_WIDGET(obj);

	clgc_tag_art_set_tag(CLGC_TAG_ART(self->m_art), self->m_tag);
}

static void clgc_medium_widget_dispose(GObject* obj)
{
	ClgcMediumWidget* self = CLGC_MEDIUM_WIDGET(obj);

	g_clear_pointer(&(self->m_box), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_medium_widget_parent_class)->dispose(obj);
}

static void clgc_medium_widget_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcMediumWidget* self = CLGC_MEDIUM_WIDGET(obj);

	switch (property_id)
	{
		case PROP_TAG:
			g_value_set_int(value, self->m_tag);
			break;
		case PROP_NAME:
			g_value_set_string(value, clgc_medium_widget_get_name(self));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_medium_widget_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcMediumWidget* self = CLGC_MEDIUM_WIDGET(obj);

	switch (property_id)
	{
		case PROP_TAG:
			self->m_tag = g_value_get_int(value);
			break;
		case PROP_NAME:
			gtk_label_set_text(GTK_LABEL(self->m_label), g_value_get_string(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_medium_widget_class_init(ClgcMediumWidgetClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->constructed = clgc_medium_widget_constructed;
	obj_class->dispose = clgc_medium_widget_dispose;
	obj_class->get_property = clgc_medium_widget_get_property;
	obj_class->set_property = clgc_medium_widget_set_property;

	obj_properties[PROP_TAG] = g_param_spec_int("tag", "Tag", "The tag of the specific tag widget", -1, CLGC_TAG_MAX, -1, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
	obj_properties[PROP_NAME] = g_param_spec_string("name", "Name", "The name of the medium widget", NULL, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-medium-widget.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumWidget, m_box);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumWidget, m_art);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumWidget, m_label);

	g_type_ensure(CLGC_TYPE_TAG_ART);
}

static void clgc_medium_widget_init(ClgcMediumWidget* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));

	gtk_label_set_max_width_chars(GTK_LABEL(self->m_label), SIZE_WIDGET / 9);
}

GtkWidget* clgc_medium_widget_new(int tag, const char* name)
{
	return g_object_new(CLGC_TYPE_MEDIUM_WIDGET, "tag", tag, "name", name, NULL);
}

const char* clgc_medium_widget_get_name(ClgcMediumWidget* self)
{
	return gtk_label_get_text(GTK_LABEL(self->m_label));
}

void clgc_medium_widget_set_image(ClgcMediumWidget* self, const char* filename)
{
	clgc_tag_art_set_image(CLGC_TAG_ART(self->m_art), filename);
}
