/*
 * clgc-panel: Calgici Panel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PANEL__
#define __CALGICI_PANEL__

#include "clgc-mode.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define CLGC_TYPE_PANEL (clgc_panel_get_type())

G_DECLARE_INTERFACE(ClgcPanel, clgc_panel, CLGC, PANEL, GObject)

struct _ClgcPanelInterface
{
	GTypeInterface m_parent;

	void (*set_mode)(ClgcPanel*, ClgcMode);

	void (*selection_all)(ClgcPanel*);
	void (*selection_change)(ClgcPanel*, int);
	void (*selection_none)(ClgcPanel*);
	void (*send_add)(ClgcPanel*);
	void (*send_remove)(ClgcPanel*);
};

/**
 * @brief Disable the selection mode for the panel
 * @param self The panel
 */
void clgc_panel_set_mode(ClgcPanel* self, ClgcMode mode);

/**
 * @brief Manage when all items must be selected
 * @param self The panel
 */
void clgc_panel_selection_all(ClgcPanel* self);

/**
 * @brief Manage when the selection change
 * @param self The panel
 */
void clgc_panel_selection_change(ClgcPanel* self, int selected);

/**
 * @brief Manage when all items must be unselected
 * @param self The panel
 */
void clgc_panel_selection_none(ClgcPanel* self);

/**
 * @brief Add the selection in a list
 * @param self The panel
 */
void clgc_panel_send_add(ClgcPanel* self);

/**
 * @brief Remove the selection in a list
 * @param self The panel
 */
void clgc_panel_send_remove(ClgcPanel* self);

G_END_DECLS

#endif // __CALGICI_PANEL__
