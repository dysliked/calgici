/*
 * clgc-playback-state: Calgici PlaybackState class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYBACK_STATE__
#define __CALGICI_PLAYBACK_STATE__

typedef enum _ClgcPlaybackState
{
	CLGC_PLAYBACK_STATE_PAUSE,
	CLGC_PLAYBACK_STATE_PLAY,
	CLGC_PLAYBACK_STATE_STOP,
	CLGC_PLAYBACK_STATE_MAX
} ClgcPlaybackState;

#endif // __CALGICI_PLAYBACK_STATE__
