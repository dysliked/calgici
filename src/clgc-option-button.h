/*
 * clgc-option-button: Calgici OptionButton class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_OPTION_BUTTON__
#define __CALGICI_OPTION_BUTTON__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_OPTION_BUTTON (clgc_option_button_get_type())

G_DECLARE_FINAL_TYPE(ClgcOptionButton, clgc_option_button, CLGC, OPTION_BUTTON, GtkWidget)

GtkWidget* clgc_option_button_new();

GList* clgc_option_button_get_audio_outputs(ClgcOptionButton* self);

void clgc_option_button_set_audio_outputs(ClgcOptionButton* self, GList* list);

G_END_DECLS

#endif // __CALGICI_OPTION_BUTTON__
