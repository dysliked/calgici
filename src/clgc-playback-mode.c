/*
 * clgc-playback-mode: Calgici PlaybackMode class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-playback-mode.h"

struct _ClgcPlaybackMode
{
	GtkWidget m_parent;

	GtkWidget* m_menu_button;
};

G_DEFINE_TYPE(ClgcPlaybackMode, clgc_playback_mode, GTK_TYPE_WIDGET)

static void clgc_playback_mode_constructed(GObject* obj)
{
	ClgcPlaybackMode* self = CLGC_PLAYBACK_MODE(obj);

	G_OBJECT_CLASS(clgc_playback_mode_parent_class)->constructed(obj);

	clgc_playback_mode_set_status(self, CLGC_PLAYBACK_STATUS_NONE);
}

static void clgc_playback_mode_dispose(GObject* obj)
{
	ClgcPlaybackMode* self = CLGC_PLAYBACK_MODE(obj);

	g_clear_pointer(&(self->m_menu_button), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_playback_mode_parent_class)->dispose(obj);
}


static void clgc_playback_mode_class_init(ClgcPlaybackModeClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->constructed = clgc_playback_mode_constructed;
	obj_class->dispose = clgc_playback_mode_dispose;

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-playback-mode.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcPlaybackMode, m_menu_button);
}

static void clgc_playback_mode_init(ClgcPlaybackMode* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_playback_mode_new()
{
	return g_object_new(CLGC_TYPE_PLAYBACK_MODE, NULL);
}

void clgc_playback_mode_set_status(ClgcPlaybackMode* self, ClgcPlaybackStatus status)
{
	char* icon_name = NULL;

	if (status & CLGC_PLAYBACK_STATUS_REPEAT && status & CLGC_PLAYBACK_STATUS_SINGLE)
		icon_name = "media-playlist-repeat-song-symbolic";
	else if (status & CLGC_PLAYBACK_STATUS_RANDOM)
		icon_name = "media-playlist-shuffle-symbolic";
	else if (status & CLGC_PLAYBACK_STATUS_REPEAT)
		icon_name = "media-playlist-repeat-symbolic";
	else if (status & CLGC_PLAYBACK_STATUS_SINGLE)
		icon_name = "zoom-original-symbolic";
	else
		icon_name = "media-playlist-consecutive-symbolic";

	gtk_menu_button_set_icon_name(GTK_MENU_BUTTON(self->m_menu_button), icon_name);
}
