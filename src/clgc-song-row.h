/*
 * clgc-song-row: Calgici SongRow class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_SONG_ROW__
#define __CALGICI_SONG_ROW__

#include "clgc-info-song.h"
#include "clgc-mode.h"
#include "clgc-playback-state.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_SONG_ROW (clgc_song_row_get_type())

G_DECLARE_FINAL_TYPE(ClgcSongRow, clgc_song_row, CLGC, SONG_ROW, GtkListBoxRow)

/**
 * @brief Constructor
 * @param id
 * @param path
 * @param track
 * @param title
 * @param artist
 * @param album
 * @param duration
 */
GtkWidget* clgc_song_row_new(ClgcInfoSong* song);

void clgc_song_row_add_in_variant(ClgcSongRow* self, GVariantBuilder* variant);

/**
 * @brief Gets the song's id
 * @param self The song row
 * @return The song's id
 */
ClgcInfoSong* clgc_song_row_get_song(ClgcSongRow* self);

/**
 * @brief Gets if the row is selected
 * @param self
 * @return 1 if the row is selected, 0 otherwise
 */
int clgc_song_row_is_selected(ClgcSongRow* self);

/**
 * @brief Select the row
 * @param self
 * @param selected
 */
void clgc_song_row_set_selected(ClgcSongRow* self, int selected);

/**
 * @brief Sets the state for the song
 * @param self The song row
 * @param status The state
 */
void clgc_song_row_set_state(ClgcSongRow* self, ClgcPlaybackState state);

/**
 * @brief Sets the mode for the row
 * @param self The row
 * @param mode The mode
 */
void clgc_song_row_set_mode(ClgcSongRow* self, ClgcMode mode);

G_END_DECLS

#endif // __CALGICI_SONG_ROW__
