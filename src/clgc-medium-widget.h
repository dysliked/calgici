/*
 * clgc-medium-widget: Calgici MediumWidget class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_MEDIUM_WIDGET__
#define __CALGICI_MEDIUM_WIDGET__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_MEDIUM_WIDGET (clgc_medium_widget_get_type())

G_DECLARE_FINAL_TYPE(ClgcMediumWidget, clgc_medium_widget, CLGC, MEDIUM_WIDGET, GtkWidget)

/**
 * @brief Gets the name for the medium widget
 * @param self
 */
const char* clgc_medium_widget_get_name(ClgcMediumWidget* self);

/**
 * @brief Sets the image for the medium widget
 * @param self
 * @param filename
 */
void clgc_medium_widget_set_image(ClgcMediumWidget* self, const char* filename);

/**
 * @brief Create an instance of medium widget
 * @param name The name of the medium widget
 * @return An instance of medium widget
 */
GtkWidget* clgc_medium_widget_new(int tag, const char* name);

#endif // __CALGICI_MEDIUM_WIDGET__
