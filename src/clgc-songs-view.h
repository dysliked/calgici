/*
 * clgc-songs-view: Calgici SongsView class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_SONGS_VIEW__
#define __CALGICI_SONGS_VIEW__

#include "clgc-info-song.h"
#include "clgc-mode.h"
#include "clgc-playback-state.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef enum _ClgcSongsViewAction
{
	CLGC_SONGS_VIEW_NONE,
	CLGC_SONGS_VIEW_PLAY,
	CLGC_SONGS_VIEW_ADD,
	CLGC_SONGS_VIEW_MAX
} ClgcSongsViewAction;

#define CLGC_TYPE_SONGS_VIEW (clgc_songs_view_get_type())

G_DECLARE_FINAL_TYPE(ClgcSongsView, clgc_songs_view, CLGC, SONGS_VIEW, GtkWidget)

/**
 * @brief Constructor
 * @param action The action to do when the user double clicks
 * @return New instance
 */
GtkWidget* clgc_songs_view_new(ClgcSongsViewAction action);

/**
 * @brief Append a song in the list
 * @param self The songs view
 * @param song The song to append
 */
void clgc_songs_view_append(ClgcSongsView* self, ClgcInfoSong* song);

/**
 * @brief Clear the list
 * @param self The songs view
 */
void clgc_songs_view_clear(ClgcSongsView* self);

/**
 * @brief Gets the number of selected row
 * @param self The songs view
 * @return The number of selected row
 */
int clgc_songs_view_get_nb_selected(ClgcSongsView* self);

/**
 * @brief Send an action to application to add selected songs in a playlist
 * @param self The songs view
 */
void clgc_songs_view_send_add_songs(ClgcSongsView* self);

/**
 * @brief Send an action to application to remove selected songs in a playlist
 * @param self The songs view
 */
void clgc_songs_view_send_remove_songs(ClgcSongsView* self);

/**
 * @brief Update the current playing song
 * @param self The songs view
 */
void clgc_songs_view_set_current_song(ClgcSongsView* self, ClgcInfoSong* song, ClgcPlaybackState state);

/**
 * @brief Set the view in normal mode
 * @param self The songs view
 */
void clgc_songs_view_set_mode(ClgcSongsView* self, ClgcMode mode);

/**
 * @brief Selects all elements in the list
 * @param self The songs view
 */
void clgc_songs_view_set_selected(ClgcSongsView* self, int selected);

#endif // __CALGICI_SONGS_VIEW__
