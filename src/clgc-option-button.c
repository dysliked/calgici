/*
 * clgc-option-button: Calgici OptionButton class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-option-button.h"

#include "clgc-audio-output.h"

#include <libintl.h>

enum _ClgcOptionButtonProperty
{
	PROP_0,
	PROP_AUDIO_OUTPUTS,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcOptionButton
{
	GtkWidget m_parent;

	GMenu* m_audio_outputs_menu;

	GtkWidget* m_button;
};

G_DEFINE_TYPE(ClgcOptionButton, clgc_option_button, GTK_TYPE_WIDGET)

static GMenu* build_server_menu()
{
	GMenu* menu = g_menu_new();
	GMenuItem* item = NULL;

	item = g_menu_item_new(gettext("Connect to a server"), "win.connect-server");
	g_menu_append_item(menu, item);

	item = g_menu_item_new(gettext("Server information"), "win.show-server-information");
	g_menu_append_item(menu, item);

	return menu;
}

static GMenu* build_other_menu()
{
	GMenu* menu = g_menu_new();
	GMenuItem* item = NULL;

	item = g_menu_item_new(gettext("Preferences"), "win.show-preferences");
	g_menu_append_item(menu, item);

	item = g_menu_item_new(gettext("Keyboard Shortcuts"), "win.show-help-overlay");
	g_menu_append_item(menu, item);

	item = g_menu_item_new(gettext("About MPD Client"), "win.about");
	g_menu_append_item(menu, item);

	return menu;
}

static void clgc_option_button_build_menu(ClgcOptionButton* self)
{
	GMenu* menu = g_menu_new();
	GMenuItem* item = NULL;

	GMenu* action_menu = g_menu_new();

	self->m_audio_outputs_menu = g_menu_new();
	g_menu_append_submenu(action_menu, gettext("Audio outputs"), G_MENU_MODEL(self->m_audio_outputs_menu));

	item = g_menu_item_new(gettext("Update database"), "app.mpd-client.update-database");
	g_menu_append_item(action_menu, item);

	g_menu_append_section(menu, NULL, G_MENU_MODEL(build_server_menu()));
	g_menu_append_section(menu, NULL, G_MENU_MODEL(action_menu));
	g_menu_append_section(menu, NULL, G_MENU_MODEL(build_other_menu()));

	gtk_menu_button_set_menu_model(GTK_MENU_BUTTON(self->m_button), G_MENU_MODEL(menu));
}

static void clgc_option_button_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcOptionButton* self = CLGC_OPTION_BUTTON(obj);

	switch (property_id)
	{
		case PROP_AUDIO_OUTPUTS:
			g_value_set_object(value, clgc_option_button_get_audio_outputs(self));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_option_button_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcOptionButton* self = CLGC_OPTION_BUTTON(obj);

	switch (property_id)
	{
		case PROP_AUDIO_OUTPUTS:
			clgc_option_button_set_audio_outputs(self, (GList*) g_value_get_pointer(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_option_button_dispose(GObject* obj)
{
	ClgcOptionButton* self = CLGC_OPTION_BUTTON(obj);

	g_clear_pointer(&(self->m_button), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_option_button_parent_class)->dispose(obj);
}

static void clgc_option_button_class_init(ClgcOptionButtonClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_option_button_dispose;
	obj_class->get_property = clgc_option_button_get_property;
	obj_class->set_property = clgc_option_button_set_property;

	obj_properties[PROP_AUDIO_OUTPUTS] = g_param_spec_pointer("audio-outputs", "Audio outputs", "Audio outputs", G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void clgc_option_button_init(ClgcOptionButton* self)
{
	self->m_button = gtk_menu_button_new();

	gtk_widget_set_parent(self->m_button, GTK_WIDGET(self));

	clgc_option_button_build_menu(self);
	gtk_menu_button_set_icon_name(GTK_MENU_BUTTON(self->m_button), "open-menu-symbolic");
}

GtkWidget* clgc_option_button_new()
{
	return g_object_new(CLGC_TYPE_OPTION_BUTTON, NULL);
}

GList* clgc_option_button_get_audio_outputs(ClgcOptionButton* self)
{
	return NULL;
}

static void clgc_option_button_set_audio_outputs_aux(gpointer data, gpointer user_data)
{
	ClgcAudioOutput* audio_output = CLGC_AUDIO_OUTPUT(data);
	GMenu* menu = G_MENU(user_data);
	gchar* action_name = g_strdup_printf("app.%s", clgc_audio_output_get_action_name(audio_output));

	GMenuItem* item = g_menu_item_new(clgc_audio_output_get_name(audio_output), action_name);

	g_menu_append_item(menu, item);
	g_free(action_name);
}

void clgc_option_button_set_audio_outputs(ClgcOptionButton* self, GList* list)
{
	g_menu_remove_all(self->m_audio_outputs_menu);
	g_list_foreach(list, clgc_option_button_set_audio_outputs_aux, self->m_audio_outputs_menu);
}
