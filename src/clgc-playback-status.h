/*
 * clgc-playback-status: Calgici PlaybackStatus class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYBACK_STATUS
#define __CALGICI_PLAYBACK_STATUS

typedef enum _ClgcPlaybackStatus
{
	CLGC_PLAYBACK_STATUS_NONE = 0x0,
	CLGC_PLAYBACK_STATUS_RANDOM = 0x1,
	CLGC_PLAYBACK_STATUS_REPEAT = 0x2,
	CLGC_PLAYBACK_STATUS_SINGLE = 0x4
} ClgcPlaybackStatus;

#endif // __CALGICI_PLAYBACK_STATUS
