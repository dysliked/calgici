/*
 * clgc-medium-page: Calgici MediumPage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_MEDIUM_PAGE__
#define __CALGICI_MEDIUM_PAGE__

#include "clgc-page.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_MEDIUM_PAGE (clgc_medium_page_get_type())

G_DECLARE_FINAL_TYPE(ClgcMediumPage, clgc_medium_page, CLGC, MEDIUM_PAGE, GtkWidget)

/**
 * @brief Creates an instance of specific tag page
 * @return An instance of medium page
 */
GtkWidget* clgc_medium_page_new();

/**
 * @brief Updates the content of the medium
 * @param self The medium page
 * @param tag The tag's number
 * @param value The name of the tag
 */
void clgc_medium_page_update(ClgcMediumPage* self, GList* list);

G_END_DECLS

#endif // __CALGICI_MEDIUM_PAGE__
