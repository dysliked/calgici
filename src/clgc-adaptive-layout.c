/*
 * clgc-adaptive-layout: Calgici AdaptiveLayout class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-adaptive-layout.h"

#include "clgc-adaptive-box.h"
#include "clgc-widget-private.h"

enum _ClgcAdaptiveLayoutId
{
	CLGC_ADAPTIVE_LAYOUT_START,
	CLGC_ADAPTIVE_LAYOUT_MAIN,
	CLGC_ADAPTIVE_LAYOUT_SECONDARY,
	CLGC_ADAPTIVE_LAYOUT_END,
	CLGC_ADAPTIVE_LAYOUT_MAX
};

enum _ClgcAdaptiveLayoutProperty
{
	PROP_0,
	PROP_SPACING,

	/* Overriden properties*/
	PROP_ORIENTATION,

	N_PROPERTIES = PROP_ORIENTATION
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcAdaptiveLayout
{
	GtkLayoutManager m_parent;

	int m_spacing;
	GtkOrientation m_orientation;
};

static GtkWidget* (*GET_CHILD[CLGC_ADAPTIVE_LAYOUT_MAX])(ClgcAdaptiveBox*) = {
	clgc_adaptive_box_get_start_widget,
	clgc_adaptive_box_get_main_widget,
	clgc_adaptive_box_get_secondary_widget,
	clgc_adaptive_box_get_end_widget
};

G_DEFINE_TYPE_WITH_CODE(ClgcAdaptiveLayout, clgc_adaptive_layout, GTK_TYPE_LAYOUT_MANAGER, G_IMPLEMENT_INTERFACE(GTK_TYPE_ORIENTABLE, NULL))

static void clgc_adaptive_layout_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	ClgcAdaptiveLayout* self = CLGC_ADAPTIVE_LAYOUT(object);

	switch (prop_id)
	{
		case PROP_SPACING:
			g_value_set_int(value, self->m_spacing);
			break;
		case PROP_ORIENTATION:
			g_value_set_enum(value, self->m_orientation);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_adaptive_layout_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	ClgcAdaptiveLayout* self = CLGC_ADAPTIVE_LAYOUT(object);

	switch (prop_id)
	{
		case PROP_SPACING:
			clgc_adaptive_layout_set_spacing(self, g_value_get_int(value));
			break;
		case PROP_ORIENTATION:
			clgc_adaptive_layout_set_orientation(self, g_value_get_enum(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static GtkSizeRequestMode clgc_adaptive_layout_get_request_mode(GtkLayoutManager* layout, GtkWidget* widget)
{
	int id;
	int count[3] = {0, };
	GtkWidget* child = NULL;

	for (id = 0; id < CLGC_ADAPTIVE_LAYOUT_MAX; id ++)
		if (!(child = GET_CHILD[id](CLGC_ADAPTIVE_BOX(widget))))
			count[gtk_widget_get_request_mode(child)]++;

	if (!count[GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH] && !count[GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT])
		return GTK_SIZE_REQUEST_CONSTANT_SIZE;

	return count[GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT] > count[GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH] ? GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT : GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;
}

static void clgc_adaptive_layout_measure(GtkLayoutManager* layout, GtkWidget* widget, GtkOrientation orientation, int for_size, int* minimum, int* natural, int* minimum_baseline, int* natural_baseline)
{
	GtkWidget* child = NULL;
	int min = -1;
	int nat = -1;
	int tmp_min, tmp_nat;

	if (orientation == GTK_ORIENTATION_VERTICAL)
	{
		for (child = gtk_widget_get_first_child(widget); child != NULL; child = gtk_widget_get_next_sibling(child))
		{
			if (!child)
				continue;

			tmp_min = 0;
			tmp_nat = 0;

			gtk_widget_measure(child, orientation, for_size, &tmp_min, &tmp_nat, NULL, NULL);

			min = MAX(tmp_min, min);
			nat = MAX(tmp_nat, nat);
		}
	}

	if (minimum)
		*minimum = min;
	if (natural)
		*natural = nat;
	if (minimum_baseline)
		*minimum_baseline = -1;
	if (natural_baseline)
		*natural_baseline = -1;
}

static void clgc_adaptive_layout_allocate(GtkLayoutManager *layout, GtkWidget* widget, int width, int height, int baseline)
{
	ClgcAdaptiveLayout* self = CLGC_ADAPTIVE_LAYOUT(layout);

	int id;
	int size, start_width;
	GtkWidget* child = NULL;
	GtkAllocation child_allocation;
	GtkRequestedSize sizes_width[CLGC_ADAPTIVE_LAYOUT_MAX];

	for (id = 0; id < CLGC_ADAPTIVE_LAYOUT_MAX; id ++)
	{
		if (!(child = GET_CHILD[id](CLGC_ADAPTIVE_BOX(widget))))
			continue;

		sizes_width[id].minimum_size = 0;
		sizes_width[id].natural_size = 0;

		gtk_widget_measure(child, GTK_ORIENTATION_HORIZONTAL, -1, &(sizes_width[id].minimum_size), &(sizes_width[id].natural_size), NULL, NULL);
	}

	size = width - sizes_width[CLGC_ADAPTIVE_LAYOUT_END].natural_size;
	start_width = MAX(sizes_width[CLGC_ADAPTIVE_LAYOUT_START].minimum_size, width / 7);

	child_allocation.x = 0;
	child_allocation.y = 0;
	child_allocation.width = start_width;
	child_allocation.height = height;
	gtk_widget_size_allocate(GET_CHILD[CLGC_ADAPTIVE_LAYOUT_START](CLGC_ADAPTIVE_BOX(widget)), &child_allocation, baseline);

	gtk_widget_measure(GET_CHILD[CLGC_ADAPTIVE_LAYOUT_START](CLGC_ADAPTIVE_BOX(widget)), GTK_ORIENTATION_HORIZONTAL, -1, NULL, &start_width, NULL, NULL);

	child_allocation.x = start_width + self->m_spacing;
	child_allocation.y = 0;
	child_allocation.width = size - child_allocation.x - self->m_spacing;
	child_allocation.height = height;
	gtk_widget_size_allocate(GET_CHILD[CLGC_ADAPTIVE_LAYOUT_MAIN](CLGC_ADAPTIVE_BOX(widget)), &child_allocation, baseline);

	child_allocation.x = child_allocation.x + child_allocation.width + self->m_spacing;
	child_allocation.y = 0;
	child_allocation.width = 24;
	child_allocation.height = height;
	gtk_widget_size_allocate(GET_CHILD[CLGC_ADAPTIVE_LAYOUT_SECONDARY](CLGC_ADAPTIVE_BOX(widget)), &child_allocation, baseline);

	child_allocation.x = size;
	child_allocation.y = 0;
	child_allocation.width = sizes_width[CLGC_ADAPTIVE_LAYOUT_END].natural_size;
	child_allocation.height = height;
	gtk_widget_size_allocate(GET_CHILD[CLGC_ADAPTIVE_LAYOUT_END](CLGC_ADAPTIVE_BOX(widget)), &child_allocation, baseline);
}

static void clgc_adaptive_layout_class_init(ClgcAdaptiveLayoutClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkLayoutManagerClass *layout_class = GTK_LAYOUT_MANAGER_CLASS (klass);

	obj_class->get_property = clgc_adaptive_layout_get_property;
	obj_class->set_property = clgc_adaptive_layout_set_property;

	layout_class->get_request_mode = clgc_adaptive_layout_get_request_mode;
	layout_class->measure = clgc_adaptive_layout_measure;
	layout_class->allocate = clgc_adaptive_layout_allocate;

	obj_properties[PROP_SPACING] = g_param_spec_int("spacing","Spacing", "The amount of space between children", 0, G_MAXINT, 0, G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
	g_object_class_override_property(obj_class, PROP_ORIENTATION, "orientation");
}

static void clgc_adaptive_layout_init(ClgcAdaptiveLayout* self)
{
	self->m_orientation = GTK_ORIENTATION_HORIZONTAL;
	self->m_spacing = 10;
}

GtkLayoutManager* clgc_adaptive_layout_new()
{
	return g_object_new(CLGC_TYPE_ADAPTIVE_LAYOUT, "orientation", GTK_ORIENTATION_HORIZONTAL, NULL);
}

int clgc_adaptive_layout_get_spacing(ClgcAdaptiveLayout* self)
{
	return self->m_spacing;
}

void clgc_adaptive_layout_set_orientation(ClgcAdaptiveLayout* self, GtkOrientation orientation)
{
	GtkWidget* widget = NULL;

	if (orientation == self->m_orientation)
		return;

	self->m_orientation = orientation;
	widget = gtk_layout_manager_get_widget(GTK_LAYOUT_MANAGER(self));

	if (widget != NULL && GTK_IS_ORIENTABLE(widget))
		clgc_widget_update_orientation(widget, self->m_orientation);

	gtk_layout_manager_layout_changed(GTK_LAYOUT_MANAGER(self));
	g_object_notify(G_OBJECT(self), "orientation");
}

void clgc_adaptive_layout_set_spacing(ClgcAdaptiveLayout* self, int spacing)
{
	if (self->m_spacing == spacing)
		return;

	self->m_spacing = spacing;

	gtk_layout_manager_layout_changed(GTK_LAYOUT_MANAGER(self));
	g_object_notify_by_pspec(G_OBJECT(self), obj_properties[PROP_SPACING]);
}
