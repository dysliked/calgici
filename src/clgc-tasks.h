/*
 * clgc-tasks: Calgici Tasks class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TASK__
#define __CALGICI_TASK__

#include "clgc-task-data.h"

#include <gio/gio.h>

void clgc_tasks_connect_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_get_data_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_get_data_with_arg_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_get_enum_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_send_command_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_send_command_with_arg_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_send_command_with_enum_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

void clgc_tasks_send_command_with_2_enums_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable);

#endif // __CALGICI_TASK__
