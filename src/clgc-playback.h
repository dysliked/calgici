/*
 * clgc-playback: Calgici Playback class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYBACK__
#define __CALGICI_PLAYBACK__

#include "clgc-info-song.h"
#include "clgc-playback-state.h"


G_BEGIN_DECLS

#define CLGC_TYPE_PLAYBACK (clgc_playback_get_type())

G_DECLARE_FINAL_TYPE(ClgcPlayback, clgc_playback, CLGC, PLAYBACK, GObject)

ClgcPlayback* clgc_playback_new(ClgcInfoSong* song, ClgcPlaybackState state);

ClgcInfoSong* clgc_playback_get_song(ClgcPlayback* self);

ClgcPlaybackState clgc_playback_get_state(ClgcPlayback* self);

G_END_DECLS

#endif // __CALGICI_PLAYBACK__
