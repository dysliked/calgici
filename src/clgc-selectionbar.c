/*
 * clgc-selectionbar: Calgici SelectionBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-selectionbar.h"

#include <adwaita.h>
#include <libintl.h>

#define BUTTON_SELECT_LABEL_DEFAULT gettext("Click on items to select them")

struct _ClgcSelectionBar
{
	AdwBin m_parent;

	GtkWidget* m_bar;
	GtkWidget* m_select_button;
	GtkWidget* m_cancel_button;
};

G_DEFINE_TYPE(ClgcSelectionBar, clgc_selectionbar, GTK_TYPE_WIDGET)

static void clgc_selectionbar_dispose(GObject* obj)
{
	ClgcSelectionBar* self = CLGC_SELECTIONBAR(obj);

	g_clear_pointer(&(self->m_bar), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_selectionbar_parent_class)->dispose(obj);
}

static void clgc_selectionbar_class_init(ClgcSelectionBarClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_selectionbar_dispose;

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-selectionbar.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcSelectionBar, m_bar);
	gtk_widget_class_bind_template_child(widget_class, ClgcSelectionBar, m_select_button);
	gtk_widget_class_bind_template_child(widget_class, ClgcSelectionBar, m_cancel_button);
}

static void clgc_selectionbar_init(ClgcSelectionBar* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));

	gtk_menu_button_set_label(GTK_MENU_BUTTON(self->m_select_button), BUTTON_SELECT_LABEL_DEFAULT);
}

GtkWidget* clgc_selectionbar_new()
{
	return g_object_new(CLGC_TYPE_SELECTIONBAR, NULL);
}

void clgc_selectionbar_selection_change(ClgcSelectionBar* self, int selected)
{
	if (selected <= 0)
	{
		gtk_menu_button_set_label(GTK_MENU_BUTTON(self->m_select_button), BUTTON_SELECT_LABEL_DEFAULT);
		return;
	}

	gtk_menu_button_set_label(GTK_MENU_BUTTON(self->m_select_button), g_strdup_printf(ngettext("%d selected", "%d selected", selected), selected));
}
