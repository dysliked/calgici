/*
 * clgc-info-song: Calgici InfoSong class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_INFO_SONG__
#define __CALGICI_INFO_SONG__

#include <glib-object.h>

G_BEGIN_DECLS

#define CLGC_TYPE_INFO_SONG (clgc_info_song_get_type())

G_DECLARE_FINAL_TYPE(ClgcInfoSong, clgc_info_song, CLGC, INFO_SONG, GObject)

ClgcInfoSong* clgc_info_song_new(unsigned id, const char* title, const char* album, const char* artist, const char* track, GDateTime* time, const char* path);

unsigned clgc_info_song_get_id(ClgcInfoSong* self);

const char* clgc_info_song_get_title(ClgcInfoSong* self);

const char* clgc_info_song_get_album(ClgcInfoSong* self);

const char* clgc_info_song_get_artist(ClgcInfoSong* self);

const char* clgc_info_song_get_track(ClgcInfoSong* self);

GDateTime* clgc_info_song_get_time(ClgcInfoSong* self);

const char* clgc_info_song_get_path(ClgcInfoSong* self);

G_END_DECLS

#endif // __CALGICI_INFO_SONG__
