/*
 * clgc-tag-art: Calgici TagArt class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-tag-art.h"

struct _ClgcTagArt
{
	GtkWidget m_parent;

	int m_tag;
	GtkWidget* m_frame;
	GtkWidget* m_image;
};

G_DEFINE_TYPE(ClgcTagArt, clgc_tag_art, GTK_TYPE_WIDGET)

static void clgc_tag_art_dispose(GObject* obj)
{
	ClgcTagArt* self = CLGC_TAG_ART(obj);

	g_clear_pointer(&(self->m_frame), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_tag_art_parent_class)->dispose(obj);
}

static void clgc_tag_art_class_init(ClgcTagArtClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_tag_art_dispose;

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-tag-art.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcTagArt, m_frame);
	gtk_widget_class_bind_template_child(widget_class, ClgcTagArt, m_image);
}

static void clgc_tag_art_init(ClgcTagArt* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));

	self->m_tag = G_MININT32;

	clgc_tag_art_set_tag(self, -1);

	gtk_widget_set_size_request(self->m_image, SIZE_WIDGET, SIZE_WIDGET);
}

GtkWidget* clgc_tag_art_new()
{
	return g_object_new(CLGC_TYPE_TAG_ART, NULL);
}

void clgc_tag_art_set_image(ClgcTagArt* self, const char* filename)
{
	if (!filename || !g_file_test(filename, G_FILE_TEST_EXISTS))
		return;

	GError* error = NULL;
	GdkPaintable* paintable = GDK_PAINTABLE(gdk_texture_new_from_filename(filename, &error));

	if (!paintable)
	{
		fprintf(stderr, "[ERROR] clgc_tag_art_set_image: Unable to read file %s\n", error->message);
		g_error_free(error);
		return;
	}

	// TODO: Just to delete warning during the compilation
	// Now, we don't use the albumart.
	gtk_image_set_from_paintable(GTK_IMAGE(self->m_image), paintable);
}

void clgc_tag_art_set_tag(ClgcTagArt* self, int tag)
{
	if (self->m_tag == tag)
		return;

	self->m_tag = tag;
	gtk_image_set_from_icon_name(GTK_IMAGE(self->m_image), clgc_tag_type_get_icon_name(self->m_tag));
	gtk_image_set_icon_size(GTK_IMAGE(self->m_image), GTK_ICON_SIZE_LARGE);
}
