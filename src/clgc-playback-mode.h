/*
 * clgc-playback-mode: Calgici PlaybackMode class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYBACK_MODE__
#define __CALGICI_PLAYBACK_MODE__

#include "clgc-playback-status.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_PLAYBACK_MODE (clgc_playback_mode_get_type())

G_DECLARE_FINAL_TYPE(ClgcPlaybackMode, clgc_playback_mode, CLGC, PLAYBACK_MODE, GtkWidget)

GtkWidget* clgc_playback_mode_new();

void clgc_playback_mode_set_status(ClgcPlaybackMode* self, ClgcPlaybackStatus status);

G_END_DECLS

#endif // __CALGICI_PLAYBACK_MODE__
