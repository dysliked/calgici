/*
 * clgc-songs-view: Calgici SongsView class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-songs-view.h"

#include "clgc-playback-state.h"
#include "clgc-song-row.h"
#include "clgc-tag.h"

#include <libintl.h>

enum _ClgcSongsViewProperty
{
	PROP_0,
	PROP_ACTION,
	N_PROPERTIES,
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcSongsView
{
	GtkWidget m_parent;

	int m_nbchild;
	ClgcSongsViewAction m_action;
	GtkWidget* m_box;
};

G_DEFINE_TYPE(ClgcSongsView, clgc_songs_view, GTK_TYPE_WIDGET)

static void clgc_songs_view_send_change_pick(ClgcSongsView* self)
{
	gtk_widget_activate_action(GTK_WIDGET(self), "win.change-pick", "i", clgc_songs_view_get_nb_selected(self));
}

static void clgc_songs_view_selected_song(GtkListBox* box, GtkListBoxRow* row, gpointer user_data)
{
	ClgcSongsView* self = CLGC_SONGS_VIEW(user_data);
	ClgcSongRow* song_row = CLGC_SONG_ROW(row);

	clgc_song_row_set_selected(song_row, !clgc_song_row_is_selected(song_row));
	clgc_songs_view_send_change_pick(self);
}

static void clgc_songs_view_dispose(GObject* obj)
{
	ClgcSongsView* self = CLGC_SONGS_VIEW(obj);

	g_clear_pointer(&(self->m_box), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_songs_view_parent_class)->dispose(obj);
}

static void clgc_songs_view_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcSongsView* self = CLGC_SONGS_VIEW(obj);

	switch (property_id)
	{
		case PROP_ACTION:
			g_value_set_int(value, self->m_action);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_songs_view_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcSongsView* self = CLGC_SONGS_VIEW(obj);

	switch (property_id)
	{
		case PROP_ACTION:
			self->m_action = g_value_get_int(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_songs_view_class_init(ClgcSongsViewClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_songs_view_dispose;
	obj_class->get_property = clgc_songs_view_get_property;
	obj_class->set_property = clgc_songs_view_set_property;

	obj_properties[PROP_ACTION] = g_param_spec_int("action", "Action", "The action to do when the user double clicks", CLGC_SONGS_VIEW_NONE, CLGC_SONGS_VIEW_MAX - 1, CLGC_SONGS_VIEW_NONE, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-songs-view.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcSongsView, m_box);
}

static void clgc_songs_view_init(ClgcSongsView* self)
{
	self->m_nbchild = 0;
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_songs_view_new(ClgcSongsViewAction action)
{
	return g_object_new(CLGC_TYPE_SONGS_VIEW, "action", action, NULL);
}

void clgc_songs_view_append(ClgcSongsView* self, ClgcInfoSong* song)
{
	gtk_list_box_append(GTK_LIST_BOX(self->m_box), clgc_song_row_new(song));
	self->m_nbchild ++;
}

void clgc_songs_view_clear(ClgcSongsView* self)
{
	GtkListBoxRow* child = NULL;

	while ((child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), 0)) != NULL)
		gtk_list_box_remove(GTK_LIST_BOX(self->m_box), GTK_WIDGET(child));

	self->m_nbchild = 0;
}

int clgc_songs_view_get_nb_selected(ClgcSongsView* self)
{
	int id;
	int count = 0;
	GtkListBoxRow* child = NULL;

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);
		count += clgc_song_row_is_selected(CLGC_SONG_ROW(child));
	}

	return count;
}

void clgc_songs_view_send_add_songs(ClgcSongsView* self)
{
	int id;
	GtkListBoxRow* child = NULL;
	GVariant* variant = NULL;
	GVariantBuilder* builder = NULL;
	ClgcInfoSong* song = NULL;

	builder = g_variant_builder_new(G_VARIANT_TYPE("as"));

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);

		if (clgc_song_row_is_selected(CLGC_SONG_ROW(child)))
		{
			song = clgc_song_row_get_song(CLGC_SONG_ROW(child));
			g_variant_builder_add(builder, "s", clgc_info_song_get_path(song));
		}
	}

	variant = g_variant_new("as", builder);
	g_variant_builder_unref(builder);

	gtk_widget_activate_action_variant(GTK_WIDGET(self), "app.mpd-client.add-songs", variant);
}

void clgc_songs_view_send_remove_songs(ClgcSongsView* self)
{
	int id;
	GtkListBoxRow* child = NULL;
	GVariant* variant = NULL;
	GVariantBuilder* builder = NULL;
	ClgcInfoSong* song = NULL;

	builder = g_variant_builder_new(G_VARIANT_TYPE("ai"));

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);

		if (clgc_song_row_is_selected(CLGC_SONG_ROW(child)))
		{
			song = clgc_song_row_get_song(CLGC_SONG_ROW(child));
			g_variant_builder_add(builder, "i", clgc_info_song_get_id(song));
		}
	}

	variant = g_variant_new("ai", builder);
	g_variant_builder_unref(builder);

	gtk_widget_activate_action_variant(GTK_WIDGET(self), "app.mpd-client.remove-songs", variant);
}

void clgc_songs_view_set_current_song(ClgcSongsView* self, ClgcInfoSong* song, ClgcPlaybackState state)
{
	int id;
	GtkListBoxRow* child = NULL;
	ClgcInfoSong* tmp;

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);
		tmp = clgc_song_row_get_song(CLGC_SONG_ROW(child));

		if (song != NULL && clgc_info_song_get_id(song) == clgc_info_song_get_id(tmp))
			clgc_song_row_set_state(CLGC_SONG_ROW(child), state);
		else
			clgc_song_row_set_state(CLGC_SONG_ROW(child), CLGC_PLAYBACK_STATE_MAX);
	}
}

void clgc_songs_view_set_mode(ClgcSongsView* self, ClgcMode mode)
{
	int id = 0;
	GtkListBoxRow* child = NULL;

	// Disable the single-click to select a song
	g_signal_handlers_disconnect_matched(G_OBJECT(self->m_box), G_SIGNAL_MATCH_FUNC, 0, 0, NULL, clgc_songs_view_selected_song, self);
	// Disable the double click to do an action on songs
	//g_signal_handlers_disconnect_matched(G_OBJECT(self->m_box), G_SIGNAL_MATCH_FUNC, 0, 0, NULL, , NULL);

	if (mode == CLGC_MODE_NORMAL)
	{
		// double-click play songs
		// gtk_signal_connect(G_OBJECT(self->m_box), "row-activated", G_CALLBACK(), NULL);
	}
	else if (mode == CLGC_MODE_SELECTION)
	{
		// Enable the single-click to select songs
		g_signal_connect(G_OBJECT(self->m_box), "row-activated", G_CALLBACK(clgc_songs_view_selected_song), self);
	}

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);
		clgc_song_row_set_mode(CLGC_SONG_ROW(child), mode);
	}
}

void clgc_songs_view_set_selected(ClgcSongsView* self, int selected)
{
	int id;
	GtkListBoxRow* child = NULL;

	for (id = 0; id < self->m_nbchild; id ++)
	{
		child = gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->m_box), id);
		clgc_song_row_set_selected(CLGC_SONG_ROW(child), selected);
	}

	clgc_songs_view_send_change_pick(self);
}
