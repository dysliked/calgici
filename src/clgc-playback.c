/*
 * clgc-playback: Calgici Playback class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-playback.h"

enum _ClgcPlaybackProperty
{
	PROP_0,
	PROP_SONG,
	PROP_STATE,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcPlayback
{
	GObject m_parent;

	ClgcInfoSong* m_song;
	ClgcPlaybackState m_state;
};

G_DEFINE_TYPE(ClgcPlayback, clgc_playback, G_TYPE_OBJECT)

static void clgc_playback_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcPlayback* self = CLGC_PLAYBACK(obj);

	switch (property_id)
	{
		case PROP_SONG:
			g_value_set_object(value, self->m_song);
			break;
		case PROP_STATE:
			g_value_set_uint(value, self->m_state);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_playback_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcPlayback* self = CLGC_PLAYBACK(obj);

	switch (property_id)
	{
		case PROP_SONG:
			self->m_song = g_value_get_object(value);
			break;
		case PROP_STATE:
			self->m_state = g_value_get_uint(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_playback_class_init(ClgcPlaybackClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);

	obj_class->get_property = clgc_playback_get_property;
	obj_class->set_property = clgc_playback_set_property;

	obj_properties[PROP_SONG] = g_param_spec_object(
		"song",
		"Song",
		"The information of the playing song",
		CLGC_TYPE_INFO_SONG,
		G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE
	);

	obj_properties[PROP_STATE] = g_param_spec_uint(
		"state",
		"State",
		"The state of the playing song",
		0,
		CLGC_PLAYBACK_STATE_MAX,
		CLGC_PLAYBACK_STATE_MAX,
		G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
}

static void clgc_playback_init(ClgcPlayback* self)
{
}

ClgcPlayback* clgc_playback_new(ClgcInfoSong* song, ClgcPlaybackState state)
{
	return g_object_new(CLGC_TYPE_PLAYBACK, "song", song, "state", state, NULL);
}

ClgcInfoSong* clgc_playback_get_song(ClgcPlayback* self)
{
	return self->m_song;
}

ClgcPlaybackState clgc_playback_get_state(ClgcPlayback* self)
{
	return self->m_state;
}
