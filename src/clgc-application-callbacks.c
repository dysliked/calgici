/*
 * clgc-application-callbacks: callbacks Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-application-callbacks.h"

#include "clgc-application-private.h"
#include "clgc-mpd-client.h"
#include "clgc-tasks.h"
#include "clgc-window.h"

static gboolean clgc_application_get_playback_time(gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GDateTime* time = NULL;

	GMutex* mutex = clgc_mpd_client_get_mutex(self->m_client);

	if (g_mutex_trylock(mutex))
	{
		time = clgc_mpd_client_get_playback_time(self->m_client);
		g_mutex_unlock(mutex);

		if (time)
			g_date_time_unref(time);
	}

	time = clgc_mpd_client_get_playback_time(self->m_client_time);

	return TRUE;
}

static int clgc_application_channel_watch(GIOChannel* channel, GIOCondition condition, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);

	if (!(condition & G_IO_IN))

	{
		fprintf(stdout, "[INFO] clgc_application_actions_watch: Bad way\n");
		return FALSE;
	}

	clgc_application_manage_event(self, clgc_mpd_client_get_event(self->m_client_listen));
	return TRUE;
}

void clgc_application_callbacks_connect(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GIOChannel* channel = NULL;

	const char* hostname = g_settings_get_string(self->m_settings, "hostname");
	const unsigned port = 0;
	const unsigned timeout = g_settings_get_uint(self->m_settings, "timeout");

	g_settings_get(self->m_settings, "port", "q", &port);

	GError* error = NULL;
	GTask* task = NULL;
	ClgcTaskData* task_data = NULL;

	self->m_client = g_task_propagate_pointer(G_TASK(res), &error);
	g_info("Connected to MPD server 'mpd://%s:%d/'", hostname, port);

	self->m_client_time = clgc_mpd_client_connect(hostname, port, timeout);
	self->m_client_listen = clgc_mpd_client_connect(hostname, port, timeout);
	channel = clgc_mpd_client_get_channel(self->m_client_listen);

	g_io_add_watch(channel, G_IO_IN, clgc_application_channel_watch, self);

	task = g_task_new(self->m_client, NULL, clgc_application_callbacks_audio_outputs, self);
	task_data = clgc_task_data_new(clgc_mpd_client_get_audio_outputs);
	g_task_set_task_data(task, task_data, NULL);
	g_task_run_in_thread(task, clgc_tasks_get_data_from_server);

	g_timeout_add(MIN(timeout, 500), clgc_application_get_playback_time, self);

	clgc_application_requests_init(self);
	clgc_application_manage_event(self, CLGC_EVENT_ALL);
}

void clgc_application_callbacks_audio_outputs(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	GList* list = g_task_propagate_pointer(G_TASK(res), &error);

	clgc_window_set_audio_outputs(CLGC_WINDOW(self->m_window), list);

	clgc_application_requests_set_audio_outputs(self, list);
}

void clgc_application_callbacks_show_contents(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	ClgcTaskData* task_data = CLGC_TASK_DATA(g_task_get_task_data(G_TASK(res)));
	ClgcTag* tag = clgc_task_data_get_arg(task_data, 0);
	GList* list = g_task_propagate_pointer(G_TASK(res), &error);

	clgc_window_show_contents(CLGC_WINDOW(self->m_window), tag, list);
}

static void clgc_application_callbacks_update_audio_outputs_aux(gpointer data, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	ClgcAudioOutput* audio_output = CLGC_AUDIO_OUTPUT(data);

	GVariant* variant = g_variant_new_boolean(clgc_audio_output_is_enabled(audio_output));
	g_action_group_change_action_state(G_ACTION_GROUP(self), clgc_audio_output_get_action_name(audio_output), variant);
}

void clgc_application_callbacks_update_audio_outputs(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	GList* list = g_task_propagate_pointer(G_TASK(res), &error);

	g_list_foreach(list, clgc_application_callbacks_update_audio_outputs_aux, self);
}

void clgc_application_callbacks_update_database(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	ClgcTaskData* task_data = CLGC_TASK_DATA(g_task_get_task_data(G_TASK(res)));
	ClgcTag* tag = clgc_task_data_get_arg(task_data, 0);
	GList* list = g_task_propagate_pointer(G_TASK(res), &error);

	clgc_window_update_database(CLGC_WINDOW(self->m_window), tag, list);
	g_list_free(list);
}

void clgc_application_callbacks_update_playback(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	ClgcPlayback* playback = g_task_propagate_pointer(G_TASK(res), &error);

	clgc_window_set_playback(CLGC_WINDOW(self->m_window), playback);
}

void clgc_application_callbacks_update_playback_status(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	GVariant* variant = NULL;
	ClgcPlaybackStatus status = g_task_propagate_int(G_TASK(res), &error);

	variant = g_variant_new_boolean(status & CLGC_PLAYBACK_STATUS_RANDOM);
	g_action_group_change_action_state(G_ACTION_GROUP(self), "mpd-client.playback-random", variant);

	variant = g_variant_new_boolean(status & CLGC_PLAYBACK_STATUS_REPEAT);
	g_action_group_change_action_state(G_ACTION_GROUP(self), "mpd-client.playback-repeat", variant);

	variant = g_variant_new_boolean(status & CLGC_PLAYBACK_STATUS_SINGLE);
	g_action_group_change_action_state(G_ACTION_GROUP(self), "mpd-client.playback-single", variant);

	clgc_window_set_playback_status(CLGC_WINDOW(self->m_window), status);
}

void clgc_application_callbacks_update_queue(GObject* object, GAsyncResult* res, gpointer user_data)
{
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GError* error = NULL;

	GList* list = g_task_propagate_pointer(G_TASK(res), &error);

	clgc_window_update_queue(CLGC_WINDOW(self->m_window), list);
	g_list_free(list);
}
