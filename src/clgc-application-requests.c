/*
 * clgc-application-requests: requests Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-application-private.h"

#include "clgc-tasks.h"

static void change_state(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	g_simple_action_set_state(action, parameter);
}

static void clgc_application_requests_add_songs(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	const char* path = NULL;
	GVariantIter* iter = NULL;
	GList* list = NULL;

	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_add_songs);

	g_variant_get(parameter, "as", &iter);

	while (g_variant_iter_loop(iter, "s", &path))
	{
		list = g_list_append(list, g_strdup(path));
	}

	g_variant_iter_free(iter);

	clgc_task_data_add_arg(task_data, list);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_arg_to_server);
}

static void clgc_application_requests_audio_output(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	const char* action_name = g_action_get_name(G_ACTION(action));

	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_set_audio_output);

	clgc_task_data_add_arg(task_data, GINT_TO_POINTER(clgc_audio_output_action_name_get_id(action_name)));
	clgc_task_data_add_arg(task_data, GINT_TO_POINTER(!g_variant_get_boolean(g_action_get_state(G_ACTION(action)))));

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_2_enums_to_server);
}

static void clgc_application_requests_get_songs(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	int tag_type = g_variant_get_int32(g_variant_get_child_value(parameter, 0));
	const char* value = g_variant_get_string(g_variant_get_child_value(parameter, 1), 0);

	ClgcTag* tag = clgc_tag_new(tag_type, value);

	clgc_application_show_contents(CLGC_APPLICATION_DEFAULT, tag);
}

static void clgc_application_requests_playback_next(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_next);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

static void clgc_application_requests_playback_pause(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_pause);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

static void clgc_application_requests_playback_play(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_play);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

static void clgc_application_requests_playback_previous(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_previous);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

static void clgc_application_requests_playback_stop(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_stop);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

static void clgc_application_requests_playback_random(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_set_random);

	clgc_task_data_add_arg(task_data, GINT_TO_POINTER(!g_variant_get_boolean(g_action_get_state(G_ACTION(action)))));

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_enum_to_server);
}

static void clgc_application_requests_playback_repeat(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_set_repeat);

	clgc_task_data_add_arg(task_data, GINT_TO_POINTER(!g_variant_get_boolean(g_action_get_state(G_ACTION(action)))));

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_enum_to_server);
}

static void clgc_application_requests_playback_single(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_set_single);

	clgc_task_data_add_arg(task_data, GINT_TO_POINTER(!g_variant_get_boolean(g_action_get_state(G_ACTION(action)))));

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_enum_to_server);
}

static void clgc_application_requests_remove_songs(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GVariantIter* iter = NULL;
	GList* list = NULL;
	int num = 0;

	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_remove_songs);


	g_variant_get(parameter, "ai", &iter);

	while (g_variant_iter_loop(iter, "i", &num))
	{
		list = g_list_append(list, GINT_TO_POINTER(num));
	}

	g_variant_iter_free(iter);

	clgc_task_data_add_arg(task_data, list);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_with_arg_to_server);
}

static void clgc_application_requests_update_database(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(user_data);
	GTask* task = g_task_new(client, NULL, NULL, NULL);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_update_database);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_send_command_to_server);
}

void clgc_application_requests_init(ClgcApplication* self)
{
	static const GActionEntry requests[] = {
		// Playback command
		{"mpd-client.playback-next", clgc_application_requests_playback_next, NULL, NULL, NULL, {0, 0, 0}},
		{"mpd-client.playback-pause", clgc_application_requests_playback_pause, NULL, NULL, NULL, {0, 0, 0}},
		{"mpd-client.playback-play", clgc_application_requests_playback_play, NULL, NULL, NULL, {0, 0, 0}},
		{"mpd-client.playback-previous", clgc_application_requests_playback_previous, NULL, NULL, NULL, {0, 0, 0}},
		{"mpd-client.playback-stop", clgc_application_requests_playback_stop, NULL, NULL, NULL, {0, 0, 0}},

		// Playback type
		{"mpd-client.playback-random", clgc_application_requests_playback_random, NULL, "false", change_state, {0, 0, 0}},
		{"mpd-client.playback-repeat", clgc_application_requests_playback_repeat, NULL, "false", change_state, {0, 0, 0}},
		{"mpd-client.playback-single", clgc_application_requests_playback_single, NULL, "false", change_state, {0, 0, 0}},

		{"mpd-client.add-songs", clgc_application_requests_add_songs, "as", NULL, NULL, {0, 0, 0}},
		{"mpd-client.get-songs", clgc_application_requests_get_songs, "(is)", NULL, NULL, {0, 0, 0}},
		{"mpd-client.remove-songs", clgc_application_requests_remove_songs, "ai", NULL, NULL, {0, 0, 0}},

		// Database
		{"mpd-client.update-database", clgc_application_requests_update_database, NULL, NULL, NULL, {0, 0, 0}},
	};

	g_action_map_add_action_entries(G_ACTION_MAP(self), requests, G_N_ELEMENTS(requests), self->m_client);
}

static void clgc_application_requests_set_audio_outputs_aux(gpointer data, gpointer user_data)
{
	ClgcAudioOutput* audio_output = CLGC_AUDIO_OUTPUT(data);
	ClgcApplication* self = CLGC_APPLICATION(user_data);
	GActionEntry entry;

	entry.name = clgc_audio_output_get_action_name(audio_output);
	entry.activate = clgc_application_requests_audio_output;
	entry.parameter_type = NULL;
	entry.state = "false";
	entry.change_state = change_state;

	g_action_map_add_action_entries(G_ACTION_MAP(self), &entry, 1, self->m_client);
}

void clgc_application_requests_set_audio_outputs(ClgcApplication* self, GList* list)
{
	g_list_foreach(list, clgc_application_requests_set_audio_outputs_aux, self);
}
