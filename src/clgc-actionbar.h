/*
 * clgc-actionbar: Calgici ActionBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_ACTIONBAR__
#define __CALGICI_ACTIONBAR__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_ACTIONBAR (clgc_actionbar_get_type())

G_DECLARE_FINAL_TYPE(ClgcActionBar, clgc_actionbar, CLGC, ACTIONBAR, GtkWidget)

/**
 * @brief Enables the remove button
 * @param self The action bar
 */
void clgc_actionbar_enable_buttons(ClgcActionBar* self);

/**
 * @brief Disables the remove button
 * @param self The action bar
 */
void clgc_actionbar_disable_buttons(ClgcActionBar* self);

/**
 * @brief Creates an instance of action bar
 * @param remove 1 if the remove button should be added, 0 otherwise
 * @return An instance of action bar
 */
GtkWidget* clgc_actionbar_new(int remove);

G_END_DECLS

#endif // __CALGICI_ACTIONBAR__
