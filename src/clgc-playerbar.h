/*
 * clgc-playerbar: Calgici PlayerBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYERBAR__
#define __CALGICI_PLAYERBAR__

#include "clgc-playback.h"
#include "clgc-playback-status.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_PLAYERBAR (clgc_playerbar_get_type())

G_DECLARE_FINAL_TYPE(ClgcPlayerBar, clgc_playerbar, CLGC, PLAYERBAR, GtkWidget)

GtkWidget* clgc_playerbar_new();

GtkWidget* clgc_playerbar_get_info_box(ClgcPlayerBar* self);

gboolean clgc_playerbar_get_reveal(ClgcPlayerBar* self);

GtkWidget* clgc_playerbar_get_time_box(ClgcPlayerBar* self);

void clgc_playerbar_set_playback(ClgcPlayerBar* self, ClgcPlayback* playback);

void clgc_playerbar_set_playback_status(ClgcPlayerBar* self, ClgcPlaybackStatus status);

void clgc_playerbar_set_reveal(ClgcPlayerBar* self, gboolean reveal);

void clgc_playerbar_set_time_song(ClgcPlayerBar* self, unsigned time);

G_END_DECLS

#endif // __CALGICI_PLAYERBAR__
