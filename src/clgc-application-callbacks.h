/*
 * clgc-application-callbacks: callbacks Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_APPLICATION_CALLBACKS__
#define __CALGICI_APPLICATION_CALLBACKS__

#include <glib-object.h>

#include <gio/gio.h>

void clgc_application_callbacks_connect(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_audio_outputs(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_show_contents(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_update_audio_outputs(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_update_database(GObject* object, GAsyncResult*res, gpointer user_data);

void clgc_application_callbacks_update_playback(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_update_playback_status(GObject* object, GAsyncResult* res, gpointer user_data);

void clgc_application_callbacks_update_queue(GObject* object, GAsyncResult* res, gpointer user_data);

#endif // __CALGICI_APPLICATION_CALLBACKS__
