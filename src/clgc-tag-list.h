/*
 * clgc-tag-list: Calgici TagList class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TAG_LIST__
#define __CALGICI_TAG_LIST__

#include "clgc-tag.h"

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _ClgcTagList ClgcTagList;

#define CLGC_TYPE_TAG_LIST (clgc_tag_list_get_type())

ClgcTagList* clgc_tag_list_new();

void clgc_tag_list_add(ClgcTagList* self, ClgcTag* tag);

ClgcTagList* clgc_tag_list_copy(ClgcTagList* self);

void clgc_tag_list_free(ClgcTagList* self);

GType clgc_tag_list_get_type();

int clgc_tag_list_length(ClgcTagList* self);

ClgcTag* clgc_tag_list_nth(ClgcTagList* self, int nth);

G_END_DECLS

#endif // __CALGICI_TAG_LIST__
