/*
 * clgc-medium-panel: Calgici MediumPanel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_MEDIUM_PANEL__
#define __CALGICI_MEDIUM_PANEL__

#include "clgc-event.h"
#include "clgc-tag.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_MEDIUM_PANEL (clgc_medium_panel_get_type())

G_DECLARE_FINAL_TYPE(ClgcMediumPanel, clgc_medium_panel, CLGC, MEDIUM_PANEL, GtkWidget)

/**
 * @brief Creates an instance of string window
 * @return An instance of string window
 */
GtkWidget* clgc_medium_panel_new();

/**
 * @brief Updates with the value of the tag
 * @param self The window
 * @param tag The tag's number of the value
 * @param value The value
 */
void clgc_medium_panel_update(ClgcMediumPanel* self, ClgcTag* tag, GList* list);

G_END_DECLS

#endif // __CALGICI_MEDIUM_PANEL__
