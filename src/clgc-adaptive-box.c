/*
 * clgc-adaptive-box: Calgici AdaptiveBox class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-adaptive-box.h"

#include "clgc-adaptive-layout.h"
#include "clgc-widget-private.h"

enum _ClgcAdaptiveBoxId
{
	CLGC_ADAPTIVE_BOX_START,
	CLGC_ADAPTIVE_BOX_MAIN,
	CLGC_ADAPTIVE_BOX_SECONDARY,
	CLGC_ADAPTIVE_BOX_END,
	CLGC_ADAPTIVE_BOX_MAX
};


enum _ClgcAdaptiveBoxProperty
{
	PROP_0,
	PROP_SPACING,
	PROP_ORIENTATION,
	N_PROPERTIES = PROP_ORIENTATION
};

struct _ClgcAdaptiveBox
{
	GtkWidget m_parent;

	GtkWidget* m_widgets[CLGC_ADAPTIVE_BOX_MAX];
};

static GtkBuildableIface* parent_buildable_iface;
static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

static void clgc_adaptive_box_buildable_init(GtkBuildableIface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcAdaptiveBox, clgc_adaptive_box, GTK_TYPE_WIDGET,
	G_IMPLEMENT_INTERFACE(GTK_TYPE_ORIENTABLE, NULL)
	G_IMPLEMENT_INTERFACE(GTK_TYPE_BUILDABLE, clgc_adaptive_box_buildable_init)
)

static void clgc_adaptive_box_dispose(GObject* obj)
{
	int id;
	ClgcAdaptiveBox* self = CLGC_ADAPTIVE_BOX(obj);

	for (id = 0; id < CLGC_ADAPTIVE_BOX_MAX; id ++)
		g_clear_pointer(&(self->m_widgets[id]), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_adaptive_box_parent_class)->dispose(obj);
}

static void clgc_adaptive_box_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_SPACING:
			g_value_set_int(value, clgc_adaptive_layout_get_spacing(CLGC_ADAPTIVE_LAYOUT(layout)));
			break;
		case PROP_ORIENTATION:
			g_value_set_enum(value, gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_adaptive_box_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	GtkOrientation orientation;
	ClgcAdaptiveBox* self = CLGC_ADAPTIVE_BOX(object);
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_SPACING:
			clgc_adaptive_layout_set_spacing(CLGC_ADAPTIVE_LAYOUT(layout), g_value_get_int(value));
			break;
		case PROP_ORIENTATION:
			{
				orientation = g_value_get_enum(value);

				if (gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)) != orientation)
				{
					gtk_orientable_set_orientation(GTK_ORIENTABLE(layout), orientation);
					clgc_widget_update_orientation(GTK_WIDGET(self), orientation);
					g_object_notify_by_pspec(object, pspec);
				}
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_adaptive_box_class_init(ClgcAdaptiveBoxClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_adaptive_box_dispose;
	obj_class->get_property = clgc_adaptive_box_get_property;
	obj_class->set_property = clgc_adaptive_box_set_property;

	g_object_class_override_property(obj_class, PROP_ORIENTATION, "orientation");

	obj_properties[PROP_SPACING] = g_param_spec_int("spacing","Spacing", "The amount of space between children", 0, G_MAXINT, 0, G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, CLGC_TYPE_ADAPTIVE_LAYOUT);
	gtk_widget_class_set_css_name(widget_class, "box");
	gtk_widget_class_set_accessible_role(widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
}

static void clgc_adaptive_box_init(ClgcAdaptiveBox* self)
{
	int id;

	for (id = 0; id < CLGC_ADAPTIVE_BOX_MAX; id ++)
		self->m_widgets[id] = NULL;
}

static void clgc_adaptive_box_add_child(GtkBuildable* buildable, GtkBuilder* builder, GObject* child, const char* type)
{
	if (!GTK_IS_WIDGET(child))
	{
		parent_buildable_iface->add_child(buildable, builder, child, type);
		return;
	}

	if (strcmp(type, "start") == 0)
		clgc_adaptive_box_set_start_widget(CLGC_ADAPTIVE_BOX(buildable), GTK_WIDGET(child));
	else if (strcmp(type, "main") == 0)
		clgc_adaptive_box_set_main_widget(CLGC_ADAPTIVE_BOX(buildable), GTK_WIDGET(child));
	else if (strcmp(type, "secondary") == 0)
		clgc_adaptive_box_set_secondary_widget(CLGC_ADAPTIVE_BOX(buildable), GTK_WIDGET(child));
	else if (strcmp(type, "end") == 0)
		clgc_adaptive_box_set_end_widget(CLGC_ADAPTIVE_BOX(buildable), GTK_WIDGET(child));
}

static void clgc_adaptive_box_buildable_init(GtkBuildableIface* iface)
{
	parent_buildable_iface = g_type_interface_peek_parent(iface);

	iface->add_child = clgc_adaptive_box_add_child;
}

GtkWidget* clgc_adaptive_box_new()
{
	return g_object_new(CLGC_TYPE_ADAPTIVE_BOX, "orientation", GTK_ORIENTATION_HORIZONTAL, "spacing", 10, NULL);
}

GtkWidget* clgc_adaptive_box_get_end_widget(ClgcAdaptiveBox* self)
{
	return self->m_widgets[CLGC_ADAPTIVE_BOX_END];
}

GtkWidget* clgc_adaptive_box_get_main_widget(ClgcAdaptiveBox* self)
{
	return self->m_widgets[CLGC_ADAPTIVE_BOX_MAIN];
}

GtkWidget* clgc_adaptive_box_get_secondary_widget(ClgcAdaptiveBox* self)
{
	return self->m_widgets[CLGC_ADAPTIVE_BOX_SECONDARY];
}

GtkWidget* clgc_adaptive_box_get_start_widget(ClgcAdaptiveBox* self)
{
	return self->m_widgets[CLGC_ADAPTIVE_BOX_START];
}

void clgc_adaptive_box_set_end_widget(ClgcAdaptiveBox* self, GtkWidget* widget)
{
	if (self->m_widgets[CLGC_ADAPTIVE_BOX_END])
		gtk_widget_unparent(self->m_widgets[CLGC_ADAPTIVE_BOX_END]);

	self->m_widgets[CLGC_ADAPTIVE_BOX_END] = widget;

	if (widget)
		gtk_widget_insert_after(widget, GTK_WIDGET(self), self->m_widgets[CLGC_ADAPTIVE_BOX_END - 1]);
}

void clgc_adaptive_box_set_main_widget(ClgcAdaptiveBox* self, GtkWidget* widget)
{
	if (self->m_widgets[CLGC_ADAPTIVE_BOX_MAIN])
		gtk_widget_unparent(self->m_widgets[CLGC_ADAPTIVE_BOX_MAIN]);

	self->m_widgets[CLGC_ADAPTIVE_BOX_MAIN] = widget;

	if (widget)
		gtk_widget_insert_after(widget, GTK_WIDGET(self), self->m_widgets[CLGC_ADAPTIVE_BOX_MAIN - 1]);
}

void clgc_adaptive_box_set_secondary_widget(ClgcAdaptiveBox* self, GtkWidget* widget)
{
	if (self->m_widgets[CLGC_ADAPTIVE_BOX_SECONDARY])
		gtk_widget_unparent(self->m_widgets[CLGC_ADAPTIVE_BOX_SECONDARY]);

	self->m_widgets[CLGC_ADAPTIVE_BOX_SECONDARY] = widget;

	if (widget)
		gtk_widget_insert_after(widget, GTK_WIDGET(self), self->m_widgets[CLGC_ADAPTIVE_BOX_SECONDARY - 1]);
}

void clgc_adaptive_box_set_start_widget(ClgcAdaptiveBox* self, GtkWidget* widget)
{
	if (self->m_widgets[CLGC_ADAPTIVE_BOX_START])
		gtk_widget_unparent(self->m_widgets[CLGC_ADAPTIVE_BOX_START]);

	self->m_widgets[CLGC_ADAPTIVE_BOX_START] = widget;

	if (widget)
		gtk_widget_insert_after(widget, GTK_WIDGET(self), NULL);
}
