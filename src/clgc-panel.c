/*
 * clgc-panel: Calgici Panel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-panel.h"

G_DEFINE_INTERFACE(ClgcPanel, clgc_panel, G_TYPE_OBJECT)

static void clgc_panel_default_init(ClgcPanelInterface* iface)
{

}

void clgc_panel_set_mode(ClgcPanel* self, ClgcMode mode)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->set_mode(self, mode);
}

void clgc_panel_selection_all(ClgcPanel* self)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->selection_all(self);
}

void clgc_panel_selection_change(ClgcPanel* self, int selected)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->selection_change(self, selected);
}

void clgc_panel_selection_none(ClgcPanel* self)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->selection_none(self);
}

void clgc_panel_send_add(ClgcPanel* self)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->send_add(self);
}

void clgc_panel_send_remove(ClgcPanel* self)
{
	ClgcPanelInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PANEL(self));

	iface = CLGC_PANEL_GET_IFACE(self);
	iface->send_remove(self);
}
