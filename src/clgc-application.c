/*
 * clgc-application: main Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-application.h"

#include "clgc-application-callbacks.h"
#include "clgc-application-private.h"
#include "clgc-mode.h"
#include "clgc-mpd-client.h"
#include "clgc-tasks.h"
#include "clgc-window.h"
#include "utils.h"

#include <gio/gio.h>

#include <glib.h>
#include <libintl.h>

G_DEFINE_TYPE(ClgcApplication, clgc_application, ADW_TYPE_APPLICATION)

static void clgc_application_activate(GApplication* app)
{
	ClgcApplication* self = CLGC_APPLICATION(app);
	GTask* task = g_task_new(self->m_settings, NULL, clgc_application_callbacks_connect, self);

	G_APPLICATION_CLASS(clgc_application_parent_class)->activate(app);

	gtk_window_present(GTK_WINDOW(self->m_window));

	g_task_run_in_thread(task, clgc_tasks_connect_to_server);
}

static void clgc_application_finalize(GObject* object)
{
	G_OBJECT_CLASS(clgc_application_parent_class)->finalize(object);
}

static void clgc_application_startup(GApplication* app)
{
	ClgcApplication* self = CLGC_APPLICATION(app);
	GVariant* variant = NULL;
	const char** array = NULL;
	gsize n_elements = 0;
	int id;

	G_APPLICATION_CLASS(clgc_application_parent_class)->startup(app);

	variant = g_settings_get_value(self->m_settings, "pages");
	array = g_variant_get_strv(variant, &n_elements);

	for (id = 0; id < n_elements; id ++)
		clgc_tag_list_add(self->m_tag_list, clgc_tag_build_from_idname(array[id]));

	self->m_window = clgc_window_new(self->m_tag_list);

	clgc_application_actions_init(self);

	clgc_window_set_mode(CLGC_WINDOW(self->m_window), CLGC_MODE_NORMAL);
}

static void clgc_application_class_init(ClgcApplicationClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GApplicationClass* app_class = G_APPLICATION_CLASS(klass);

	obj_class->finalize = clgc_application_finalize;

	app_class->activate = clgc_application_activate;
	app_class->startup = clgc_application_startup;
}

static void clgc_application_init(ClgcApplication* self)
{
	self->m_settings = g_settings_new(APPLICATION_ID);
	self->m_tag_list = clgc_tag_list_new();
}

GApplication* clgc_application_new()
{
	return g_object_new(CLGC_TYPE_APPLICATION, "application-id", APPLICATION_ID, "flags", G_APPLICATION_DEFAULT_FLAGS, NULL);
}
