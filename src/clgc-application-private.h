/*
 * clgc-application-private: private Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_APPLICATION_PRIVATE__
#define __CALGICI_APPLICATION_PRIVATE__

#include "clgc-application.h"
#include "clgc-tag-list.h"

struct _ClgcApplication
{
	AdwApplication m_parent;

	GtkWidget* m_window;
	GSettings* m_settings;

	ClgcTagList* m_tag_list;

	ClgcMPDClient* m_client;
	ClgcMPDClient* m_client_listen;
	ClgcMPDClient* m_client_time;
};

void clgc_application_actions_init(ClgcApplication* self);

void clgc_application_requests_init(ClgcApplication* self);

void clgc_application_requests_set_audio_outputs(ClgcApplication* self, GList* list);

void clgc_application_set_playback(ClgcApplication* self);

void clgc_application_show_contents(ClgcApplication* self, ClgcTag* tag);

void clgc_application_manage_event(ClgcApplication* self, ClgcEvent event);

#endif // __CALGICI_APPLICATION_PRIVATE__
