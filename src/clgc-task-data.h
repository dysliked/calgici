/*
 * clgc-task-data: Calgici TaskData class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TASK_DATA__
#define __CALGICI_TASK_DATA__

#include <glib-object.h>

G_BEGIN_DECLS

#define CLGC_TYPE_TASK_DATA (clgc_task_data_get_type())

G_DECLARE_FINAL_TYPE(ClgcTaskData, clgc_task_data, CLGC, TASK_DATA, GObject)

ClgcTaskData* clgc_task_data_new(gpointer func);

void clgc_task_data_add_arg(ClgcTaskData* self, gpointer arg);

gpointer clgc_task_data_get_arg(ClgcTaskData* self, int id);

gpointer clgc_task_data_get_func(ClgcTaskData* self);

G_END_DECLS

#endif // __CALGICI_TASK_DATA__
