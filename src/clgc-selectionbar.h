/*
 * clgc-selectionbar: Calgici SelectionBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_SELECTIONBAR__
#define __CALGICI_SELECTIONBAR__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_SELECTIONBAR (clgc_selectionbar_get_type())

G_DECLARE_FINAL_TYPE(ClgcSelectionBar, clgc_selectionbar, CLGC, SELECTIONBAR, GtkWidget)

/**
 * @brief Creates an instance of selectionbar
 * @return An instance of selectionbar
 */
GtkWidget* clgc_selectionbar_new();

/**
 * @brief Manage when the selection changes
 * @param self The selection bar
 * @param selected The number of selected item
 */
void clgc_selectionbar_selection_change(ClgcSelectionBar* self, int selected);

G_END_DECLS

#endif // __CALGICI_SELECTIONBAR__
