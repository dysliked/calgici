/*
 * clgc-adaptive-layout: Calgici AdaptiveLayout class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CLGC_ADAPTIVE_LAYOUT__
#define __CLGC_ADAPTIVE_LAYOUT__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_ADAPTIVE_LAYOUT (clgc_adaptive_layout_get_type())

G_DECLARE_FINAL_TYPE(ClgcAdaptiveLayout, clgc_adaptive_layout, CLGC, ADAPTIVE_LAYOUT, GtkLayoutManager)

GtkLayoutManager* clgc_adaptive_layout_new();

int clgc_adaptive_layout_get_spacing(ClgcAdaptiveLayout* self);

void clgc_adaptive_layout_set_orientation(ClgcAdaptiveLayout* self, GtkOrientation orientation);

void clgc_adaptive_layout_set_spacing(ClgcAdaptiveLayout* self, int spacing);

G_END_DECLS

#endif // __CLGC_ADAPTIVE_LAYOUT__
