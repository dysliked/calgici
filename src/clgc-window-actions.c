/*
 * clgc-window-actions: actions Calgici Window class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-window-private.h"

#include "clgc-application.h"
#include "clgc-panel.h"
#include "clgc-medium-panel.h"
#include "clgc-tag.h"

#include <libintl.h>

static void clgc_window_actions_change_pick(GtkWidget* widget, const char* action_name, GVariant* param)
{
	ClgcWindow* self = CLGC_WINDOW(widget);
	AdwNavigationPage* page = adw_navigation_view_get_visible_page(ADW_NAVIGATION_VIEW(self->m_view));
	GtkWidget* panel = adw_navigation_page_get_child(page);
	int nb_selected = g_variant_get_int32(param);

	clgc_panel_selection_change(CLGC_PANEL(panel), nb_selected);
}

static void clgc_window_actions_previous(GtkWidget* widget, const char* action_name, GVariant* param)
{
	ClgcWindow* self = CLGC_WINDOW(widget);

	adw_navigation_view_push_by_tag(ADW_NAVIGATION_VIEW(self->m_view), "library");
}

static void clgc_window_actions_select_items(GtkWidget* widget, const char* action_name, GVariant* param)
{
	ClgcWindow* self = CLGC_WINDOW(widget);
	AdwNavigationPage* page = adw_navigation_view_get_visible_page(ADW_NAVIGATION_VIEW(self->m_view));
	GtkWidget* panel = adw_navigation_page_get_child(page);

	if (strcmp(action_name, "win.select-all") == 0)
		clgc_panel_selection_all(CLGC_PANEL(panel));
	else if (strcmp(action_name, "win.select-none") == 0)
		clgc_panel_selection_none(CLGC_PANEL(panel));
}

static void clgc_window_actions_send_action(GtkWidget* widget, const char* action_name, GVariant* param)
{
	ClgcWindow* self = CLGC_WINDOW(widget);
	AdwNavigationPage* page = adw_navigation_view_get_visible_page(ADW_NAVIGATION_VIEW(self->m_view));
	GtkWidget* panel = adw_navigation_page_get_child(page);

	if (strcmp(action_name, "win.send-add") == 0)
		clgc_panel_send_add(CLGC_PANEL(panel));
	else if (strcmp(action_name, "win.send-remove") == 0)
		clgc_panel_send_remove(CLGC_PANEL(panel));

	clgc_window_set_mode(self, CLGC_MODE_NORMAL);
}

static void clgc_window_actions_set_mode(GtkWidget* widget, const char* action_name, GVariant* param)
{
	ClgcWindow* self = CLGC_WINDOW(widget);

	if (strcmp(action_name, "win.normal-mode") == 0)
		clgc_window_set_mode(self, CLGC_MODE_NORMAL);
	else if (strcmp(action_name, "win.selection-mode") == 0)
		clgc_window_set_mode(self, CLGC_MODE_SELECTION);
}

static void clgc_window_actions_show_about(GtkWidget* widget, const char* action_name, GVariant* param)
{
	const char* developpers[] = {
		"Thibault Peypelut",
		NULL
	};

	const char* designers[] = {
		"Thibault Peypelut",
		"GNOME Design Team",
		NULL
	};

	adw_show_about_dialog_from_appdata(widget, "/org/dysliked/calgici/appdata", NULL, NULL);
/*
	adw_show_about_window(GTK_WINDOW(widget),
			"application-name", gettext(APP_TITLE),
			"application-icon", APPLICATION_ID,
			"version", VERSION,
			"copyright", "© 2020-2023 Thibault Peypelut",
			"issue-url", "https://framagit.org/dysliked/calgici/-/issues",
			"license-type", GTK_LICENSE_GPL_3_0,
			"developers", developpers,
			"designers", designers,
			"translator-credits", gettext("translator-credits"),
			NULL);
*/
}

static void clgc_window_actions_show_help_overlay(GtkWidget* widget, const char* action_name, GVariant* param)
{
	GtkBuilder* builder = NULL;
	GObject* help_overlay = NULL;

	builder = gtk_builder_new_from_resource("/org/dysliked/calgici/ui/help-overlay.ui");
	help_overlay = gtk_builder_get_object(builder, "help_overlay");

	if (GTK_IS_SHORTCUTS_WINDOW(help_overlay))
	{
		gtk_window_set_transient_for(GTK_WINDOW(help_overlay), GTK_WINDOW(widget));
		gtk_window_present(GTK_WINDOW(help_overlay));
	}
}

static void clgc_window_actions_show_preferences(GtkWidget* widget, const char* action_name, GVariant* param)
{

}

static void clgc_window_actions_show_server_information(GtkWidget* widget, const char* action_name, GVariant* param)
{

}

void clgc_window_class_actions_init(ClgcWindowClass* klass)
{
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	// Change the window mode
	gtk_widget_class_install_action(widget_class, "win.normal-mode", NULL, clgc_window_actions_set_mode);
	gtk_widget_class_install_action(widget_class, "win.selection-mode", NULL, clgc_window_actions_set_mode);

	// Change the main panel
	gtk_widget_class_install_action(widget_class, "win.previous", NULL, clgc_window_actions_previous);
	gtk_widget_class_install_action(widget_class, "win.show-help-overlay", NULL, clgc_window_actions_show_help_overlay);
	gtk_widget_class_install_action(widget_class, "win.show-preferences", NULL, clgc_window_actions_show_preferences);
	gtk_widget_class_install_action(widget_class, "win.show-server-information", NULL, clgc_window_actions_show_server_information);
	gtk_widget_class_install_action(widget_class, "win.about", NULL, clgc_window_actions_show_about);

	// An item was been select/deselect
	gtk_widget_class_install_action(widget_class, "win.change-pick", "i", clgc_window_actions_change_pick);

	// Select/Deselect items in the main panel
	gtk_widget_class_install_action(widget_class, "win.select-all", NULL, clgc_window_actions_select_items);
	gtk_widget_class_install_action(widget_class, "win.select-none", NULL, clgc_window_actions_select_items);

	// Send an action add or remove on selected items in a playlists
	gtk_widget_class_install_action(widget_class, "win.send-add", NULL, clgc_window_actions_send_action);
	gtk_widget_class_install_action(widget_class, "win.send-remove", NULL, clgc_window_actions_send_action);
}

void clgc_window_actions_update(ClgcWindow* self)
{
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.normal-mode", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.selection-mode", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.previous", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.change-pick", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.select-all", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.select-none", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.send-add", TRUE);
	gtk_widget_action_set_enabled(GTK_WIDGET(self), "win.send-remove", TRUE);
}
