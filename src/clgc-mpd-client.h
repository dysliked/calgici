/*
 * clgc-mpd-client: Calgici MPDClient class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_MPD_CLIENT__
#define __CALGICI_MPD_CLIENT__

#include "clgc-event.h"
#include "clgc-audio-output.h"
#include "clgc-playback.h"
#include "clgc-playback-status.h"
#include "clgc-tag.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define CLGC_TYPE_MPD_CLIENT (clgc_mpd_client_get_type())

G_DECLARE_FINAL_TYPE(ClgcMPDClient, clgc_mpd_client, CLGC, MPD_CLIENT, GObject)

/**
 * @brief Connect to a MPD server
 * @param address The address of the MPD server
 * @param port The port of the MPD server
 * @param timeout 
 * @return An instance to communicate with the MPD server
 */
ClgcMPDClient* clgc_mpd_client_connect(const char* address, unsigned port, unsigned timeout);

void clgc_mpd_client_add_song(ClgcMPDClient* self, const char* song_uri);

void clgc_mpd_client_add_songs(ClgcMPDClient* self, GList* list);

/**
 * @brief Disconnect to the MPD server
 * @param self The connection with the MPD server
 */
void clgc_mpd_client_disconnect(ClgcMPDClient* self);

/**
 * @brief
 * @param self The connection with the MPD server
 * @return
 */
GIOChannel* clgc_mpd_client_get_channel(ClgcMPDClient* self);

GList* clgc_mpd_client_get_data_from_tag(ClgcMPDClient* self, ClgcTag* tag);

/**
 * @brief
 * @param self The connection with the MPD server
 * @return
 */
ClgcEvent clgc_mpd_client_get_event(ClgcMPDClient* self);

GList* clgc_mpd_client_get_audio_outputs(ClgcMPDClient* self);

GMutex* clgc_mpd_client_get_mutex(ClgcMPDClient* self);

ClgcPlayback* clgc_mpd_client_get_playback(ClgcMPDClient* self);

ClgcInfoSong* clgc_mpd_client_get_playback_song(ClgcMPDClient* self);

ClgcPlaybackStatus clgc_mpd_client_get_playback_status(ClgcMPDClient* self);

ClgcPlaybackState clgc_mpd_client_get_playback_state(ClgcMPDClient* self);

GDateTime* clgc_mpd_client_get_playback_time(ClgcMPDClient* self);

GList* clgc_mpd_client_get_queue(ClgcMPDClient* self);

void clgc_mpd_client_keep_alive(ClgcMPDClient* self);

void clgc_mpd_client_next(ClgcMPDClient* self);

void clgc_mpd_client_pause(ClgcMPDClient* self);

void clgc_mpd_client_play(ClgcMPDClient* self);

void clgc_mpd_client_previous(ClgcMPDClient* self);

void clgc_mpd_client_remove_song(ClgcMPDClient* self, unsigned song_id);

void clgc_mpd_client_remove_songs(ClgcMPDClient* self, GList* list);

void clgc_mpd_client_send_request(ClgcMPDClient* self);

void clgc_mpd_client_set_audio_output(ClgcMPDClient* self, int audio_output_id, gboolean state);

void clgc_mpd_client_set_idle(ClgcMPDClient* self, gboolean state);

void clgc_mpd_client_set_random(ClgcMPDClient* self, gboolean active);

void clgc_mpd_client_set_repeat(ClgcMPDClient* self, gboolean active);

void clgc_mpd_client_set_single(ClgcMPDClient* self, gboolean active);

void clgc_mpd_client_stop(ClgcMPDClient* self);

unsigned clgc_mpd_client_update_database(ClgcMPDClient* self);

G_END_DECLS

#endif // __CALGICI_MPD_CLIENT__
