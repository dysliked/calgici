/*
 * clgc-tag-art: Calgici TagArt class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TAG_ART__
#define __CALGICI_TAG_ART__

#include "clgc-tag.h"

#include <gtk/gtk.h>

#define SIZE_WIDGET 160

G_BEGIN_DECLS

#define CLGC_TYPE_TAG_ART (clgc_tag_art_get_type())

G_DECLARE_FINAL_TYPE(ClgcTagArt, clgc_tag_art, CLGC, TAG_ART, GtkWidget)

/**
 @brief Create an instance of the tag art
 @return An instance of the tag art
 */
GtkWidget* clgc_tag_art_new();

/**
 * @brief Sets the image for the widget
 */
void clgc_tag_art_set_image(ClgcTagArt* self, const char* filename);

/**
 * @brief Sets the tag for the image
 */
void clgc_tag_art_set_tag(ClgcTagArt* self, int tag);

G_END_DECLS

#endif // __CALGICI_TAG_ART__
