/*
 * clgc-application-actions: actions Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-application.h"
#include "clgc-application-private.h"

#include "clgc-window.h"

static void clgc_application_actions_quit(GSimpleAction* action, GVariant* parameter, gpointer user_data)
{
	g_application_quit(G_APPLICATION(user_data));
}

void clgc_application_actions_init(ClgcApplication* self)
{
	static const char* quit_accels[] = {"<Primary>Q", NULL};
	static GActionEntry actions[] = {
		{"quit", clgc_application_actions_quit, NULL, NULL, NULL, {0, 0, 0}},
	};

	gtk_application_set_accels_for_action(GTK_APPLICATION(self), "app.quit", quit_accels);

	g_action_map_add_action_entries(G_ACTION_MAP(self), actions, G_N_ELEMENTS(actions), self);
}
