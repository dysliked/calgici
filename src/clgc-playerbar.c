/*
 * clgc-playerbar: Calgici PlayerBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-playerbar.h"

#include "clgc-adaptive-box.h"
#include "clgc-playback-commands.h"
#include "clgc-playback-mode.h"

#include <libintl.h>

enum _ClgcPlayerBarProperty
{
	PROP_0,
	PROP_REVEAL,
	N_PROPERTIES,
};

static GParamSpec* obj_properties[N_PROPERTIES] = { NULL, };

struct _ClgcPlayerBar
{
	GtkWidget m_parent;

	GtkWidget* m_revealer;

	GtkWidget* m_commands;

	GtkWidget* m_infobox;
	GtkWidget* m_titlesong;
	GtkWidget* m_artistsong;

	GtkWidget* m_timesong;

	GtkWidget* m_mode;
};

G_DEFINE_TYPE(ClgcPlayerBar, clgc_playerbar, GTK_TYPE_WIDGET)

static void clgc_playerbar_dispose(GObject* obj)
{
	ClgcPlayerBar* self = CLGC_PLAYERBAR(obj);

	g_clear_pointer(&(self->m_revealer), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_playerbar_parent_class)->dispose(obj);
}

static void clgc_actionbar_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcPlayerBar* self = CLGC_PLAYERBAR(obj);

	switch (property_id)
	{
		case PROP_REVEAL:
			g_value_set_boolean(value, clgc_playerbar_get_reveal(self));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_actionbar_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcPlayerBar* self = CLGC_PLAYERBAR(obj);

	switch (property_id)
	{
		case PROP_REVEAL:
			clgc_playerbar_set_reveal(self, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_playerbar_class_init(ClgcPlayerBarClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_playerbar_dispose;
	obj_class->get_property = clgc_actionbar_get_property;
	obj_class->set_property = clgc_actionbar_set_property;

	obj_properties[PROP_REVEAL] = g_param_spec_boolean(
		"reveal",
		NULL,
		NULL,
		TRUE,
		G_PARAM_READWRITE
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-playerbar.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_revealer);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_commands);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_infobox);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_titlesong);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_artistsong);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_timesong);
	gtk_widget_class_bind_template_child(widget_class, ClgcPlayerBar, m_mode);

	g_type_ensure(CLGC_TYPE_ADAPTIVE_BOX);
	g_type_ensure(CLGC_TYPE_PLAYBACK_COMMANDS);
	g_type_ensure(CLGC_TYPE_PLAYBACK_MODE);
}

static void clgc_playerbar_init(ClgcPlayerBar* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_playerbar_new()
{
	return g_object_new(CLGC_TYPE_PLAYERBAR, NULL);
}

GtkWidget* clgc_playerbar_get_info_box(ClgcPlayerBar* self)
{
	return self->m_infobox;
}

gboolean clgc_playerbar_get_reveal(ClgcPlayerBar* self)
{
	return gtk_revealer_get_reveal_child(GTK_REVEALER(self->m_revealer));
}

GtkWidget* clgc_playerbar_get_time_box(ClgcPlayerBar* self)
{
	return self->m_timesong;
}

void clgc_playerbar_set_playback(ClgcPlayerBar* self, ClgcPlayback* playback)
{
	const char* title = "";
	const char* artist = "";
	ClgcInfoSong* song = clgc_playback_get_song(playback);
	ClgcPlaybackState state = clgc_playback_get_state(playback);

	if (song == NULL)
		gtk_widget_set_visible(self->m_timesong, FALSE);
	else
	{
		title = clgc_info_song_get_title(song);
		artist = clgc_info_song_get_artist(song);
	}

	gtk_label_set_text(GTK_LABEL(self->m_titlesong), title);
	gtk_label_set_text(GTK_LABEL(self->m_artistsong), artist);

	clgc_playback_commands_set_state(CLGC_PLAYBACK_COMMANDS(self->m_commands), state);
}

void clgc_playerbar_set_playback_status(ClgcPlayerBar* self, ClgcPlaybackStatus status)
{
	clgc_playback_mode_set_status(CLGC_PLAYBACK_MODE(self->m_mode), status);
}

void clgc_playerbar_set_reveal(ClgcPlayerBar* self, gboolean reveal)
{
	if (clgc_playerbar_get_reveal(self) == reveal)
		return;

	gtk_revealer_set_reveal_child(GTK_REVEALER(self->m_revealer), reveal);
	g_object_notify_by_pspec(G_OBJECT(self), obj_properties[PROP_REVEAL]);
}

void clgc_playerbar_set_time_song(ClgcPlayerBar* self, unsigned time)
{
	// TODO
}
