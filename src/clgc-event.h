/*
 * clgc-event: Calgici Event class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_EVENT__
#define __CALGICI_EVENT__

typedef enum _ClgcEvent
{
	CLGC_EVENT_NULL = 0,
	
	// MPD Event
	CLGC_EVENT_DATABASE = 0x1,
	CLGC_EVENT_QUEUE = 0x4,
	CLGC_EVENT_PLAYER = 0x8,
	CLGC_EVENT_MIXER = 0x10,
	CLGC_EVENT_OUTPUT = 0x20,
	CLGC_EVENT_OPTIONS = 0x40,
	CLGC_EVENT_ALL = 0xFF,
} ClgcEvent;

#endif // __CALGICI_EVENT__
