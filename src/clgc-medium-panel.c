/*
 * clgc-medium-panel: Calgici MediumPanel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-medium-panel.h"

#include "clgc-actionbar.h"
#include "clgc-headerbar.h"
#include "clgc-panel.h"
#include "clgc-selectionbar.h"
#include "clgc-medium-page.h"
#include "clgc-widget-private.h"


enum _ClgcMediumProperty
{
	PROP_0,
	PROP_ORIENTATION,
	N_PROPERTIES = PROP_ORIENTATION
};

struct _ClgcMediumPanel
{
	GtkWidget m_parent;

	GtkWidget* m_titlebox;
	GtkWidget* m_title;
	GtkWidget* m_subtitle;

	GtkWidget* m_headerbar;
	GtkWidget* m_selectionbar;
	GtkWidget* m_page;
	GtkWidget* m_actionbar;
};

static void clgc_medium_panel_interface_init(ClgcPanelInterface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcMediumPanel, clgc_medium_panel, GTK_TYPE_WIDGET,
	G_IMPLEMENT_INTERFACE(GTK_TYPE_ORIENTABLE, NULL)
	G_IMPLEMENT_INTERFACE(CLGC_TYPE_PANEL, clgc_medium_panel_interface_init)
)

static void clgc_medium_panel_dispose(GObject* obj)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(obj);

	g_clear_pointer(&(self->m_headerbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_selectionbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_page), gtk_widget_unparent);
	g_clear_pointer(&(self->m_actionbar), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_medium_panel_parent_class)->dispose(obj);
}

static void clgc_medium_panel_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_ORIENTATION:
			g_value_set_enum(value, gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_medium_panel_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	GtkOrientation orientation;
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(object);
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_ORIENTATION:
			{
				orientation = g_value_get_enum (value);

				if (gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)) != orientation)
				{
					gtk_orientable_set_orientation(GTK_ORIENTABLE(layout), orientation);
					clgc_widget_update_orientation(GTK_WIDGET(self), orientation);
					g_object_notify_by_pspec(object, pspec);
				}
			}
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_medium_panel_set_mode(ClgcPanel* panel, ClgcMode mode)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	clgc_page_set_mode(CLGC_PAGE(self->m_page), mode);

	switch (mode)
	{
		case CLGC_MODE_NORMAL:
			gtk_widget_set_visible(self->m_headerbar, TRUE);
			gtk_widget_set_visible(self->m_selectionbar, FALSE);
			gtk_widget_set_visible(self->m_actionbar, FALSE);
			break;
		case CLGC_MODE_SELECTION:
			gtk_widget_set_visible(self->m_headerbar, FALSE);
			gtk_widget_set_visible(self->m_selectionbar, TRUE);
			gtk_widget_set_visible(self->m_actionbar, TRUE);
			break;
		default:
			break;
	}
}

static void clgc_medium_panel_selection_all(ClgcPanel* panel)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	clgc_page_selection_all(CLGC_PAGE(self->m_page));
}

static void clgc_medium_panel_selection_change(ClgcPanel* panel, int selected)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	if (selected > 0)
	{
		gtk_widget_set_visible(self->m_headerbar, FALSE);
		gtk_widget_set_visible(self->m_selectionbar, TRUE);
		gtk_widget_set_visible(self->m_actionbar, TRUE);

		clgc_actionbar_enable_buttons(CLGC_ACTIONBAR(self->m_actionbar));
	}
	else
	{
		gtk_widget_set_visible(self->m_headerbar, TRUE);
		gtk_widget_set_visible(self->m_selectionbar, FALSE);
		gtk_widget_set_visible(self->m_actionbar, FALSE);

		clgc_actionbar_disable_buttons(CLGC_ACTIONBAR(self->m_actionbar));
	}

	clgc_selectionbar_selection_change(CLGC_SELECTIONBAR(self->m_selectionbar), selected);
}

static void clgc_medium_panel_selection_none(ClgcPanel* panel)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	clgc_page_selection_none(CLGC_PAGE(self->m_page));
}

static void clgc_medium_panel_send_add(ClgcPanel* panel)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	clgc_page_send_add(CLGC_PAGE(self->m_page));
}

static void clgc_medium_panel_send_remove(ClgcPanel* panel)
{
	ClgcMediumPanel* self = CLGC_MEDIUM_PANEL(panel);

	//clgc_page_send_remove(CLGC_PAGE(self->m_page));
}

static void clgc_medium_panel_interface_init(ClgcPanelInterface* iface)
{
	iface->set_mode = clgc_medium_panel_set_mode;

	iface->selection_all = clgc_medium_panel_selection_all;
	iface->selection_change = clgc_medium_panel_selection_change;
	iface->selection_none = clgc_medium_panel_selection_none;

	iface->send_add = clgc_medium_panel_send_add;
	iface->send_remove = clgc_medium_panel_send_remove;
}

static void clgc_medium_panel_class_init(ClgcMediumPanelClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_medium_panel_dispose;
	obj_class->get_property = clgc_medium_panel_get_property;
	obj_class->set_property = clgc_medium_panel_set_property;

	g_object_class_override_property(obj_class, PROP_ORIENTATION, "orientation");

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BOX_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-medium-panel.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_headerbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_titlebox);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_title);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_subtitle);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_selectionbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_page);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPanel, m_actionbar);

	g_type_ensure(CLGC_TYPE_HEADERBAR);
	g_type_ensure(CLGC_TYPE_SELECTIONBAR);
	g_type_ensure(CLGC_TYPE_MEDIUM_PAGE);
	g_type_ensure(CLGC_TYPE_ACTIONBAR);
}

static void clgc_medium_panel_init(ClgcMediumPanel* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_medium_panel_new()
{
	return g_object_new(CLGC_TYPE_MEDIUM_PANEL, "orientation", GTK_ORIENTATION_VERTICAL, NULL);
}

void clgc_medium_panel_update(ClgcMediumPanel* self, ClgcTag* tag, GList* list)
{
	gtk_label_set_text(GTK_LABEL(self->m_title), clgc_tag_get_value(tag));
	gtk_label_set_text(GTK_LABEL(self->m_subtitle), clgc_tag_get_name(tag));

	clgc_medium_panel_set_mode(CLGC_PANEL(self), CLGC_MODE_NORMAL);

	clgc_medium_page_update(CLGC_MEDIUM_PAGE(self->m_page), list);
}
