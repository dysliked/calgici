/*
 * clgc-medium-page: Calgici MediumPage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-medium-page.h"

#include "clgc-page.h"
#include "clgc-songs-view.h"
#include "clgc-tag-art.h"
#include "utils.h"

#include <adwaita.h>
#include <libintl.h>

#define SIZE_WIDGET 160

struct _ClgcMediumPage
{
	GtkWidget m_parent;

	GtkWidget* m_scrolledwindow;
	GtkWidget* m_box;
	GtkWidget* m_boxtitle;
	GtkWidget* m_art;
	GtkWidget* m_nbtracks;
	GtkWidget* m_totaltime;
	GtkWidget* m_clamp;
	GtkWidget* m_view;
};

static void clgc_medium_page_interface_init(ClgcPageInterface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcMediumPage, clgc_medium_page, GTK_TYPE_WIDGET, G_IMPLEMENT_INTERFACE(CLGC_TYPE_PAGE, clgc_medium_page_interface_init))

static void clgc_medium_page_dispose(GObject* obj)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(obj);

	g_clear_pointer(&(self->m_scrolledwindow), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_medium_page_parent_class)->dispose(obj);
}

static void clgc_medium_page_set_mode(ClgcPage* page, ClgcMode mode)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(page);

	if (mode == CLGC_MODE_NORMAL)
		clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_view), 0);
}

static void clgc_medium_page_selection_all(ClgcPage* page)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(page);

	clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_view), 1);
}

static void clgc_medium_page_selection_none(ClgcPage* page)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(page);

	clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_view), 0);
}

static void clgc_medium_page_send_add(ClgcPage* page)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(page);

	clgc_songs_view_send_add_songs(CLGC_SONGS_VIEW(self->m_view));
}

static void clgc_medium_page_send_remove(ClgcPage* page)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(page);

	clgc_songs_view_send_remove_songs(CLGC_SONGS_VIEW(self->m_view));
}

static void clgc_medium_page_interface_init(ClgcPageInterface* iface)
{
	iface->set_mode = clgc_medium_page_set_mode;

	iface->selection_all = clgc_medium_page_selection_all;
	iface->selection_none = clgc_medium_page_selection_none;

	iface->send_add = clgc_medium_page_send_add;
	iface->send_remove = clgc_medium_page_send_remove;
}

static void clgc_medium_page_class_init(ClgcMediumPageClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_medium_page_dispose;

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-medium-page.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_scrolledwindow);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_box);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_boxtitle);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_art);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_nbtracks);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_totaltime);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_clamp);
	gtk_widget_class_bind_template_child(widget_class, ClgcMediumPage, m_view);

	g_type_ensure(CLGC_TYPE_SONGS_VIEW);
	g_type_ensure(CLGC_TYPE_TAG_ART);
}

static void clgc_medium_page_init(ClgcMediumPage* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_medium_page_new()
{
	return g_object_new(CLGC_TYPE_MEDIUM_PAGE, NULL);
}

void clgc_medium_page_update_aux(gpointer data, gpointer user_data)
{
	ClgcMediumPage* self = CLGC_MEDIUM_PAGE(user_data);

	clgc_songs_view_append(CLGC_SONGS_VIEW(self->m_view), CLGC_INFO_SONG(data));
}

void clgc_song_list_get_time(gpointer data, gpointer user_data)
{
	GDateTime* time = clgc_info_song_get_time(CLGC_INFO_SONG(data));
	GDateTime** user_time = (GDateTime**) user_data;

	GDateTime* tmp_time = g_date_time_add_seconds(*user_time, g_date_time_to_unix(time));

	g_date_time_unref(*user_time);
	*user_time = tmp_time;
}

GString* g_date_time_to_string(GDateTime* date_time)
{
	GString* string = g_string_new("");
	gint hours = g_date_time_get_hour(date_time);
	gint minutes = g_date_time_get_minute(date_time);
	gint seconds = g_date_time_get_second(date_time);

	if (hours)
		g_string_append_printf(string, ngettext("%d hour", "%d hours", hours), hours);

	if (minutes)
	{
		if (string->len > 0)
			g_string_append(string, ", ");

		g_string_append_printf(string, ngettext("%d minute", "%d minutes", minutes), minutes);
	}

	if (seconds)
	{
		if (string->len > 0)
			g_string_append(string, ", ");

		g_string_append_printf(string, ngettext("%d second", "%d seconds", seconds), seconds);
	}

	return string;
}

void clgc_medium_page_update(ClgcMediumPage* self, GList* list)
{
	guint length = g_list_length(list);
	GDateTime* time_length = g_date_time_new_from_unix_utc(0);
	gchar* nbtracks_str = g_strdup_printf(ngettext("%d Track", "%d Tracks", length), length);
	//gchar* filename = clgc_utils_get_albumart_filename(value);

	clgc_songs_view_clear(CLGC_SONGS_VIEW(self->m_view));
	g_list_foreach(list, clgc_medium_page_update_aux, self);
	g_list_foreach(list, clgc_song_list_get_time, &time_length);

	GString* time_str = g_date_time_to_string(time_length);

	clgc_songs_view_set_mode(CLGC_SONGS_VIEW(self->m_view), CLGC_MODE_SELECTION);

	gtk_label_set_label(GTK_LABEL(self->m_nbtracks), nbtracks_str);
	gtk_label_set_label(GTK_LABEL(self->m_totaltime), time_str->str);
	g_free(nbtracks_str);
	g_string_free(time_str, TRUE);
	//clgc_tag_art_set_tag(CLGC_TAG_ART(self->m_art), tag);
	//clgc_tag_art_set_image(CLGC_TAG_ART(self->m_art), filename);
	//g_free(filename);
}
