/*
 * clgc-info-song: Calgici InfoSong class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-info-song.h"

enum _ClgcInfoSongProperty
{
	PROP_0,
	PROP_ID,
	PROP_TITLE,
	PROP_ALBUM,
	PROP_ARTIST,
	PROP_TRACK,
	PROP_TIME,
	PROP_PATH,
	N_PROPERTIES
};

struct _ClgcInfoSong
{
	GObject m_parent;

	unsigned m_id;
	char* m_title;
	char* m_album;
	char* m_artist;
	char* m_track;
	GDateTime* m_time;
	char* m_path;
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcInfoSong, clgc_info_song, G_TYPE_OBJECT)

static void clgc_info_song_finalize(GObject* obj)
{
	ClgcInfoSong* self = CLGC_INFO_SONG(obj);

	g_free(self->m_title);
	g_free(self->m_album);
	g_free(self->m_artist);
	g_free(self->m_track);
	g_free(self->m_path);

	G_OBJECT_CLASS(clgc_info_song_parent_class)->finalize(obj);
}

static void clgc_info_song_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcInfoSong* self = CLGC_INFO_SONG(obj);

	switch (property_id)
	{
		case PROP_ID:
			g_value_set_uint(value, clgc_info_song_get_id(self));
			break;
		case PROP_TITLE:
			g_value_set_string(value, clgc_info_song_get_title(self));
			break;
		case PROP_ALBUM:
			g_value_set_string(value, clgc_info_song_get_album(self));
			break;
		case PROP_ARTIST:
			g_value_set_string(value, clgc_info_song_get_artist(self));
			break;
		case PROP_TRACK:
			g_value_set_string(value, clgc_info_song_get_track(self));
			break;
		case PROP_TIME:
			g_value_set_pointer(value, clgc_info_song_get_time(self));
			break;
		case PROP_PATH:
			g_value_set_string(value, clgc_info_song_get_path(self));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_info_song_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcInfoSong* self = CLGC_INFO_SONG(obj);

	switch (property_id)
	{
		case PROP_ID:
			self->m_id = g_value_get_uint(value);
			break;
		case PROP_TITLE:
			self->m_title = g_strdup(g_value_get_string(value));
			break;
		case PROP_ALBUM:
			self->m_album = g_strdup(g_value_get_string(value));
			break;
		case PROP_ARTIST:
			self->m_artist = g_strdup(g_value_get_string(value));
			break;
		case PROP_TRACK:
			self->m_track = g_strdup(g_value_get_string(value));
			break;
		case PROP_TIME:
			self->m_time = g_value_get_pointer(value);
			break;
		case PROP_PATH:
			self->m_path = g_strdup(g_value_get_string(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_info_song_class_init(ClgcInfoSongClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);

	obj_class->finalize = clgc_info_song_finalize;
	obj_class->get_property = clgc_info_song_get_property;
	obj_class->set_property = clgc_info_song_set_property;

	obj_properties[PROP_ID] = g_param_spec_uint("id", "ID", "The identifient of the song in the queue", 0, G_MAXUINT, 0, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_TITLE] = g_param_spec_string("title", "Title", "The title of the song", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_ALBUM] = g_param_spec_string("album", "Album", "The album of the song", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_ARTIST] = g_param_spec_string("artist", "Artist", "The artist of the song", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_TRACK] = g_param_spec_string("track", "Track", "The track of the song in the album", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_TIME] = g_param_spec_pointer("time", "Time", "The time of the song", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_PATH] = g_param_spec_string("path", "Path", "The path in the filesystem of the song", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
}

static void clgc_info_song_init(ClgcInfoSong* self)
{
}

ClgcInfoSong* clgc_info_song_new(unsigned id, const char* title, const char* album, const char* artist, const char* track, GDateTime* time, const char* path)
{
	return g_object_new(CLGC_TYPE_INFO_SONG,
			"id", id,
			"title", title,
			"album", album,
			"artist", artist,
			"track", track,
			"time", time,
			"path", path,
			NULL);
}

unsigned clgc_info_song_get_id(ClgcInfoSong* self)
{
	return self->m_id;
}

const char* clgc_info_song_get_title(ClgcInfoSong* self)
{
	return self->m_title;
}

const char* clgc_info_song_get_album(ClgcInfoSong* self)
{
	return self->m_album;
}

const char* clgc_info_song_get_artist(ClgcInfoSong* self)
{
	return self->m_artist;
}

const char* clgc_info_song_get_track(ClgcInfoSong* self)
{
	return self->m_track;
}

GDateTime* clgc_info_song_get_time(ClgcInfoSong* self)
{
	return self->m_time;
}

const char* clgc_info_song_get_path(ClgcInfoSong* self)
{
	return self->m_path;
}
