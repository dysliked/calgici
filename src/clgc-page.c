/*
 * clgc-page: Calgici Page class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-page.h"

G_DEFINE_INTERFACE(ClgcPage, clgc_page, G_TYPE_OBJECT)

static void clgc_page_default_init(ClgcPageInterface* iface)
{

}

void clgc_page_set_mode(ClgcPage* self, ClgcMode mode)
{
	ClgcPageInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PAGE(self));

	iface = CLGC_PAGE_GET_IFACE(self);
	iface->set_mode(self, mode);
}

void clgc_page_selection_all(ClgcPage* self)
{
	ClgcPageInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PAGE(self));

	iface = CLGC_PAGE_GET_IFACE(self);
	iface->selection_all(self);
}

void clgc_page_selection_none(ClgcPage* self)
{
	ClgcPageInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PAGE(self));

	iface = CLGC_PAGE_GET_IFACE(self);
	iface->selection_none(self);
}

void clgc_page_send_add(ClgcPage* self)
{
	ClgcPageInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PAGE(self));

	iface = CLGC_PAGE_GET_IFACE(self);
	iface->send_add(self);
}

void clgc_page_send_remove(ClgcPage* self)
{
	ClgcPageInterface* iface = NULL;

	g_return_if_fail(CLGC_IS_PAGE(self));

	iface = CLGC_PAGE_GET_IFACE(self);
	iface->send_remove(self);
}
