/*
 * clgc-mpd-client: Calgici MPDClient class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-mpd-client.h"

#include <mpd/client.h>

#include <stdio.h>

enum _ClgcMPDClientProperty
{
	PROP_CONNECTION = 1,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcMPDClient
{
	GObject m_parent;

	GMutex m_mutex;

	struct mpd_connection* m_connection;
};

G_DEFINE_TYPE(ClgcMPDClient, clgc_mpd_client, G_TYPE_OBJECT)

static ClgcInfoSong* mpd_song_to_clgc_info_song(const struct mpd_song* song)
{
	return clgc_info_song_new(
			mpd_song_get_id(song),
			mpd_song_get_tag(song, MPD_TAG_TITLE, 0),
			mpd_song_get_tag(song, MPD_TAG_ALBUM, 0),
			mpd_song_get_tag(song, MPD_TAG_ARTIST, 0),
			mpd_song_get_tag(song, MPD_TAG_TRACK, 0),
			g_date_time_new_from_unix_utc(mpd_song_get_duration(song)),
			mpd_song_get_uri(song)
			);
}

static ClgcAudioOutput* mpd_output_to_clgc_audio_output(const struct mpd_output* output)
{
	return clgc_audio_output_new(mpd_output_get_id(output), mpd_output_get_name(output), mpd_output_get_enabled(output));
}

static void clgc_mpd_client_finalize(GObject* obj)
{
	ClgcMPDClient* self = CLGC_MPD_CLIENT(obj);

	G_OBJECT_CLASS(clgc_mpd_client_parent_class)->finalize(obj);

	mpd_connection_free(self->m_connection);
}

static void clgc_mpd_client_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcMPDClient* self = CLGC_MPD_CLIENT(obj);

	switch (property_id)
	{
		case PROP_CONNECTION:
			g_value_set_pointer(value, self->m_connection);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_mpd_client_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcMPDClient* self = CLGC_MPD_CLIENT(obj);

	switch (property_id)
	{
		case PROP_CONNECTION:
			self->m_connection = (struct mpd_connection*) g_value_get_pointer(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_mpd_client_class_init(ClgcMPDClientClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);

	obj_class->finalize = clgc_mpd_client_finalize;
	obj_class->get_property = clgc_mpd_client_get_property;
	obj_class->set_property = clgc_mpd_client_set_property;

	obj_properties[PROP_CONNECTION] = g_param_spec_pointer("connection", "Connection", "The pointer returned by the mpd_connection_new", G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
}

static void clgc_mpd_client_init(ClgcMPDClient* self)
{
	g_mutex_init(&(self->m_mutex));
}

ClgcMPDClient* clgc_mpd_client_new(struct mpd_connection* connection)
{
	return g_object_new(CLGC_TYPE_MPD_CLIENT, "connection", connection, NULL);
}

void clgc_mpd_client_add_song(ClgcMPDClient* self, const char* song_uri)
{
	mpd_send_add(self->m_connection, song_uri);
}

static void clgc_mpd_client_add_songs_aux(gpointer data, gpointer user_data)
{
	ClgcMPDClient* self = CLGC_MPD_CLIENT(user_data);
	const char* song_uri = (const char*) data;

	clgc_mpd_client_add_song(self, song_uri);
	clgc_mpd_client_send_request(self);
}

void clgc_mpd_client_add_songs(ClgcMPDClient* self, GList* list)
{
	g_list_foreach(list, clgc_mpd_client_add_songs_aux, self);
}

ClgcMPDClient* clgc_mpd_client_connect(const char* address, unsigned port, unsigned timeout)
{
	void* connection = mpd_connection_new(address, port, timeout);

	if (!connection)
	{
		g_warning("Impossible to connect to the server 'mpd://%s:%u/'", address, port);
		return NULL;
	}

	if (mpd_connection_get_error(connection) != MPD_ERROR_SUCCESS)
	{
		g_warning("%s", mpd_connection_get_error_message(connection));
		mpd_connection_free(connection);
		return NULL;
	}

	return clgc_mpd_client_new(connection);
}

static enum mpd_tag_type clgc_tag_to_mpd_tag(ClgcTagType type)
{
	switch (type)
	{
		case CLGC_TAG_ALBUM: return MPD_TAG_ALBUM;
		case CLGC_TAG_ARTIST: return MPD_TAG_ARTIST;
		case CLGC_TAG_ALBUM_ARTIST: return MPD_TAG_ALBUM_ARTIST;
		case CLGC_TAG_GENRE: return MPD_TAG_GENRE;
		case CLGC_TAG_DATE: return MPD_TAG_DATE;
		default: return MPD_TAG_UNKNOWN;
	}
}

static GList* clgc_mpd_client_get_medium_list(ClgcMPDClient* self)
{
	GList* list = NULL;
	struct mpd_song* response = NULL;
	ClgcInfoSong* song = NULL;

	while ((response = mpd_recv_song(self->m_connection)) != NULL)
	{
		song = mpd_song_to_clgc_info_song(response);
		mpd_song_free(response);

		list = g_list_append(list, song);
	}

	return list;
}

static GList* clgc_mpd_client_get_tag_list(ClgcMPDClient* self, ClgcTagType type)
{
	GList* list = NULL;
	struct mpd_pair* response = NULL;
	char* value = NULL;

	while ((response = mpd_recv_pair_tag(self->m_connection, clgc_tag_to_mpd_tag(type))) != NULL)
	{
		value = strdup(response->value);
		mpd_return_pair(self->m_connection, response);

		list = g_list_append(list, value);
	}

	return list;
}

GList* clgc_mpd_client_get_data_from_tag(ClgcMPDClient* self, ClgcTag* tag)
{
	ClgcTagType type = clgc_tag_get_key(tag);
	const char* value = clgc_tag_get_value(tag);

	mpd_search_cancel(self->m_connection);

	if (value)
	{
		mpd_search_db_songs(self->m_connection, true);
		mpd_search_add_tag_constraint(self->m_connection, MPD_OPERATOR_DEFAULT, clgc_tag_to_mpd_tag(type), value);
	}
	else
		mpd_search_db_tags(self->m_connection, clgc_tag_to_mpd_tag(type));

	if (mpd_search_commit(self->m_connection) == FALSE)
	{
		g_warning(mpd_connection_get_error_message(self->m_connection));
		return NULL;
	}

	if (value)
		return clgc_mpd_client_get_medium_list(self);

	return clgc_mpd_client_get_tag_list(self, type);
}

ClgcEvent clgc_mpd_client_get_event(ClgcMPDClient* self)
{
	return (ClgcEvent) mpd_recv_idle(self->m_connection, FALSE);
}

GIOChannel* clgc_mpd_client_get_channel(ClgcMPDClient* self)
{
	int fd = mpd_connection_get_fd(self->m_connection);

	if (fd < 0)
		return NULL;

	return g_io_channel_unix_new(fd);
}

GList* clgc_mpd_client_get_audio_outputs(ClgcMPDClient* self)
{
	GList* list = NULL;
	struct mpd_output* output = NULL;

	if (!mpd_send_outputs(self->m_connection))
	{
		g_warning(mpd_connection_get_error_message(self->m_connection));
		return NULL;
	}

	while ((output = mpd_recv_output(self->m_connection)))
	{
		list = g_list_append(list, mpd_output_to_clgc_audio_output(output));
		mpd_output_free(output);
	}

	return list;
}

GMutex* clgc_mpd_client_get_mutex(ClgcMPDClient* self)
{
	return &(self->m_mutex);
}

ClgcPlayback* clgc_mpd_client_get_playback(ClgcMPDClient* self)
{
	return clgc_playback_new(clgc_mpd_client_get_playback_song(self), clgc_mpd_client_get_playback_state(self));
}

ClgcInfoSong* clgc_mpd_client_get_playback_song(ClgcMPDClient* self)
{
	ClgcInfoSong* song = NULL;
	struct mpd_song* mpdsong = mpd_run_current_song(self->m_connection);

	if (!mpdsong)
		return NULL;

	song = mpd_song_to_clgc_info_song(mpdsong);
	mpd_song_free(mpdsong);

	return song;
}

ClgcPlaybackStatus clgc_mpd_client_get_playback_status(ClgcMPDClient* self)
{
	ClgcPlaybackStatus flag = CLGC_PLAYBACK_STATUS_NONE;
	struct mpd_status* status = NULL;

	status = mpd_run_status(self->m_connection);
	flag |= (mpd_status_get_random(status)) ? CLGC_PLAYBACK_STATUS_RANDOM : 0;
	flag |= (mpd_status_get_repeat(status)) ? CLGC_PLAYBACK_STATUS_REPEAT : 0;
	flag |= (mpd_status_get_single(status)) ? CLGC_PLAYBACK_STATUS_SINGLE : 0;
	mpd_status_free(status);

	return flag;
}

ClgcPlaybackState clgc_mpd_client_get_playback_state(ClgcMPDClient* self)
{
	struct mpd_status* status = NULL;
	int state = 0;

	status = mpd_run_status(self->m_connection);

	if (!status)
		return CLGC_PLAYBACK_STATE_STOP;

	state = mpd_status_get_state(status);
	mpd_status_free(status);

	switch (state)
	{
		case MPD_STATE_PLAY: return CLGC_PLAYBACK_STATE_PLAY;
		case MPD_STATE_PAUSE: return CLGC_PLAYBACK_STATE_PAUSE;
		default: return CLGC_PLAYBACK_STATE_STOP;
	}
}

GDateTime* clgc_mpd_client_get_playback_time(ClgcMPDClient* self)
{
	struct mpd_status* status = mpd_run_status(self->m_connection);

	if (!status)
	{
		g_warning(mpd_connection_get_error_message(self->m_connection));
		return NULL;
	}

	return g_date_time_new_from_unix_utc(mpd_status_get_elapsed_ms(status));
}

GList* clgc_mpd_client_get_queue(ClgcMPDClient* self)
{
	GList* list = NULL;
	ClgcInfoSong* song = NULL;
	struct mpd_entity* response = NULL;

	if (mpd_send_list_queue_meta(self->m_connection) == false)
		return NULL;

	while ((response = mpd_recv_entity(self->m_connection)) != NULL)
	{
		song = mpd_song_to_clgc_info_song(mpd_entity_get_song(response));
		list = g_list_append(list, song);

		mpd_entity_free(response);
	}

	return list;
}

void clgc_mpd_client_keep_alive(ClgcMPDClient* self)
{
	if (mpd_connection_set_keepalive(self->m_connection, TRUE) == FALSE)
		g_warning(mpd_connection_get_error_message(self->m_connection));
}

void clgc_mpd_client_next(ClgcMPDClient* self)
{
	mpd_send_next(self->m_connection);
}

void clgc_mpd_client_pause(ClgcMPDClient* self)
{
	mpd_send_pause(self->m_connection, TRUE);
}

void clgc_mpd_client_play(ClgcMPDClient* self)
{
	mpd_send_play(self->m_connection);
}

void clgc_mpd_client_previous(ClgcMPDClient* self)
{
	mpd_send_previous(self->m_connection);
}

void clgc_mpd_client_remove_song(ClgcMPDClient* self, unsigned song_id)
{
	mpd_send_delete_id(self->m_connection, song_id);
}

static void clgc_mpd_client_remove_songs_aux(gpointer data, gpointer user_data)
{
	ClgcMPDClient* self = CLGC_MPD_CLIENT(user_data);
	unsigned song_id = GPOINTER_TO_INT(data);

	clgc_mpd_client_remove_song(self, song_id);
	clgc_mpd_client_send_request(self);
}

void clgc_mpd_client_remove_songs(ClgcMPDClient* self, GList* list)
{
	g_list_foreach(list, clgc_mpd_client_remove_songs_aux, self);
}

void clgc_mpd_client_send_request(ClgcMPDClient* self)
{
	mpd_response_finish(self->m_connection);
}

void clgc_mpd_client_set_audio_output(ClgcMPDClient* self, int audio_output_id, gboolean state)
{
	if (state == TRUE)
		mpd_send_enable_output(self->m_connection, audio_output_id);
	else
		mpd_send_disable_output(self->m_connection, audio_output_id);
}

void clgc_mpd_client_set_idle(ClgcMPDClient* self, gboolean state)
{
	if (state == TRUE)
		mpd_send_idle(self->m_connection);
	else
	{
		mpd_send_noidle(self->m_connection);
		mpd_recv_idle(self->m_connection, false);
	}
}

void clgc_mpd_client_set_random(ClgcMPDClient* self, gboolean active)
{
	mpd_send_random(self->m_connection, active);
}

void clgc_mpd_client_set_repeat(ClgcMPDClient* self, gboolean active)
{
	mpd_send_repeat(self->m_connection, active);
}

void clgc_mpd_client_set_single(ClgcMPDClient* self, gboolean active)
{
	mpd_send_single(self->m_connection, active);
}

void clgc_mpd_client_stop(ClgcMPDClient* self)
{
	mpd_send_stop(self->m_connection);
}

unsigned clgc_mpd_client_update_database(ClgcMPDClient* self)
{
	return mpd_run_update(self->m_connection, NULL);
}
