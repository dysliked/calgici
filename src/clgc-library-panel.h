/*
 * clgc-library-panel: Calgici LibraryPanel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_LIBRARY_PANEL__
#define __CALGICI_LIBRARY_PANEL__

#include "clgc-playback.h"
#include "clgc-playback-status.h"
#include "clgc-tag-list.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_LIBRARY_PANEL (clgc_library_panel_get_type())

G_DECLARE_FINAL_TYPE(ClgcLibraryPanel, clgc_library_panel, CLGC, LIBRARY_PANEL, GtkWidget)

/**
 * @brief Creates an instance of library panel
 * @return An instance of library panel
 */
GtkWidget* clgc_library_panel_new();

gboolean clgc_library_panel_get_bar_revealed(ClgcLibraryPanel* self);

ClgcTagList* clgc_library_panel_get_pages(ClgcLibraryPanel* self);

void clgc_library_panel_set_audio_outputs(ClgcLibraryPanel* self, GList* list);

void clgc_library_panel_set_bar_revealed(ClgcLibraryPanel* self, gboolean reveal_bar);

void clgc_library_panel_set_end_space(ClgcLibraryPanel* self, int space);

void clgc_library_panel_set_pages(ClgcLibraryPanel* self, ClgcTagList* list);

void clgc_library_panel_set_playback(ClgcLibraryPanel* self, ClgcPlayback* playback);

void clgc_library_panel_set_playback_status(ClgcLibraryPanel* self, ClgcPlaybackStatus status);

void clgc_library_panel_update_queue(ClgcLibraryPanel* self, GList* list);

void clgc_library_panel_update_page(ClgcLibraryPanel* self, ClgcTag* tag, GList* list);

G_END_DECLS

#endif // __CALGICI_LIBRARY_PANEL__
