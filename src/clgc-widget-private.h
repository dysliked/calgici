/*
 * clgc-widget-private: Calgici WidgetPrivate class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_WIDGET_PRIVATE__
#define __CALGICI_WIDGET_PRIVATE__

#include <gtk/gtk.h>

void clgc_widget_update_orientation(GtkWidget* widget, GtkOrientation orientation);

#endif // __CLGC_WIDGET_PRIVATE__
