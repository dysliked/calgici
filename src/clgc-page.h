/*
 * clgc-page: Calgici Page class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PAGE__
#define __CALGICI_PAGE__

#include "clgc-mode.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define CLGC_TYPE_PAGE (clgc_page_get_type())

G_DECLARE_INTERFACE(ClgcPage, clgc_page, CLGC, PAGE, GObject)

struct _ClgcPageInterface
{
	GTypeInterface m_parent;

	void (*set_mode)(ClgcPage*, ClgcMode);

	void (*selection_all)(ClgcPage*);
	void (*selection_none)(ClgcPage*);

	void (*send_add)(ClgcPage*);
	void (*send_remove)(ClgcPage*);
};

/**
 * @brief Disable the selection mode for the page
 * @param self The page
 */
void clgc_page_set_mode(ClgcPage* self, ClgcMode mode);

/**
 * @brief Manage when all items must be selected
 * @param self The page
 */
void clgc_page_selection_all(ClgcPage* self);

/**
 * @brief Manage when all items must be unselected
 * @param self The page
 */
void clgc_page_selection_none(ClgcPage* self);

/**
 * @brief Add the selection in a list
 * @param self The page
 */
void clgc_page_send_add(ClgcPage* self);

/**
 * @brief Remove the selection in a list
 * @param self The page
 */
void clgc_page_send_remove(ClgcPage* self);

G_END_DECLS

#endif // __CALGICI_PAGE__
