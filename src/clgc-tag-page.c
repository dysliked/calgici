/*
 * clgc-tag-page: Calgici TagPage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-tag-page.h"

#include "clgc-medium-widget.h"
#include "utils.h"

enum _ClgcTagPageProperty
{
	PROP_0,
	PROP_TAG,

	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcTagPage
{
	GtkWidget m_parent;

	ClgcTag* m_tag;

	GtkWidget* m_scrolledwindow;
	GtkWidget* m_flowbox;
};

static void clgc_tag_page_interface_init(ClgcPageInterface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcTagPage, clgc_tag_page, GTK_TYPE_WIDGET, G_IMPLEMENT_INTERFACE(CLGC_TYPE_PAGE, clgc_tag_page_interface_init))

static void clgc_tag_page_dispose(GObject* obj)
{
	ClgcTagPage* self = CLGC_TAG_PAGE(obj);

	g_clear_pointer(&(self->m_scrolledwindow), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_tag_page_parent_class)->dispose(obj);
}

static void clgc_tag_page_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcTagPage* self = CLGC_TAG_PAGE(obj);

	switch (property_id)
	{
		case PROP_TAG:
			g_value_set_boxed(value, self->m_tag);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_tag_page_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcTagPage* self = CLGC_TAG_PAGE(obj);

	switch (property_id)
	{
		case PROP_TAG:
			self->m_tag = clgc_tag_copy(g_value_get_boxed(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_tag_page_set_mode(ClgcPage* self, ClgcMode mode)
{
}

static void clgc_tag_page_selection_all(ClgcPage* self)
{
}

static void clgc_tag_page_selection_none(ClgcPage* self)
{
}

static void clgc_tag_page_send_remove(ClgcPage* self)
{
}

static void clgc_tag_page_on_select(GtkFlowBox* flowbox, GtkFlowBoxChild* child, gpointer user_data)
{
	ClgcTagPage* self = CLGC_TAG_PAGE(user_data);
	GtkWidget* tag_widget = gtk_flow_box_child_get_child(child);
	const char* string = clgc_medium_widget_get_name(CLGC_MEDIUM_WIDGET(tag_widget));

	gtk_widget_activate_action(GTK_WIDGET(self), "app.mpd-client.get-songs", "(is)", clgc_tag_get_key(self->m_tag), string);
}

static void clgc_tag_page_interface_init(ClgcPageInterface* iface)
{
	iface->set_mode = clgc_tag_page_set_mode;

	iface->selection_all = clgc_tag_page_selection_all;
	iface->selection_none = clgc_tag_page_selection_none;

	iface->send_remove = clgc_tag_page_send_remove;
}

static void clgc_tag_page_class_init(ClgcTagPageClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_tag_page_dispose;
	obj_class->get_property = clgc_tag_page_get_property;
	obj_class->set_property = clgc_tag_page_set_property;

	obj_properties[PROP_TAG] = g_param_spec_boxed("tag", "Tag", "The tag of the page", CLGC_TYPE_TAG, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-tag-page.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcTagPage, m_scrolledwindow);
	gtk_widget_class_bind_template_child(widget_class, ClgcTagPage, m_flowbox);
}

static void clgc_tag_page_init(ClgcTagPage* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));

	g_signal_connect(G_OBJECT(self->m_flowbox), "child-activated", G_CALLBACK(clgc_tag_page_on_select), self);
}

GtkWidget* clgc_tag_page_new(ClgcTag* tag)
{
	return g_object_new(CLGC_TYPE_TAG_PAGE, "tag", tag, NULL);
}

ClgcTag* clgc_tag_page_get_tag(ClgcTagPage* self)
{
	return self->m_tag;
}

void clgc_tag_page_update_aux(gpointer data, gpointer user_data)
{
	ClgcTagPage* self = CLGC_TAG_PAGE(user_data);
	GtkWidget* medium_widget = NULL;
	char* tag_name = (char*) data;
	gchar* filename = NULL;

	filename = clgc_utils_get_albumart_filename(tag_name);
	medium_widget = clgc_medium_widget_new(clgc_tag_get_key(self->m_tag), tag_name);

	if (g_file_test(filename, G_FILE_TEST_EXISTS))
		clgc_medium_widget_set_image(CLGC_MEDIUM_WIDGET(medium_widget), filename);

	gtk_widget_set_visible(medium_widget, TRUE);

	gtk_flow_box_insert(GTK_FLOW_BOX(self->m_flowbox), medium_widget, -1);
	g_free(filename);
}

void clgc_tag_page_update(ClgcTagPage* self, GList* list)
{
	g_list_foreach(list, clgc_tag_page_update_aux, self);
}
