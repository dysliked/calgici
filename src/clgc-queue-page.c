/*
 * clgc-queue-page: Calgici QueuePage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-queue-page.h"

#include "clgc-songs-view.h"

struct _ClgcQueuePage
{
	GtkWidget m_parent;

	GtkWidget* m_scrolledwindow;
	GtkWidget* m_box;
};

static void clgc_queue_page_interface_init(ClgcPageInterface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcQueuePage, clgc_queue_page, GTK_TYPE_WIDGET, G_IMPLEMENT_INTERFACE(CLGC_TYPE_PAGE, clgc_queue_page_interface_init))

static void clgc_queue_page_dispose(GObject* obj)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(obj);

	g_clear_pointer(&(self->m_scrolledwindow), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_queue_page_parent_class)->dispose(obj);
}

static void clgc_queue_page_update_current_song(ClgcQueuePage* self)
{
	void* song = NULL;//clgc_mpc_get_current_song(clgc_mpc_manager_get());
	ClgcPlaybackState state = 0;//clgc_mpc_get_status(clgc_mpc_manager_get());

	if (!song)
		return;

	clgc_songs_view_set_current_song(CLGC_SONGS_VIEW(self->m_box), song, state);
}

static void clgc_queue_page_set_mode(ClgcPage* page, ClgcMode mode)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(page);

	clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_box), 0);
	clgc_songs_view_set_mode(CLGC_SONGS_VIEW(self->m_box), mode);
}

static void clgc_queue_page_selection_all(ClgcPage* page)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(page);

	clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_box), 1);
}

static void clgc_queue_page_selection_none(ClgcPage* page)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(page);

	clgc_songs_view_set_selected(CLGC_SONGS_VIEW(self->m_box), 0);
}

static void clgc_queue_page_send_remove(ClgcPage* page)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(page);

	clgc_songs_view_send_remove_songs(CLGC_SONGS_VIEW(self->m_box));
}

static void clgc_queue_page_interface_init(ClgcPageInterface* iface)
{
	iface->set_mode = clgc_queue_page_set_mode;

	iface->selection_all = clgc_queue_page_selection_all;
	iface->selection_none = clgc_queue_page_selection_none;

	iface->send_remove = clgc_queue_page_send_remove;
}

static void clgc_queue_page_class_init(ClgcQueuePageClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_queue_page_dispose;

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-queue-page.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcQueuePage, m_scrolledwindow);
	gtk_widget_class_bind_template_child(widget_class, ClgcQueuePage, m_box);

	g_type_ensure(CLGC_TYPE_SONGS_VIEW);
}

static void clgc_queue_page_init(ClgcQueuePage* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_queue_page_new()
{
	return g_object_new(CLGC_TYPE_QUEUE_PAGE, NULL);
}

void clgc_queue_page_set_playback(ClgcQueuePage* self, ClgcPlayback* playback)
{
	ClgcInfoSong* song = clgc_playback_get_song(playback);
	ClgcPlaybackState state = clgc_playback_get_state(playback);

	clgc_songs_view_set_current_song(CLGC_SONGS_VIEW(self->m_box), song, state);
}

static void clgc_queue_page_update_aux(gpointer data, gpointer user_data)
{
	ClgcQueuePage* self = CLGC_QUEUE_PAGE(user_data);

	clgc_songs_view_append(CLGC_SONGS_VIEW(self->m_box), data);
}

void clgc_queue_page_update(ClgcQueuePage* self, GList* list)
{
	clgc_songs_view_clear(CLGC_SONGS_VIEW(self->m_box));
	g_list_foreach(list, clgc_queue_page_update_aux, self);
	clgc_queue_page_update_current_song(self);
}
