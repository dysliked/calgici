/*
 * clgc-audio-output: Calgici AudioOuptut class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_AUDIO_OUTPUT__
#define __CALGICI_AUDIO_OUTPUT__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_AUDIO_OUTPUT (clgc_audio_output_get_type())

G_DECLARE_FINAL_TYPE(ClgcAudioOutput, clgc_audio_output, CLGC, AUDIO_OUTPUT, GObject)

ClgcAudioOutput* clgc_audio_output_new(int id, const char* name, gboolean enabled);

int clgc_audio_output_action_name_get_id(const char* action_name);

int clgc_audio_output_get_id(ClgcAudioOutput* self);

const char* clgc_audio_output_get_name(ClgcAudioOutput* self);

const char* clgc_audio_output_get_action_name(ClgcAudioOutput* self);

gboolean clgc_audio_output_is_enabled(ClgcAudioOutput* self);

G_END_DECLS

#endif // __CALGICI_AUDIO_OUTPUT__
