/*
 * clgc-playback-commands: Calgici PlaybackCommands class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_PLAYBACK_COMMANDS__
#define __CALGICI_PLAYBACK_COMMANDS__

#include "clgc-playback-state.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_PLAYBACK_COMMANDS (clgc_playback_commands_get_type())

G_DECLARE_FINAL_TYPE(ClgcPlaybackCommands, clgc_playback_commands, CLGC, PLAYBACK_COMMANDS, GtkWidget)

typedef enum _ClgcPlaybackCommandsPolicy
{
	CLGC_PLAYBACK_COMMANDS_POLICY_AUTO,
	CLGC_PLAYBACK_COMMANDS_POLICY_NARROW,
	CLGC_PLAYBACK_COMMANDS_POLICY_NORMAL,
	CLGC_PLAYBACK_COMMANDS_POLICY_WIDE
} ClgcPlaybackCommandsPolicy;

/**
 * @brief Creates an instance of player commands
 * @return An instance of player commands
 */
GtkWidget* clgc_playback_commands_new();

ClgcPlaybackCommandsPolicy clgc_playback_commands_get_policy(ClgcPlaybackCommands* self);

void clgc_playback_commands_set_state(ClgcPlaybackCommands* self, ClgcPlaybackState state);

void clgc_playback_commands_set_policy(ClgcPlaybackCommands* self, ClgcPlaybackCommandsPolicy policy);

G_END_DECLS

#endif // __CALGICI_PLAYBACK_COMMANDS__
