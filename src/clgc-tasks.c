/*
 * clgc-tasks: Calgici Tasks class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-tasks.h"

#include "clgc-mpd-client.h"

typedef gpointer (*ClgcGetDataFunc)(ClgcMPDClient*);
typedef gpointer (*ClgcGetDataWithArgFunc)(ClgcMPDClient*, gpointer);
typedef int (*ClgcGetEnumFunc)(ClgcMPDClient*);
typedef void (*ClgcSendCommandFunc)(ClgcMPDClient*);
typedef void (*ClgcSendCommandWithArgFunc)(ClgcMPDClient*, gpointer);
typedef void (*ClgcSendCommandWithEnumFunc)(ClgcMPDClient*, int);
typedef void (*ClgcSendCommandWith2EnumsFunc)(ClgcMPDClient*, int, int);

void clgc_tasks_connect_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	GSettings* settings = G_SETTINGS(object);
	ClgcMPDClient* client = NULL;

	char* hostname = g_settings_get_string(settings, "hostname");
	unsigned port = 0;
	unsigned timeout = g_settings_get_uint(settings, "timeout");

	g_settings_get(settings, "port", "q", &port);

	while (!client)
	{
		client = clgc_mpd_client_connect(hostname, port, timeout);
		g_usleep(1000);
	}

	g_task_return_pointer(task, client, g_object_unref);
}

void clgc_tasks_get_data_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcGetDataFunc func = (ClgcGetDataFunc) clgc_task_data_get_func(task_data);

	gpointer data = NULL;
	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	data = func(client);
	g_mutex_unlock(mutex);

	g_task_return_pointer(task, data, g_object_unref);
}

void clgc_tasks_get_data_with_arg_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcGetDataWithArgFunc func = (ClgcGetDataWithArgFunc) clgc_task_data_get_func(task_data);
	gpointer arg = clgc_task_data_get_arg(task_data, 0);

	gpointer data = NULL;
	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	data = func(client, arg);
	g_mutex_unlock(mutex);

	g_task_return_pointer(task, data, g_object_unref);
}

void clgc_tasks_get_enum_from_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcGetEnumFunc func = (ClgcGetEnumFunc) clgc_task_data_get_func(task_data);

	int data = 0;
	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	data = func(client);
	g_mutex_unlock(mutex);

	g_task_return_int(task, data);
}

void clgc_tasks_send_command_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcSendCommandFunc func = (ClgcSendCommandFunc) clgc_task_data_get_func(task_data);

	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	func(client);
	clgc_mpd_client_send_request(client);
	g_mutex_unlock(mutex);
}

void clgc_tasks_send_command_with_arg_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcSendCommandWithArgFunc func = (ClgcSendCommandWithArgFunc) clgc_task_data_get_func(task_data);
	gpointer arg = clgc_task_data_get_arg(task_data, 0);

	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	func(client, arg);
	clgc_mpd_client_send_request(client);
	g_mutex_unlock(mutex);
}

void clgc_tasks_send_command_with_enum_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcSendCommandWithEnumFunc func = (ClgcSendCommandWithEnumFunc) clgc_task_data_get_func(task_data);
	int arg = GPOINTER_TO_INT(clgc_task_data_get_arg(task_data, 0));

	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	func(client, arg);
	clgc_mpd_client_send_request(client);
	g_mutex_unlock(mutex);
}

void clgc_tasks_send_command_with_2_enums_to_server(GTask* task, gpointer object, gpointer user_data, GCancellable* cancellable)
{
	ClgcMPDClient* client = CLGC_MPD_CLIENT(object);
	ClgcTaskData* task_data = CLGC_TASK_DATA(user_data);

	ClgcSendCommandWith2EnumsFunc func = (ClgcSendCommandWith2EnumsFunc) clgc_task_data_get_func(task_data);
	int arg1 = GPOINTER_TO_INT(clgc_task_data_get_arg(task_data, 0));
	int arg2 = GPOINTER_TO_INT(clgc_task_data_get_arg(task_data, 1));

	GMutex* mutex = clgc_mpd_client_get_mutex(client);

	g_mutex_lock(mutex);
	func(client, arg1, arg2);
	clgc_mpd_client_send_request(client);
	g_mutex_unlock(mutex);
}
