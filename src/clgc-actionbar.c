/*
 * clgc-actionbar: Calgici ActionBar class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-actionbar.h"

#include <libintl.h>

enum _ClgcActionBarProperty
{
	PROP_FLAGS = 1,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcActionBar
{
	GtkWidget m_parent;

	unsigned m_flags;

	GtkWidget* m_bar;
	GtkWidget* m_box;
	GtkWidget* m_add_button;
	GtkWidget* m_add_other_button;
	GtkWidget* m_remove_button;
};

G_DEFINE_TYPE(ClgcActionBar, clgc_actionbar, GTK_TYPE_WIDGET)

static void clgc_actionbar_constructed(GObject* obj)
{
	ClgcActionBar* self = CLGC_ACTIONBAR(obj);

	if (self->m_flags)
	{
		self->m_remove_button = gtk_button_new_with_label(gettext("Remove"));

		gtk_action_bar_pack_end(GTK_ACTION_BAR(self->m_bar), self->m_remove_button);

		gtk_actionable_set_action_name(GTK_ACTIONABLE(self->m_remove_button), "win.send-remove");
		gtk_widget_add_css_class(self->m_remove_button, "destructive-action");

		gtk_widget_set_visible(self->m_remove_button, TRUE);
	}
}

static void clgc_actionbar_dispose(GObject* obj)
{
	ClgcActionBar* self = CLGC_ACTIONBAR(obj);

	g_clear_pointer(&(self->m_bar), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_actionbar_parent_class)->dispose(obj);
}

static void clgc_actionbar_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcActionBar* self = CLGC_ACTIONBAR(obj);

	switch (property_id)
	{
		case PROP_FLAGS:
			g_value_set_uint(value, self->m_flags);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_actionbar_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcActionBar* self = CLGC_ACTIONBAR(obj);

	switch (property_id)
	{
		case PROP_FLAGS:
			self->m_flags = g_value_get_uint(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_actionbar_class_init(ClgcActionBarClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->constructed = clgc_actionbar_constructed;
	obj_class->dispose = clgc_actionbar_dispose;
	obj_class->get_property = clgc_actionbar_get_property;
	obj_class->set_property = clgc_actionbar_set_property;

	obj_properties[PROP_FLAGS] = g_param_spec_uint("flags", "Flags", "Flags to indicate the element to add", 0, 1, 1, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BIN_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-actionbar.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcActionBar, m_bar);
	gtk_widget_class_bind_template_child(widget_class, ClgcActionBar, m_box);
	gtk_widget_class_bind_template_child(widget_class, ClgcActionBar, m_add_button);
	gtk_widget_class_bind_template_child(widget_class, ClgcActionBar, m_add_other_button);
}

static void clgc_actionbar_init(ClgcActionBar* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_actionbar_new(int remove)
{
	return g_object_new(CLGC_TYPE_ACTIONBAR, "flags", remove, NULL);
}

void clgc_actionbar_enable_buttons(ClgcActionBar* self)
{
	gtk_widget_set_sensitive(GTK_WIDGET(self->m_box), TRUE);

	if (self->m_remove_button)
		gtk_widget_set_sensitive(GTK_WIDGET(self->m_remove_button), TRUE);
}

void clgc_actionbar_disable_buttons(ClgcActionBar* self)
{
	gtk_widget_set_sensitive(GTK_WIDGET(self->m_box), FALSE);

	if (self->m_remove_button)
		gtk_widget_set_sensitive(GTK_WIDGET(self->m_remove_button), FALSE);
}
