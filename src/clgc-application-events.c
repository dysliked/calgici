/*
 * clgc-application-events: events Calgici Application class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-application.h"
#include "clgc-application-private.h"

#include "clgc-application-callbacks.h"
#include "clgc-tasks.h"
#include "clgc-window.h"

void clgc_application_show_contents(ClgcApplication* self, ClgcTag* tag)
{
	GTask* task = g_task_new(self->m_client, NULL, clgc_application_callbacks_show_contents, self);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_get_data_from_tag);

	clgc_task_data_add_arg(task_data, tag);
	g_task_set_task_data(task, task_data, NULL);

	g_task_run_in_thread(task, clgc_tasks_get_data_with_arg_from_server);
}

static void clgc_application_update_database(ClgcApplication* self)
{
	int id;
	GTask* task = NULL;
	ClgcTaskData* task_data = NULL;

	for (id = 0; id < clgc_tag_list_length(self->m_tag_list); id ++)
	{
		task = g_task_new(self->m_client, NULL, clgc_application_callbacks_update_database, self);
		task_data = clgc_task_data_new(clgc_mpd_client_get_data_from_tag);

		clgc_task_data_add_arg(task_data, clgc_tag_list_nth(self->m_tag_list, id));
		g_task_set_task_data(task, task_data, g_object_unref);

		g_task_run_in_thread(task, clgc_tasks_get_data_with_arg_from_server);
	}
}

static void clgc_application_update_audio_outputs(ClgcApplication* self)
{
	GTask* task = g_task_new(self->m_client, NULL, clgc_application_callbacks_update_audio_outputs, self);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_get_audio_outputs);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_get_data_from_server);
}

void clgc_application_update_playback(ClgcApplication* self)
{
	GTask* task = g_task_new(self->m_client, NULL, clgc_application_callbacks_update_playback, self);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_get_playback);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_get_data_from_server);
}

static void clgc_application_update_playback_status(ClgcApplication* self)
{
	GTask* task = g_task_new(self->m_client, NULL, clgc_application_callbacks_update_playback_status, self);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_get_playback_status);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_get_enum_from_server);
}

static void clgc_application_update_queue(ClgcApplication* self)
{
	GTask* task = g_task_new(self->m_client, NULL, clgc_application_callbacks_update_queue, self);
	ClgcTaskData* task_data = clgc_task_data_new(clgc_mpd_client_get_queue);

	g_task_set_task_data(task, task_data, g_object_unref);
	g_task_run_in_thread(task, clgc_tasks_get_data_from_server);
}

void clgc_application_manage_event(ClgcApplication* self, ClgcEvent event)
{
	g_info("Receiveid an event from MPD server: %d\n", event);

	if (event & CLGC_EVENT_DATABASE)
		clgc_application_update_database(self);

	if (event & CLGC_EVENT_QUEUE)
		clgc_application_update_queue(self);

	if (event & CLGC_EVENT_PLAYER)
		clgc_application_update_playback(self);

	if (event & CLGC_EVENT_OUTPUT)
		clgc_application_update_audio_outputs(self);

	if (event & CLGC_EVENT_OPTIONS)
		clgc_application_update_playback_status(self);

	clgc_mpd_client_set_idle(self->m_client_listen, TRUE);
}
