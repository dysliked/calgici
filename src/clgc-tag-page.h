/*
 * clgc-tag-page: Calgici TagPage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TAG_PAGE__
#define __CALGICI_TAG_PAGE__

#include "clgc-page.h"
#include "clgc-tag.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_TAG_PAGE (clgc_tag_page_get_type())

G_DECLARE_FINAL_TYPE(ClgcTagPage, clgc_tag_page, CLGC, TAG_PAGE, GtkWidget)

/**
 * @brief Creates an instance of tag page
 * @param tag The tag for the page
 * @return An instance of tag page
 */
GtkWidget* clgc_tag_page_new(ClgcTag* tag);

ClgcTag* clgc_tag_page_get_tag(ClgcTagPage* self);

/**
 * @brief Updates the page
 * @param self The page to be update
 */
void clgc_tag_page_update(ClgcTagPage* self, GList* list);

G_END_DECLS

#endif // __CALGICI_TAG_PAGE__
