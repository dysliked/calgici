/*
 * clgc-audio-output: Calgici AudioOuptut class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-audio-output.h"

enum _ClgcAudioOutputProperty
{
	PROP_0,
	PROP_ID,
	PROP_NAME,
	PROP_ENABLED,
	N_PROPERTIES
};

struct _ClgcAudioOutput
{
	GObject m_parent;

	int m_id;
	char* m_name;
	char m_action_name[32];
	gboolean m_enabled;
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcAudioOutput, clgc_audio_output, G_TYPE_OBJECT)

static void clgc_audio_output_constructed(GObject* obj)
{
	ClgcAudioOutput* self = CLGC_AUDIO_OUTPUT(obj);

	snprintf(self->m_action_name, 32, "mpd-client.audio-output.%02d", self->m_id);
}

static void clgc_audio_output_finalize(GObject* obj)
{
	ClgcAudioOutput* self = CLGC_AUDIO_OUTPUT(obj);

	g_free(self->m_name);

	G_OBJECT_CLASS(clgc_audio_output_parent_class)->finalize(obj);
}

static void clgc_audio_output_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcAudioOutput* self = CLGC_AUDIO_OUTPUT(obj);

	switch (property_id)
	{
		case PROP_ID:
			g_value_set_int(value, clgc_audio_output_get_id(self));
			break;
		case PROP_NAME:
			g_value_set_string(value, clgc_audio_output_get_name(self));
			break;
		case PROP_ENABLED:
			g_value_set_boolean(value, self->m_enabled);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_audio_output_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcAudioOutput* self = CLGC_AUDIO_OUTPUT(obj);

	switch (property_id)
	{
		case PROP_ID:
			self->m_id = g_value_get_int(value);
			break;
		case PROP_NAME:
			self->m_name = g_strdup(g_value_get_string(value));
			break;
		case PROP_ENABLED:
			self->m_enabled = g_value_get_boolean(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_audio_output_class_init(ClgcAudioOutputClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);

	obj_class->constructed = clgc_audio_output_constructed;
	obj_class->finalize = clgc_audio_output_finalize;
	obj_class->get_property = clgc_audio_output_get_property;
	obj_class->set_property = clgc_audio_output_set_property;

	obj_properties[PROP_ID] = g_param_spec_int("id", "ID", "The identifiant of the audio output", G_MININT, G_MAXINT, -1, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_NAME] = g_param_spec_string("name", "Name", "The name of the audio output", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	obj_properties[PROP_ENABLED] = g_param_spec_boolean("enabled", "Enabled", "If the audio output is enabled", FALSE, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
}

static void clgc_audio_output_init(ClgcAudioOutput* self)
{
}

ClgcAudioOutput* clgc_audio_output_new(int id, const char* name, gboolean enabled)
{
	return g_object_new(CLGC_TYPE_AUDIO_OUTPUT, "id", id, "name", name, "enabled", enabled, NULL);
}

int clgc_audio_output_action_name_get_id(const char* action_name)
{
	int id;
	char** array = g_strsplit(action_name, ".", -1);

	for (id = 0; array[id] != NULL; id ++)
		;

	id = g_ascii_strtoll(array[id - 1], NULL, 10);
	g_strfreev(array);
	return id;
}

int clgc_audio_output_get_id(ClgcAudioOutput* self)
{
	return self->m_id;
}

const char* clgc_audio_output_get_name(ClgcAudioOutput* self)
{
	return self->m_name;
}

const char* clgc_audio_output_get_action_name(ClgcAudioOutput* self)
{
	return self->m_action_name;
}

gboolean clgc_audio_output_is_enabled(ClgcAudioOutput* self)
{
	return self->m_enabled;
}
