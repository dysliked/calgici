/*
 * clgc-task-data: Calgici TaskData class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-task-data.h"

enum _ClgcTaskDataProperty
{
	PROP_FUNCTION = 1,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcTaskData
{
	GObject m_parent;

	gpointer m_func;
	GList* m_list;
};

G_DEFINE_TYPE(ClgcTaskData, clgc_task_data, G_TYPE_OBJECT)

static void clgc_task_data_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcTaskData* self = CLGC_TASK_DATA(obj);

	switch (property_id)
	{
		case PROP_FUNCTION:
			g_value_set_pointer(value, self->m_func);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_task_data_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcTaskData* self = CLGC_TASK_DATA(obj);

	switch (property_id)
	{
		case PROP_FUNCTION:
			self->m_func = g_value_get_pointer(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_task_data_class_init(ClgcTaskDataClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);

	obj_class->get_property = clgc_task_data_get_property;
	obj_class->set_property = clgc_task_data_set_property;

	obj_properties[PROP_FUNCTION] = g_param_spec_pointer(
		"function",
		"Function",
		"Function to call in the task",
		G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);
}

static void clgc_task_data_init(ClgcTaskData* self)
{
	self->m_func = NULL;
	self->m_list = NULL;
}

ClgcTaskData* clgc_task_data_new(gpointer func)
{
	return g_object_new(CLGC_TYPE_TASK_DATA, "function", func, NULL);
}

void clgc_task_data_add_arg(ClgcTaskData* self, gpointer arg)
{
	self->m_list = g_list_append(self->m_list, arg);
}

gpointer clgc_task_data_get_arg(ClgcTaskData* self, int id)
{
	return g_list_nth_data(self->m_list, id);
}

gpointer clgc_task_data_get_func(ClgcTaskData* self)
{
	return self->m_func;
}
