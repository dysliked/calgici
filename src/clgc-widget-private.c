/*
 * clgc-widget-private: Calgici WidgetPrivate class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-widget-private.h"

void clgc_widget_update_orientation(GtkWidget* widget, GtkOrientation orientation)
{
	g_return_if_fail (GTK_IS_WIDGET (widget));

	if (orientation == GTK_ORIENTATION_HORIZONTAL)
	{
		gtk_widget_add_css_class (widget, "horizontal");
		gtk_widget_remove_css_class (widget, "vertical");
	}
	else
	{
		gtk_widget_add_css_class (widget, "vertical");
		gtk_widget_remove_css_class (widget, "horizontal");
	}

	gtk_accessible_update_property(GTK_ACCESSIBLE(widget), GTK_ACCESSIBLE_PROPERTY_ORIENTATION, orientation, -1);
}
