/*
 * clgc-queue-page: Calgici QueuePage class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_QUEUE_PAGE__
#define __CALGICI_QUEUE_PAGE__

#include "clgc-page.h"
#include "clgc-playback.h"

#include <gtk/gtk.h>

#include <adwaita.h>

G_BEGIN_DECLS

#define CLGC_TYPE_QUEUE_PAGE (clgc_queue_page_get_type())

G_DECLARE_FINAL_TYPE(ClgcQueuePage, clgc_queue_page, CLGC, QUEUE_PAGE, GtkWidget)

/**
 * @brief Create an instance of the queue page
 * @return An instance of the queue page
 */
GtkWidget* clgc_queue_page_new();

void clgc_queue_page_set_playback(ClgcQueuePage* self, ClgcPlayback* playback);

void clgc_queue_page_update(ClgcQueuePage* self, GList* list);

G_END_DECLS

#endif // __CALGICI_QUEUE_PAGE__
