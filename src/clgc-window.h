/*
 * clgc-window: main Calgici Window class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_WINDOW__
#define __CALGICI_WINDOW__

#include "clgc-mode.h"
#include "clgc-playback.h"
#include "clgc-playback-status.h"
#include "clgc-playerbar.h"
#include "clgc-tag.h"
#include "clgc-tag-list.h"

#include <gtk/gtk.h>

#include <adwaita.h>

G_BEGIN_DECLS

#define CLGC_TYPE_WINDOW (clgc_window_get_type())

G_DECLARE_FINAL_TYPE(ClgcWindow, clgc_window, CLGC, WINDOW, AdwApplicationWindow)

/**
 * @brief Create an instance of window
 * @return An instance of window
 */
GtkWidget* clgc_window_new(ClgcTagList* pages);

/**
 * @brief Adds the selection in the window
 * @param self The window
 */
void clgc_window_selection_add(ClgcWindow* self);

/**
 * @brief Remove the selection in the window
 * @param self The window
 */
void clgc_window_selection_remove(ClgcWindow* self);

void clgc_window_set_audio_outputs(ClgcWindow* self, GList* list);

/**
 * @brief Sets the mode for the window
 * @param self The window
 * @param mode The mode to be set
 */
void clgc_window_set_mode(ClgcWindow* self, ClgcMode mode);

void clgc_window_set_playback(ClgcWindow* self, ClgcPlayback* playback);

void clgc_window_set_playback_status(ClgcWindow* self, ClgcPlaybackStatus status);

void clgc_window_show_contents(ClgcWindow* self, ClgcTag* tag, GList* list);

void clgc_window_update_queue(ClgcWindow* self, GList* list);

void clgc_window_update_database(ClgcWindow* self, ClgcTag* tag, GList* list);

G_END_DECLS

#endif // __CALGICI_WINDOW__
