/*
 * clgc-window: main Calgici Window class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-window.h"

#include "clgc-window-private.h"

#include "clgc-application.h"
#include "clgc-panel.h"
#include "clgc-library-panel.h"
#include "clgc-medium-panel.h"

#include <libintl.h>

enum _ClgcWindowProperty
{
	PROP_PAGES = 1,
	N_PROPERTIES
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

G_DEFINE_TYPE(ClgcWindow, clgc_window, ADW_TYPE_APPLICATION_WINDOW)

static void clgc_window_constructed(GObject* obj)
{
	ClgcWindow* self = CLGC_WINDOW(obj);

	G_OBJECT_CLASS(clgc_window_parent_class)->constructed(obj);
	clgc_window_actions_update(self);
}

static void clgc_window_get_property(GObject* obj, guint property_id, GValue* value, GParamSpec* pspec)
{
	ClgcWindow* self = CLGC_WINDOW(obj);

	switch (property_id)
	{
		case PROP_PAGES:
			g_value_set_boxed(value, clgc_library_panel_get_pages(CLGC_LIBRARY_PANEL(self->m_library_panel)));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_window_set_property(GObject* obj, guint property_id, const GValue* value, GParamSpec* pspec)
{
	ClgcWindow* self = CLGC_WINDOW(obj);

	switch (property_id)
	{
		case PROP_PAGES:
			clgc_library_panel_set_pages(CLGC_LIBRARY_PANEL(self->m_library_panel), g_value_get_boxed(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
			break;
	}
}

static void clgc_window_class_init(ClgcWindowClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->constructed = clgc_window_constructed;
	obj_class->get_property = clgc_window_get_property;
	obj_class->set_property = clgc_window_set_property;

	obj_properties[PROP_PAGES] = g_param_spec_boxed(
		"pages",
		"Pages",
		"Pages to show in the library panel",
		CLGC_TYPE_TAG_LIST,
		G_PARAM_CONSTRUCT | G_PARAM_READWRITE
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-window.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcWindow, m_view);
	gtk_widget_class_bind_template_child(widget_class, ClgcWindow, m_library_panel);
	gtk_widget_class_bind_template_child(widget_class, ClgcWindow, m_tag_panel);

	g_type_ensure(CLGC_TYPE_LIBRARY_PANEL);
	g_type_ensure(CLGC_TYPE_MEDIUM_PANEL);

	gtk_widget_class_add_binding_action(widget_class, GDK_KEY_Escape, 0, "win.normal-mode", NULL);
	gtk_widget_class_add_binding_action(widget_class, GDK_KEY_question, GDK_CONTROL_MASK, "win.show-help-overlay", NULL);
	gtk_widget_class_add_binding_action(widget_class, GDK_KEY_comma, GDK_CONTROL_MASK, "win.show-preferences", NULL);

	clgc_window_class_actions_init(klass);
}

static void clgc_window_init(ClgcWindow* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_window_new(ClgcTagList* pages)
{
	return g_object_new(CLGC_TYPE_WINDOW, "application", CLGC_APPLICATION_DEFAULT, "title", gettext(APP_TITLE), "pages", pages, NULL);
}

void clgc_window_selection_remove(ClgcWindow* self)
{
}

void clgc_window_set_audio_outputs(ClgcWindow* self, GList* list)
{
	clgc_library_panel_set_audio_outputs(CLGC_LIBRARY_PANEL(self->m_library_panel), list);
}

void clgc_window_set_mode(ClgcWindow* self, ClgcMode mode)
{
	AdwNavigationPage* page = adw_navigation_view_get_visible_page(ADW_NAVIGATION_VIEW(self->m_view));
	GtkWidget* panel = adw_navigation_page_get_child(page);

	clgc_panel_set_mode(CLGC_PANEL(panel), mode);
}

void clgc_window_set_playback(ClgcWindow* self, ClgcPlayback* playback)
{
	clgc_library_panel_set_playback(CLGC_LIBRARY_PANEL(self->m_library_panel), playback);
}

void clgc_window_set_playback_status(ClgcWindow* self, ClgcPlaybackStatus status)
{
	clgc_library_panel_set_playback_status(CLGC_LIBRARY_PANEL(self->m_library_panel), status);
}

void clgc_window_show_contents(ClgcWindow* self, ClgcTag* tag, GList* list)
{
	clgc_medium_panel_update(CLGC_MEDIUM_PANEL(self->m_tag_panel), tag, list);
	adw_navigation_view_push_by_tag(ADW_NAVIGATION_VIEW(self->m_view), "medium");
}

void clgc_window_update_queue(ClgcWindow* self, GList* list)
{
	clgc_library_panel_update_queue(CLGC_LIBRARY_PANEL(self->m_library_panel), list);
}

void clgc_window_update_database(ClgcWindow* self, ClgcTag* tag, GList* list)
{
	clgc_library_panel_update_page(CLGC_LIBRARY_PANEL(self->m_library_panel), tag, list);
}
