/*
 * clgc-tag-list: Calgici TagList class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-tag-list.h"

struct _ClgcTagList
{
	GPtrArray* m_array;
};

G_DEFINE_BOXED_TYPE(ClgcTagList, clgc_tag_list, clgc_tag_list_copy, clgc_tag_list_free)

static void clgc_tag_list_init(ClgcTagList* self)
{
	self->m_array = NULL;
}

ClgcTagList* clgc_tag_list_new()
{
	ClgcTagList* list = g_slice_new(ClgcTagList);

	clgc_tag_list_init(list);

	return list;
}

void clgc_tag_list_add(ClgcTagList* self, ClgcTag* tag)
{
	if (!self->m_array)
		self->m_array = g_ptr_array_new();

	g_ptr_array_add(self->m_array, tag);
}

ClgcTagList* clgc_tag_list_copy(ClgcTagList* self)
{
	ClgcTagList* copy;

	if (self == NULL)
		return NULL;

	copy = clgc_tag_list_new();
	copy->m_array = g_ptr_array_copy(self->m_array, (GCopyFunc) clgc_tag_copy, NULL);

	return copy;
}

void clgc_tag_list_free(ClgcTagList* self)
{
	int id;

	for (id = 0; id < self->m_array->len; id ++)
	{
		ClgcTag* tag = g_ptr_array_index(self->m_array, id);
		clgc_tag_free(tag);
	}

	g_ptr_array_free(self->m_array, TRUE);
	g_slice_free(ClgcTagList, self);
}

int clgc_tag_list_length(ClgcTagList* self)
{
	return self->m_array->len;
}

ClgcTag* clgc_tag_list_nth(ClgcTagList* self, int nth)
{
	if (nth < 0 || nth >= clgc_tag_list_length(self))
		return NULL;

	return g_ptr_array_index(self->m_array, nth);
}
