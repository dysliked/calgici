/*
 * clgc-library-panel: Calgici LibraryPanel class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clgc-library-panel.h"

#include "clgc-actionbar.h"
#include "clgc-headerbar.h"
#include "clgc-panel.h"
#include "clgc-playerbar.h"
#include "clgc-queue-page.h"
#include "clgc-selectionbar.h"
#include "clgc-tag-page.h"
#include "clgc-tag-list.h"
#include "clgc-widget-private.h"

#include <libintl.h>

enum _ClgcLibraryPanelProperty
{
	PROP_0,
	PROP_PAGES,

	/* Overriden properties */
	PROP_ORIENTATION,

	N_PROPERTIES = PROP_ORIENTATION
};

static GParamSpec* obj_properties[N_PROPERTIES] = {NULL, };

struct _ClgcLibraryPanel
{
	GtkWidget m_parent;

	GtkWidget* m_stack_headerbar;
	GtkWidget* m_headerbar;
	GtkWidget* m_selectionbar;

	GtkWidget* m_overlay;
	GtkWidget* m_stack;
	GtkWidget* m_queue_page;
	GtkWidget* m_playerbar;

	GtkWidget* m_stack_footerbar;
	GtkWidget* m_actionbar;
	GtkWidget* m_switcher_footerbar;
};

static void clgc_library_panel_interface_init(ClgcPanelInterface* iface);

G_DEFINE_TYPE_WITH_CODE(ClgcLibraryPanel, clgc_library_panel, GTK_TYPE_WIDGET,
	G_IMPLEMENT_INTERFACE(GTK_TYPE_ORIENTABLE, NULL)
	G_IMPLEMENT_INTERFACE(CLGC_TYPE_PANEL, clgc_library_panel_interface_init)
)

static void clgc_library_panel_set_mode(ClgcPanel* panel, ClgcMode mode)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);
	GtkWidget* page = adw_view_stack_get_visible_child(ADW_VIEW_STACK(self->m_stack));

	clgc_page_set_mode(CLGC_PAGE(page), mode);

	switch (mode)
	{
		case CLGC_MODE_NORMAL:
			gtk_stack_set_visible_child(GTK_STACK(self->m_stack_headerbar), self->m_headerbar);
			gtk_stack_set_visible_child(GTK_STACK(self->m_stack_footerbar), self->m_switcher_footerbar);
			gtk_widget_set_visible(self->m_actionbar, FALSE);
			clgc_playerbar_set_reveal(CLGC_PLAYERBAR(self->m_playerbar), TRUE);
			gtk_widget_set_margin_bottom(self->m_stack, 47);
			break;
		case CLGC_MODE_SELECTION:
			gtk_widget_set_visible(self->m_actionbar, TRUE);
			gtk_stack_set_visible_child(GTK_STACK(self->m_stack_headerbar), self->m_selectionbar);
			gtk_stack_set_visible_child(GTK_STACK(self->m_stack_footerbar), self->m_actionbar);
			clgc_playerbar_set_reveal(CLGC_PLAYERBAR(self->m_playerbar), FALSE);
			gtk_widget_set_margin_bottom(self->m_stack, 0);
			break;
		default:
			break;
	}
}

static void clgc_library_panel_selection_all(ClgcPanel* panel)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);
	GtkWidget* page = adw_view_stack_get_visible_child(ADW_VIEW_STACK(self->m_stack));

	clgc_page_selection_all(CLGC_PAGE(page));
}

static void clgc_library_panel_selection_change(ClgcPanel* panel, int selected)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);

	if (selected)
		clgc_actionbar_enable_buttons(CLGC_ACTIONBAR(self->m_actionbar));
	else
		clgc_actionbar_disable_buttons(CLGC_ACTIONBAR(self->m_actionbar));

	clgc_selectionbar_selection_change(CLGC_SELECTIONBAR(self->m_selectionbar), selected);
}

static void clgc_library_panel_selection_none(ClgcPanel* panel)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);
	GtkWidget* page = adw_view_stack_get_visible_child(ADW_VIEW_STACK(self->m_stack));

	clgc_page_selection_none(CLGC_PAGE(page));
}

static void clgc_library_panel_send_add(ClgcPanel* panel)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);
	GtkWidget* page = adw_view_stack_get_visible_child(ADW_VIEW_STACK(self->m_stack));

	clgc_page_send_add(CLGC_PAGE(page));
}

static void clgc_library_panel_send_remove(ClgcPanel* panel)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(panel);
	GtkWidget* page = adw_view_stack_get_visible_child(ADW_VIEW_STACK(self->m_stack));

	clgc_page_send_remove(CLGC_PAGE(page));
}

static void clgc_library_panel_dispose(GObject* obj)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(obj);

	g_clear_pointer(&(self->m_stack_headerbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_headerbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_selectionbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_overlay), gtk_widget_unparent);
	g_clear_pointer(&(self->m_stack), gtk_widget_unparent);
	g_clear_pointer(&(self->m_playerbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_stack_footerbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_actionbar), gtk_widget_unparent);
	g_clear_pointer(&(self->m_switcher_footerbar), gtk_widget_unparent);

	G_OBJECT_CLASS(clgc_library_panel_parent_class)->dispose(obj);
}

static void clgc_library_panel_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(object);
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_PAGES:
			g_value_set_boxed(value, clgc_library_panel_get_pages(self));
			break;
		case PROP_ORIENTATION:
			g_value_set_enum(value, gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_library_panel_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	GtkOrientation orientation;
	ClgcLibraryPanel* self = CLGC_LIBRARY_PANEL(object);
	GtkLayoutManager* layout = gtk_widget_get_layout_manager(GTK_WIDGET(object));

	switch (prop_id)
	{
		case PROP_PAGES:
			clgc_library_panel_set_pages(self, g_value_get_boxed(value));
			break;
		case PROP_ORIENTATION:
			{
				orientation = g_value_get_enum (value);

				if (gtk_orientable_get_orientation(GTK_ORIENTABLE(layout)) != orientation)
				{
					gtk_orientable_set_orientation(GTK_ORIENTABLE(layout), orientation);
					clgc_widget_update_orientation(GTK_WIDGET(self), orientation);
					g_object_notify_by_pspec(object, pspec);
				}
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void clgc_library_panel_interface_init(ClgcPanelInterface* iface)
{
	iface->set_mode = clgc_library_panel_set_mode;

	iface->selection_all = clgc_library_panel_selection_all;
	iface->selection_change = clgc_library_panel_selection_change;
	iface->selection_none = clgc_library_panel_selection_none;

	iface->send_add = clgc_library_panel_send_add;
	iface->send_remove = clgc_library_panel_send_remove;
}

static void clgc_library_panel_class_init(ClgcLibraryPanelClass* klass)
{
	GObjectClass* obj_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(klass);

	obj_class->dispose = clgc_library_panel_dispose;
	obj_class->get_property = clgc_library_panel_get_property;
	obj_class->set_property = clgc_library_panel_set_property;

	obj_properties[PROP_PAGES] = g_param_spec_boxed(
		"pages",
		"Pages",
		"Pages to add in the stack",
		CLGC_TYPE_TAG_LIST,
		G_PARAM_READWRITE
	);

	g_object_class_override_property(obj_class, PROP_ORIENTATION, "orientation");

	g_object_class_install_properties(obj_class, N_PROPERTIES, obj_properties);

	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_BOX_LAYOUT);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/dysliked/calgici/ui/clgc-library-panel.ui");
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_stack_headerbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_headerbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_selectionbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_overlay);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_stack);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_queue_page);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_playerbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_stack_footerbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_actionbar);
	gtk_widget_class_bind_template_child(widget_class, ClgcLibraryPanel, m_switcher_footerbar);

	g_type_ensure(CLGC_TYPE_HEADERBAR);
	g_type_ensure(CLGC_TYPE_SELECTIONBAR);
	g_type_ensure(CLGC_TYPE_QUEUE_PAGE);
	g_type_ensure(CLGC_TYPE_PLAYERBAR);
	g_type_ensure(CLGC_TYPE_ACTIONBAR);
}

static void clgc_library_panel_init(ClgcLibraryPanel* self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}

GtkWidget* clgc_library_panel_new()
{
	return g_object_new(CLGC_TYPE_LIBRARY_PANEL, NULL);
}

ClgcTagList* clgc_library_panel_get_pages(ClgcLibraryPanel* self)
{
	return NULL;
}

void clgc_library_panel_set_audio_outputs(ClgcLibraryPanel* self, GList* list)
{
	clgc_headerbar_set_audio_outputs(CLGC_HEADERBAR(self->m_headerbar), list);
}

void clgc_library_panel_set_pages(ClgcLibraryPanel* self, ClgcTagList* list)
{
	int id = 0;
	ClgcTag* tag = NULL;
	ClgcTagType type;
	AdwViewStackPage* page = NULL;
	GtkWidget* tag_page = NULL;

	for (id = 0; id < clgc_tag_list_length(list); id ++)
	{
		tag = clgc_tag_list_nth(list, id);
		type = clgc_tag_get_key(tag);

		tag_page = clgc_tag_page_new(tag);

		page = adw_view_stack_add_titled(ADW_VIEW_STACK(self->m_stack), tag_page, clgc_tag_type_get_id(type), gettext(clgc_tag_type_get_name(type)));
		adw_view_stack_page_set_icon_name(page, clgc_tag_type_get_icon_name(type));
	}
}

void clgc_library_panel_set_playback(ClgcLibraryPanel* self, ClgcPlayback* playback)
{
	clgc_queue_page_set_playback(CLGC_QUEUE_PAGE(self->m_queue_page), playback);
	clgc_playerbar_set_playback(CLGC_PLAYERBAR(self->m_playerbar), playback);
}

void clgc_library_panel_set_playback_status(ClgcLibraryPanel* self, ClgcPlaybackStatus status)
{
	clgc_playerbar_set_playback_status(CLGC_PLAYERBAR(self->m_playerbar), status);
}

void clgc_library_panel_update_queue(ClgcLibraryPanel* self, GList* list)
{
	clgc_queue_page_update(CLGC_QUEUE_PAGE(self->m_queue_page), list);
}

void clgc_library_panel_update_page(ClgcLibraryPanel* self, ClgcTag* tag, GList* list)
{
	GtkWidget* page = adw_view_stack_get_child_by_name(ADW_VIEW_STACK(self->m_stack), clgc_tag_get_id(tag));

	clgc_tag_page_update(CLGC_TAG_PAGE(page), list);
}
