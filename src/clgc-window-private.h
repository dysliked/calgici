/*
 * clgc-window-private: private Calgici Window class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_WINDOW_PRIVATE__
#define __CALGICI_WINDOW_PRIVATE__

#include "clgc-tag-list.h"
#include "clgc-window.h"

struct _ClgcWindow
{
	AdwApplicationWindow parent;

	GtkWidget* m_view;
	GtkWidget* m_library_panel;
	GtkWidget* m_tag_panel;
};

void clgc_window_class_actions_init(ClgcWindowClass* klass);

void clgc_window_actions_update(ClgcWindow* self);

void clgc_window_class_requests_init(ClgcWindowClass* klass);

#endif // __CALGICI_WINDOW_PRIVATE__
