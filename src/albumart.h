/*
 * albumart: MPD albumart class to get back the albumart
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MPD_ALBUMART_H
#define MPD_ALBUMART_H

#include <mpd/client.h>
#include <stdio.h>

struct mpd_albumart;

struct mpd_albumart* mpd_albumart_begin(const struct mpd_pair* pair);

const void* mpd_albumart_get_data(struct mpd_albumart* albumart);

size_t mpd_albumart_get_size(struct mpd_albumart* albumart);

size_t mpd_albumart_get_size_max(struct mpd_albumart* albumart);

void mpd_albumart_free(struct mpd_albumart* albumart);

struct mpd_albumart* mpd_recv_get_albumart(struct mpd_connection* connection);

struct mpd_albumart* mpd_run_get_albumart(struct mpd_connection* connection, const char* uri, size_t offset);

bool mpd_send_get_albumart(struct mpd_connection* connection, const char* uri, size_t offset);

#endif // MPD_ALBUMART_H
