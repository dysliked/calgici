/*
 * clgc-tag: Calgici Tag class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CALGICI_TAG__
#define __CALGICI_TAG__

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum _ClgcTagType
{
	CLGC_TAG_TITLE,
	CLGC_TAG_ALBUM,
	CLGC_TAG_ARTIST,
	CLGC_TAG_NUMBER,
	CLGC_TAG_TIME,
	CLGC_TAG_PLAYING,
	CLGC_TAG_ALBUM_ARTIST,
	CLGC_TAG_GENRE,
	CLGC_TAG_DATE,
	CLGC_TAG_MAX
} ClgcTagType;

typedef struct _ClgcTag ClgcTag;

#define CLGC_TYPE_TAG (clgc_tag_get_type())

ClgcTag* clgc_tag_new(ClgcTagType type, const char* value);

ClgcTag* clgc_tag_build_from_idname(const char* name);

ClgcTag* clgc_tag_copy(ClgcTag* self);

void clgc_tag_free(ClgcTag* self);

/**
 * @brief Transform the enum to an icon name
 * @param tag The enum
 * @return The icon name of the enum
 */
const char* clgc_tag_get_icon_name(ClgcTag* self);

/**
 * @brief Transform the enum to a identifiant
 * @param tag The enum
 * @return The ID of the enum
 */
const char* clgc_tag_get_id(ClgcTag* self);

ClgcTagType clgc_tag_get_key(ClgcTag* self);

/**
 * @brief Transform the enum to a string
 * @param tag The enum
 * @return The string of the enum
 */
const char* clgc_tag_get_name(ClgcTag* self);

const char* clgc_tag_get_value(ClgcTag* self);

GType clgc_tag_get_type();

/**
 * @brief Transform the enum to an icon name
 * @param tag The enum
 * @return The icon name of the enum
 */
const char* clgc_tag_type_get_icon_name(ClgcTagType tag);

/**
 * @brief Transform the enum to a identifiant
 * @param tag The enum
 * @return The ID of the enum
 */
const char* clgc_tag_type_get_id(ClgcTagType tag);

/**
 * @brief Transform the enum to a string
 * @param tag The enum
 * @return The string of the enum
 */
const char* clgc_tag_type_get_name(ClgcTagType tag);

G_END_DECLS

#endif // __CALGICI_TAG__
