/*
 * clgc-adaptive-layout: Calgici AdaptiveLayout class
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CLGC_ADAPTIVE_BOX__
#define __CLGC_ADAPTIVE_BOX__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CLGC_TYPE_ADAPTIVE_BOX (clgc_adaptive_box_get_type())

G_DECLARE_FINAL_TYPE(ClgcAdaptiveBox, clgc_adaptive_box, CLGC, ADAPTIVE_BOX, GtkWidget)

/**
 * @brief Create an instance of adaptive box
 * @return An instance of adaptive box
 */
GtkWidget* clgc_adaptive_box_new();

GtkWidget* clgc_adaptive_box_get_end_widget(ClgcAdaptiveBox* self);

GtkWidget* clgc_adaptive_box_get_main_widget(ClgcAdaptiveBox* self);

GtkWidget* clgc_adaptive_box_get_secondary_widget(ClgcAdaptiveBox* self);

GtkWidget* clgc_adaptive_box_get_start_widget(ClgcAdaptiveBox* self);

void clgc_adaptive_box_set_end_widget(ClgcAdaptiveBox* self, GtkWidget* widget);

void clgc_adaptive_box_set_main_widget(ClgcAdaptiveBox* self, GtkWidget* widget);

void clgc_adaptive_box_set_secondary_widget(ClgcAdaptiveBox* self, GtkWidget* widget);

void clgc_adaptive_box_set_start_widget(ClgcAdaptiveBox* self, GtkWidget* widget);

G_END_DECLS

#endif // __CLGC_ADAPTIVE_BOX__
