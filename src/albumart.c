/*
 * albumart: MPD albumart class to get back the albumart
 *
 * Copyright (C) 2020-2023  Thibault Peypelut <thibault.pey@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "albumart.h"

#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

struct mpd_albumart
{
	size_t m_size_max;
	size_t m_size;
	void* m_data;
};

static bool mpd_albumart_set_size_data(struct mpd_albumart* albumart, size_t size)
{
	if (!(size > 0))
		return false;

	albumart->m_data = malloc(1 * size);

	if (!albumart->m_data)
		// Out of memory
		return false;

	albumart->m_size = size;
	return true;
}

static struct mpd_albumart* mpd_albumart_new(size_t size_max)
{
	if (size_max == 0)
		return NULL;

	struct mpd_albumart* albumart = (struct mpd_albumart*) malloc(1 * sizeof(struct mpd_albumart));

	if (!albumart)
		return NULL;

	albumart->m_size_max = size_max;
	albumart->m_size = 0;
	albumart->m_data = NULL;

	return albumart;
}

void mpd_albumart_free(struct mpd_albumart* albumart)
{
	if (!albumart)
		free(albumart->m_data);

	free(albumart);
}

struct mpd_albumart* mpd_albumart_begin(const struct mpd_pair* pair)
{
	size_t size = 0;

	if (strcmp(pair->name, "size") != 0 || !isdigit(pair->value[0]))
		return NULL;

	size = strtoumax(pair->value, NULL, 10);
	return mpd_albumart_new(size);
}

const void* mpd_albumart_get_data(struct mpd_albumart* albumart)
{
	return (albumart) ? albumart->m_data : NULL;
}

size_t mpd_albumart_get_size(struct mpd_albumart* albumart)
{
	return (albumart) ? albumart->m_size : 0;
}

size_t mpd_albumart_get_size_max(struct mpd_albumart* albumart)
{
	return (albumart) ? albumart->m_size_max : 0;
}

struct mpd_albumart* mpd_recv_get_albumart(struct mpd_connection* connection)
{
	size_t size = 0;
	struct mpd_pair* pair = NULL;
	struct mpd_albumart* albumart = NULL;

	pair = mpd_recv_pair_named(connection, "size");

	if (!pair)
		return NULL;

	albumart = mpd_albumart_begin(pair);
	mpd_return_pair(connection, pair);

	if (!albumart)
		// Out of memory
		return NULL;

	pair = mpd_recv_pair_named(connection, "binary");

	if (!pair || !isdigit(pair->value[0]))
	{
		mpd_albumart_free(albumart);
		return NULL;
	}

	size = strtoumax(pair->value, NULL, 10);
	mpd_return_pair(connection, pair);

	if (!mpd_albumart_set_size_data(albumart, size))
	{
		mpd_albumart_free(albumart);
		return NULL;
	}


	if (!mpd_recv_binary(connection, albumart->m_data, albumart->m_size))
	{
		mpd_albumart_free(albumart);
		return NULL;
	}

	return albumart;
}

struct mpd_albumart* mpd_run_get_albumart(struct mpd_connection* connection, const char* uri, size_t offset)
{
	struct mpd_albumart* albumart;

	if (!mpd_send_get_albumart(connection, uri, offset))
		return NULL;

	albumart = mpd_recv_get_albumart(connection);

	if (!mpd_response_finish(connection) && albumart != NULL)
	{
		mpd_albumart_free(albumart);
		albumart = NULL;
	}

	mpd_response_finish(connection);
	return albumart;
}

bool mpd_send_get_albumart(struct mpd_connection* connection, const char* uri, size_t offset)
{
	char offset_str[32];

	snprintf(offset_str, 32, "%lu", offset);
	return mpd_send_command(connection, "albumart", uri, offset_str, NULL);
}
