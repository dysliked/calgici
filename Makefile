NAME := ${notdir ${realpath .}}
IDNAME := org.dysliked.${NAME}
TITLE := ${shell TMP=${NAME}; echo $${TMP^^}}
VERSION := 0.0.2

# INPUT

## Locales po
PO_DIR := po
PO_PREFIX := po/
PO_SUFFIX := .po

PO := ${wildcard ${PO_PREFIX}*${PO_SUFFIX}}

## Modules
PC_DIR := pc
PC_PREFIX := ${PC_DIR}/
PC_SUFFIX := .pc
PKG := gtk4 libadwaita-1 libmpdclient

PC := ${addprefix ${PC_PREFIX},${addsuffix ${PC_SUFFIX},${PKG}}}

### Resources
RSC_DIR := data
RSC_PREFIX := ${RSC_DIR}/
RSC_SUFFIX := .gresource.xml

RSC := ${wildcard ${RSC_PREFIX}*${RSC_SUFFIX}}

### Schemas
SCH_DIR := data
SCH_PREFIX := ${SCH_DIR}/
SCH_SUFFIX := .gschema.xml

SCH := ${wildcard ${SCH_PREFIX}*${SCH_SUFFIX}}

## Sources
SRC_DIR := src
SRC_PREFIX := ${SRC_DIR}/
SRC_SUFFIX := .c

SRC := ${wildcard ${SRC_PREFIX}*${SRC_SUFFIX}}


# OUTPUT
BUILD_DIR := target
BUILD_PREFIX := ${BUILD_DIR}/

SHARE_DIR := ${BUILD_PREFIX}share
SHARE_PREFIX := ${SHARE_DIR}/

## Binary
BIN_DIR := ${BUILD_PREFIX}bin
BIN_PREFIX := ${BIN_DIR}/
BIN_SUFFIX :=

BIN := ${BIN_PREFIX}${NAME}${BIN_SUFFIX}

## Dependancies
DEP_DIR := ${BUILD_PREFIX}depends
DEP_PREFIX := ${DEP_DIR}/
DEP_SUFFIX := .mk

DEP := ${addprefix ${DEP_PREFIX},${addsuffix ${DEP_SUFFIX},${basename ${SRC}}}}

## Includes
INC_DIR := ${BUILD_PREFIX}include
INC_PREFIX := ${INC_DIR}/
INC_SUFFIX := .h

INC := ${subst ${SRC_PREFIX},${INC_PREFIX},${wildcard ${SRC_PREFIX}*${INC_SUFFIX}}}

## Library
LIB_DIR := ${BUILD_PREFIX}lib
LIB_PREFIX := ${LIB_DIR}/lib
LIB_SUFFIX := .so

LIB := ${LIB_PREFIX}${NAME}${LIB_SUFFIX}

## Objects
OBJ_DIR := ${BUILD_PREFIX}obj
OBJ_PREFIX := ${OBJ_DIR}/
OBJ_SUFFIX := .o

OBJ := ${addprefix ${OBJ_PREFIX},${addsuffix ${OBJ_SUFFIX},${basename ${SRC}}}}

## Resources
RSC_SRC_DIR := ${BUILD_PREFIX}resources
RSC_SRC_PREFIX := ${RSC_SRC_DIR}/

RSC_SRC := ${subst ${RSC_PREFIX},${RSC_SRC_PREFIX},${subst ${RSC_SUFFIX},${SRC_SUFFIX},${RSC}}}

RSC_DEP := ${addprefix ${DEP_PREFIX},${addsuffix ${DEP_SUFFIX},${basename ${RSC_SRC}}}}
DEP += ${RSC_DEP}

RSC_OBJ := ${addprefix ${OBJ_PREFIX},${addsuffix ${OBJ_SUFFIX},${basename ${RSC_SRC}}}}
OBJ += ${RSC_OBJ}

## Schema
SCH_BUILD_DIR := ${SHARE_PREFIX}glib-2.0/schemas
SCH_BUILD_PREFIX := ${SCH_BUILD_DIR}/

SCH_BUILD := ${subst ${SCH_PREFIX},${SCH_BUILD_PREFIX},${SCH}}
SCHC := ${SCH_BUILD_PREFIX}gschemas.compiled

## Locales mo
MO_PREFIX := ${SHARE_PREFIX}locale/
MO_SUFFIX := /LC_MESSAGES/${NAME}.mo

MO := ${addprefix ${MO_PREFIX},${addsuffix ${MO_SUFFIX},${basename ${notdir ${PO}}}}}

# AUTO


# Archive
TAR_PREFIX := ../
TAR_SUFFIX := .tar.gz


# Util binaries
TESTDIR := test -d
MKDIR := mkdir -p
ECHO := echo
RMDIR := ${RM} -r
CD := cd
LINK := ln -s -f
CP := cp
MV := mv
TAR := tar czf
PKG_CONFIG := pkg-config

# Compilator
CC := gcc

# Flags
CFLAGS := -g -Wall -Wextra -DNAME=\"${NAME}\" -DVERSION=\"${VERSION}\" -DAPP_TITLE=\"${TITLE}\" -DAPPLICATION_ID=\"${IDNAME}\" -DLOCALE_DIR=\"locale\" ${shell ${PKG_CONFIG} --cflags ${PKG}}
LDFLAGS := ${shell ${PKG_CONFIG} --libs ${PKG}}

all: binary schemas lang

binary: objects ${BIN}

library: objects ${LIB}

objects: headers depends ${OBJ}

resources: ${RSC_SRC}

schemas: ${SCHC}

depends: ${DEP}

headers: ${INC}

lang: ${MO}

check: ${PC}

tests:
	@${ECHO} "TODO"

archive: distclean
	@${ECHO} "Building the archive"
	${TAR} ${TAR_PREFIX}${NAME}-${VERSION}${TAR_SUFFIX} ../${NAME}

distclean: clean
	@${ECHO} "Cleaning the binaries, libraries and includes of ${NAME}"
	${RMDIR} ${BIN_DIR}
	${RMDIR} ${LIB_DIR}
	${RMDIR} ${INC_DIR}
	${RMDIR} ${SHARE_DIR}

clean:
	@${ECHO} "Cleaning the generated files of ${NAME}"
	${RMDIR} ${DEP_DIR}
	${RMDIR} ${OBJ_DIR}
	${RMDIR} ${RSC_SRC_DIR}

${BIN_PREFIX}%${BIN_SUFFIX}: ${OBJ}
	@${ECHO} "Link: $@"
	${MKDIR} ${@D}
	${CC} -o $@ ${LDFLAGS} $^

${LIB_PREFIX}%${LIB_SUFFIX}:
	@${ECHO} "Build: $@"
	${MKDIR} ${@D}
	${CC} -shared -o $@ ${LDFLAGS} $^

${DEP_PREFIX}%${DEP_SUFFIX}: %${SRC_SUFFIX}
	@${ECHO} "Dependance: $*"
	${MKDIR} ${@D}
	${CC} -o $@ ${CFLAGS} -MM -MT ${OBJ_PREFIX}$*${OBJ_SUFFIX} $<

${OBJ_PREFIX}%${OBJ_SUFFIX}: %${SRC_SUFFIX}
	@${ECHO} "Generate: $*"
	${MKDIR} ${@D}
	${CC} -o $@ ${CFLAGS} -fPIC -c $<

${RSC_SRC_PREFIX}%${SRC_SUFFIX}: ${RSC_PREFIX}%${RSC_SUFFIX}
	@${ECHO} "Resource: $*"
	${MKDIR} ${@D}
	glib-compile-resources --sourcedir ${<D} --c-name ${NAME} --internal --generate --target $@ $<

${RSC_DEP_PREFIX}%${DEP_SUFFIX}: ${RSC_PREFIX}%${RSC_SUFFIX}
	@${ECHO} "Resource (dependencies): $*"
	${MKDIR} ${@D}
	glib-compile-resources --sourcedir ${<D} --c-name ${NAME} --internal --generate-dependencies --target $@ $<

${SCHC}: ${SCH_BUILD}
	@${ECHO} "Schemas: $@"
	${MKDIR} ${@D}
	glib-compile-schemas --strict --targetdir ${@D} ${SCH_BUILD_DIR}

${SCH_BUILD_PREFIX}%${SCH_SUFFIX}: ${SCH_PREFIX}%${SCH_SUFFIX}
	@${ECHO} "Copy: $*"
	${MKDIR} ${@D}
	${CP} $< $@

${INC_PREFIX}%${INC_SUFFIX}: ${SRC_PREFIX}%${INC_SUFFIX}
	@${ECHO} "Copy: $*"
	${MKDIR} ${@D}
	${CP} $< $@

${MO_PREFIX}%${MO_SUFFIX}: ${PO_PREFIX}%${PO_SUFFIX}
	@${ECHO} "Translate: $*"
	${MKDIR} ${@D}
	msgfmt -c -v -o $@ $<

${PC_PREFIX}%${PC_SUFFIX}:
	${PKG_CONFIG} --exists $*

BLU:
	@${ECHO} "NAME: ${NAME}"
	@${ECHO} "IDNAME: ${IDNAME}"
	@${ECHO} "TITLE: ${TITLE}"
	@${ECHO} "-----"
	@${ECHO} "CC: ${CC}"
	@${ECHO} "CFLAGS: ${CFLAGS}"
	@${ECHO} "LDFLAGS: ${LDFLAGS}"
	@${ECHO} "====="
	@${ECHO} "SOURCES: ${SRC}"
	@${ECHO} "-----"
	@${ECHO} "HEADERS: ${INC}"
	@${ECHO} "-----"
	@${ECHO} "RESOURCES: ${RSC}"
	@${ECHO} "RESOURCES: ${RSC_SRC}"
	@${ECHO} "SCHEMAS: ${SCH}"
	@${ECHO} "SCHEMAS: ${SCH_BUILD}"
	@${ECHO} "====="
	@${ECHO} "PO: ${PO}"
	@${ECHO} "-----"
	@${ECHO} "MO: ${MO}"
	@${ECHO} "====="
	@${ECHO} "DEPENDS: ${DEP}"
	@${ECHO} "-----"
	@${ECHO} "OBJECTS: ${OBJ}"
	@${ECHO} "====="
	@${ECHO} "LIBRARY: ${LIB}"
	@${ECHO} "BINARY: ${BIN}"

ifneq (${wildcard ${DEP_PREFIX}*},)
 -include ${DEP}
endif
