# Calgici - Client MPD

Calgici is a MPD client for GTK/GNOME.

## Build

### Requirements

 - `gtk4`
 - `libmpdclient`
 - `libadwaita-1`
 - `mpd` _(optional)_
 - `gettext` _(make)_
 - `meson` _(make)_

### Compilation

To compile calgici using makefile command:
```bash
meson setup --prefix=/usr build
meson compile -C build
```

### Local execution

Install schemas in your home directory
```bash
mkdir --parents $HOME/.local/share/glib-2.0/schemas
ln -sf $(pwd)/build/data/gschemas.compiled $HOME/.local/share/glib-2.0/schemas/.
```

Execute the binary
```bash
XDG_DATA_DIR=$HOME/.local/share ./build/src/calgici
```

# Authors
 - Thibault PEYPELUT (dysliked)
