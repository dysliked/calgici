#include <mpclient.h>

#include <stdio.h>

int main(int argc, char* argv[])
{
	struct clgc_mpc* mpc = clgc_mpc_new("localhost", 6600, 0);
	
	clgc_mpc_get_albumart(mpc, "Artemis", "toto");
	clgc_mpc_get_albumart(mpc, "Back in Black", "titi");
	
	clgc_mpc_free(mpc);
	return 0;
}

